(* Name   : M2HM.DEF
 * Changed: 12/25/87 LF
 * Notes :
 * 1. Changed 'MP' from A6 to A5, the rational behind this is that the
 *    Amiga's system calls use A6 and by using A5 as the frame ptr A6 will
 *    be free to use as the base for library calls and will not have to be
 *    saved/restored.
 * 2. Added new "ItemMode" relocMd, this mode is to support refrences to
 *    variables in imported modules.
 * 3. Updated with changes.
 * 4. Moved variable "mask" to definition part of module to allow M2EM.MOD to
 *    use the "mask" lookup table.
 * 5. Added variable "glbReloc" to control if global variables should be
 *    accessed as relocMd or SB relative mode.
 * 6. Added variable "pcLastProcResult" to keep track of last time a
 *    Move Result(D0 or D1),Dn was generated. This information is used 
 *    in M2CM.GenAssign to elliminate the redundant instruction
 * 7. Added variable "profiler" to control generation of profiling code.
 *)

(*$L+ relocatable *)
DEFINITION MODULE M2HM;

  FROM M2DM IMPORT ObjPtr, StrPtr, ConstValue;
  FROM M2SM IMPORT Symbol;

  CONST   (* D-Register pool for expression evaluation *)
          D0 = 0;  (* freely used, never reserved      *)
          D1 = 1;  (* freely used, never reserved      *)
          D2 = 2;  (* D-pool, reserved when used       *)
          D3 = 3;  (* D-pool, reserved when used       *)
          D4 = 4;  (* D-pool, reserved when used       *)
          D5 = 5;  (* D-pool, reserved when used       *)
          D6 = 6;  (* D-pool, reserved when used       *)
          D7 = 7;  (* D-pool, reserved when used       *)

          (* A-Register pool for address calculations  *)
          A0 = 0;  (* A-pool, reserved when used       *)
          A1 = 1;  (* A-pool, reserved when used       *)
          A2 = 2;  (* A-pool, reserved when used       *)
          A3 = 3;  (* A-pool, reserved when used       *)

          (* Dedicated A-Registers                     *)
          SB = 4;  (* SB = A4: static base pointer     *)
          MP = 5;  (* MP = A5: mark/frame pointer      *)
          A6 = 6;  (* A6 is   n e v e r   used !       *)
          SP = 7;  (* SP = A7: active stack pointer    *)

          (* Instruction size for simple types         *)
          byte = 0;   word = 1;   long = 2;

          (* Descriptor size for dynamic array param.  *)
          DynArrDesSize = 6;

  TYPE    Condition = ( T,  F, HI, LS, CC, CS, NE, EQ,
                       VC, VS, PL, MI, GE, LT, GT, LE );
                      (* possibly extended by an ordered *)
                      (* set of Floating Point Conditions *)

          RegType   = (Dreg, Areg);
          Register  = [0.. 15];
          WidType   = [byte..long];
          ShiType   = (Asl, Asr, Lsl, Lsr, Rol, Ror);

          ItemMode  = (absMd,  relocMd, RindMd, RidxMd, conMd,
                       stkMd,  AregMd,  DregMd, cocMd,  typMd,
                       procMd, codMd,   prgMd,  fltMd);

          ItSet     = SET OF ItemMode;

          Item      = RECORD
                        typ: StrPtr;
                        CASE mode: ItemMode OF
                          absMd, relocMd, RindMd, RidxMd,
                          stkMd, AregMd, DregMd:
                                  mod, lev:   CARDINAL;
                                  adr, off:   INTEGER;
                                  R, RX:      Register;
                                  wid:        WidType;
                                  scl:        CARDINAL;
                                  indir:      BOOLEAN;
                        | conMd:  val:        ConstValue;
                        | cocMd:  CC:         Condition;
                                  Tjmp, Fjmp: CARDINAL;
                        | procMd,
                          codMd:  proc:       ObjPtr;
                        | prgMd:  where:      CARDINAL;
                        | fltMd:  FR:         Register;
                        | typMd:  (* no field *)
                        END;
                      END;

  VAR     curLev    : CARDINAL;
          mask      : ARRAY [ 0 .. 32 ] OF LONGINT;
          rngchk    : BOOLEAN;
          ovflchk   : BOOLEAN;
          glbReloc  : BOOLEAN; (* TRUE = relocatable, FALSE = SB relative *)
          profiler  : BOOLEAN; (* TRUE = gen profiling code *)
          pcLastProcResult : CARDINAL;

  PROCEDURE LongVal  (VAR x: Item): LONGINT;   (* 32 bits of constant x      *)
  PROCEDURE WordVal  (VAR x: Item): CARDINAL;  (* 16 bits of constant x      *)
  PROCEDURE SimpleC  (VAR x: Item): BOOLEAN;   (* is x a simple constant     *)
  PROCEDURE SignedT  (VAR x: Item): BOOLEAN;   (* is x a signed type         *)
  PROCEDURE SimpleT  (VAR x: Item): BOOLEAN;   (* is x a simple type         *)
  PROCEDURE RealT    (VAR x: Item): BOOLEAN;   (* is x a floating-point type *)

  PROCEDURE SetglbMd(VAR x: Item; fadr: INTEGER;  ftyp: StrPtr);
  PROCEDURE SetlocMd(VAR x: Item; fadr: INTEGER;  ftyp: StrPtr);
  PROCEDURE SetregMd(VAR x: Item; freg: Register; ftyp: StrPtr);
  PROCEDURE SetstkMd(VAR x: Item; ftyp: StrPtr);
  PROCEDURE SetconMd(VAR x: Item; fval: LONGINT;  ftyp: StrPtr);

  PROCEDURE GetReg     (VAR r: Register; qual: RegType);
  PROCEDURE GetRegPair (VAR r: Register); 
  PROCEDURE Release    (VAR x: Item);
  PROCEDURE ReleaseReg (r: Register);
  PROCEDURE LockReg    (r: Register);
  PROCEDURE UnlockReg  (r: Register);
  PROCEDURE SetbusyReg (r: Register);
  PROCEDURE SaveRegs   (VAR saveSet: BITSET);
  PROCEDURE RestoreRegs(saveSet: BITSET);
  PROCEDURE InitRegs;
  PROCEDURE CheckRegs;

  PROCEDURE Isz (VAR x: Item; VAR fsz: WidType); (* instruction size for x  *)
  PROCEDURE Gea (VAR x: Item; VAR fea: CARDINAL);(* effective address for x *)
  PROCEDURE Ext (VAR x: Item);                   (* instr. extension for x  *)

  PROCEDURE InvertCC (cond: Condition): Condition;
  PROCEDURE Jf(c: Condition; VAR l: CARDINAL);  (* jump forward, build chain *)
  PROCEDURE Jb(c: Condition; l: CARDINAL);      (* jump backward             *)

  PROCEDURE StackTop(i: INTEGER);          (* increment stack pointer SP*)
  PROCEDURE SetupSL(plev: CARDINAL);       (* push Static Link to stack *)
  PROCEDURE CallSystem(n: CARDINAL);       (* call System.#n procedure  *)
  PROCEDURE GenHalt(haltindex: CARDINAL);  (* call System.HALTX proc.   *)

  PROCEDURE LoadAdr   (VAR x: Item);       (* load address of x         *)
  PROCEDURE LoadD     (VAR x: Item);       (* load data designated by x *)
  PROCEDURE LoadP     (VAR x: Item);       (* load pointer design. by x *)
  PROCEDURE LoadX     (VAR x: Item;        (* load and extend value(x)  *)
                       req: WidType);      (* to requested width        *)

  PROCEDURE Move      (VAR x, y: Item);    (* value(x) --> y            *)
  PROCEDURE MoveAdr   (VAR x, y: Item);    (* adr(x)   --> y            *)
  PROCEDURE MoveBlock (VAR x, y: Item;     (* mve 'sz' bytes of x to y  *)
                       sz: INTEGER; isstring: BOOLEAN);

  PROCEDURE Neg1      (VAR x: Item);       (* x := -x                   *)
  PROCEDURE Abs1      (VAR x: Item);       (* x := ABS(x)               *)
  PROCEDURE Cap1      (VAR x: Item);       (* x := CAP(x)               *)
  PROCEDURE Tst1      (VAR x: Item);       (* x - 0                     *)
  PROCEDURE Com1      (VAR x: Item);       (* x := complement(x)        *)
  PROCEDURE Inc1      (VAR x: Item);       (* x := x + 1                *)
  PROCEDURE Dec1      (VAR x: Item);       (* x := x - 1                *)

  PROCEDURE Add2      (VAR x, y: Item);    (* x := x + y                *)
  PROCEDURE Sub2      (VAR x, y: Item);    (* x := x - y                *)
  PROCEDURE And2      (VAR x, y: Item);    (* x := x AND y              *)
  PROCEDURE Or2       (VAR x, y: Item);    (* x := x OR y               *)
  PROCEDURE Eor2      (VAR x, y: Item);    (* x := x EOR y              *)
  PROCEDURE Div2      (VAR x, y: Item);    (* x := x DIV y              *)
  PROCEDURE Mod2      (VAR x, y: Item);    (* x := x MOD y              *)
  PROCEDURE Mul2      (VAR x, y: Item);    (* x := x * y                *)
  PROCEDURE Cmp2      (VAR x, y: Item);    (* x - y                     *)
  PROCEDURE In2       (VAR x, y: Item);    (* x IN y                    *)
  PROCEDURE Shi2      (VAR x, y: Item;     (* x := x shifted by y       *)
                       shiftop: ShiType);
  PROCEDURE Ash2      (VAR x, y :Item;     (* x := x arith.shifted by y *) 
                       shiftop: ShiType);

  PROCEDURE ConvertTyp(functyp: StrPtr; VAR x: Item);
    (* x := x converted to type functyp *)

  PROCEDURE ConIndex(VAR x: Item; inc: INTEGER);
    (* constant offset/index inc to base x *)

  PROCEDURE VarIndex(VAR x, y: Item; elsize: INTEGER);
    (* variable offset/index y to base x (elements of size elsize) *)

  PROCEDURE GetHigh(VAR x: Item);
    (* get high bound of dynamic array parameter x *)

  PROCEDURE Normalize(VAR x: Item; i: INTEGER);
    (* normalize subrange x to zero by lowbound i *)

  PROCEDURE CheckHigh(VAR x, high: Item);
    (* check x in the variable range [ 0 .. high ] *)

  PROCEDURE CheckClimit(VAR x: Item; limit: INTEGER);
    (* check x in the constant interval [ 0 .. limit ] *)

  PROCEDURE CheckRange(VAR x: Item; min, max: INTEGER);
    (* check x in the constant range [ min .. max ] *)

  PROCEDURE CheckDbltoSingle(VAR x, y: Item);
    (* check for assignment of double-word x to single-word y *)
 
  PROCEDURE PreLoad(VAR op: Symbol; VAR x, y: Item);
    (* exchange x and y for optimization to GenOp *)

  PROCEDURE DynArray(VAR x, y: Item);
    (* descriptor for dynamic array parameter *)

  PROCEDURE CopyDynArray(adr, size: INTEGER);
    (* value for dynamic array parameter *)

  PROCEDURE EnterCase(VAR x: Item; base, lo, hi: INTEGER);
    (* enter case-statement processor *)

  PROCEDURE ExitCase;
    (* leave case-statement processor *)

  PROCEDURE Link(VAR l: CARDINAL; lev: CARDINAL);
    (* generate entry-code for procedure at level lev *)

  PROCEDURE Unlink(parSize: INTEGER; lev: CARDINAL);
    (* generate exit-code for procedure at level lev *)

  PROCEDURE CallInt(proc: ObjPtr);    (* call of local procedure     *)
  PROCEDURE CallExt(proc: ObjPtr);    (* call of external procedure  *)
  PROCEDURE CallInd(VAR x: Item);     (* call of procedure variable  *)

  PROCEDURE EnterModule;              (* main module's entry code    *)
  PROCEDURE ExitModule;               (* main module's exit code     *)
  PROCEDURE InitModule(m : CARDINAL); (* init of imported modules    *)

  PROCEDURE InitM2HM;

END M2HM.
