(* Name   : M2DM.MOD
 * Changed: 06/06/87 LF
 * Notes :
 * 1. The heap is now allocated in another module, to initialize the heap
 *    management variables the procedure "InitHeap" is called.
 * 2. Added code to "ALLOCATE" to AbortCompilation if the heap overflows.
 * 3. Remove procedure "Available" from module, useless.
 * 4. Turned off compiler switch "D".
 *)

(*$L+,D-  relocatable *)
IMPLEMENTATION MODULE M2DM;

  FROM SYSTEM IMPORT ADDRESS, ADR;
  FROM M2SM   IMPORT Mark;
  FROM M2     IMPORT AbortCompilation;

  VAR     current : ADDRESS; (* Current position of heap ptr *)
          last    : ADDRESS; (* lowest address in heap *)

  PROCEDURE ALLOCATE(VAR a: ADDRESS; n: CARDINAL);
  BEGIN
    (* move heap ptr down by specified amount *)
    DEC(current, VAL(ADDRESS, n));
    IF current < last THEN (* Heap Overflow *)
      Mark(228);
      AbortCompilation;
    END;
    a := current;
  END ALLOCATE;

  PROCEDURE ResetHeap(a: ADDRESS);
  BEGIN current := a
  END ResetHeap;

  (* Initialize heap management variables *)
  PROCEDURE InitHeap;
  BEGIN
    last := HeapBasePtr;
    current := last + VAL(ADDRESS,HeapSize);
  END InitHeap;

END M2DM.
