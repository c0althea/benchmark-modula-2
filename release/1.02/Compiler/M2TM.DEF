(* Name   : M2TM.DEF
 * Changed: 06/06/87 LF
 * Notes :
 * 1. Added procedure "M2TMModuleBody" which represents the code for the
 *    module body.
 * 2. Removed procedure "CheckUDP". Now located in module "M2PM".
 *)

(*$L- SB-relative *)
DEFINITION MODULE M2TM;

  FROM M2DM IMPORT ObjPtr, ObjClass, StrPtr, StrForm, ParPtr, PDPtr;

  VAR topScope, Scope: ObjPtr;  (*header of scope located by Find*)

  PROCEDURE FindInScope(id: INTEGER; root: ObjPtr): ObjPtr;
  PROCEDURE Find(id: INTEGER): ObjPtr;
  PROCEDURE FindImport(id: INTEGER): ObjPtr;

  PROCEDURE NewObj(id: INTEGER; class: ObjClass): ObjPtr;
  PROCEDURE NewStr(form: StrForm): StrPtr;
  PROCEDURE NewPar(ident: INTEGER; isvar: BOOLEAN; last: ParPtr): ParPtr;
  PROCEDURE NewImp(scope, obj: ObjPtr);

  PROCEDURE NewScope(kind: ObjClass);
  PROCEDURE CloseScope;

  PROCEDURE MarkHeap;
  PROCEDURE ReleaseHeap;
  PROCEDURE InitTableHandler;
  PROCEDURE M2TMModuleBody;

END M2TM.
