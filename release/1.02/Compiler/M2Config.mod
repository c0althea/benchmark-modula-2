(************************************************************************
 *                Modula-2 Software Construction Set                    *
 *                     (c) 1993 by Jim Olinger                          *
 ************************************************************************
 * Name: M2Config.MOD                  Version: Amiga.01.01             *
 * Created: 05/16/87   Updated: 11/01/87   Author: Leon Frenkel         *
 * Description: Change the default compiler options to new values.      *
 * Usage: M2Config [-s|-d] <M2_FileName>                                *
 *     = Input Configuration                                            *
 *  -s = Only Show Configuration                                        *
 *  -d = Set Default Configuration                                      *
 ************************************************************************)
 
 (* Version 1.01: 10/26/93 JKO
  *  1. Changed title.
  *  2. Changed "COMaxIdentBufSize" from 32766D to 65532D.
  *)

(*$L-,D- *)
MODULE M2Config;

FROM SYSTEM IMPORT
	ADDRESS, BYTE,
	ADR, TSIZE;
FROM Conversions IMPORT
	ConvStringToNumber;
IMPORT CPrintStdOut;
FROM CStdIO IMPORT
	FILE,
	openstdio, closestdio,
	fopen, fclose,
	fseek, ftell,
	getc, getw, fgets, getchar, ungetc,
	putc, putw;
FROM CStrings IMPORT
	strcmp;
FROM FormatString IMPORT
	FormatArg;
FROM System IMPORT
	argc, argv;

CONST
  (***
  Title =
    "M2SCS: Config Compiler  Amiga.01.00 Copyright \xA9 1987 by Leon Frenkel\n";
  ***)
  
  Title =
    "M2SCS: Compiler Config 01.01 Copyright \xA9 1993 by Jim Olinger\n";

  MsgUsage = "Usage: M2Config [-d|-s] <M2_FileName>";

  M2Hunks = 16D;	(* Hunks in a M2 compiler executable *)

  HUNKHeader  = LONGCARD(03F3H);
  HUNKCode    = LONGCARD(03E9H);
  HUNKReloc32 = LONGCARD(03ECH);
  HUNKSymbol  = LONGCARD(03F0H);
  HUNKDebug   = LONGCARD(03F1H);
  HUNKEnd     = LONGCARD(03F2H);

  (* (C)ompiler(O)ption(D)efault... *)
  CODHeapSize          = 150000D;
  CODCodeBufSize       = 32766D;
  CODConstBufSize      = 32766D;
  CODIdentBufSize      = 32766D;
  CODAdrRelocBufSize   = 1000D;
  CODGenRefFile        = FALSE;
  CODRangeChk          = FALSE;
  CODOverflowChk       = FALSE;
  CODDiskBufSize       = 1024D;
  CODCopyDynValueParms = TRUE;
  CODClrModuleKey      = FALSE;
  CODGlobalVarAbsAdr   = FALSE;

  COMinHeapSize        = 10000D;
  COMaxHeapSize        = 0FFFFFFFFH;
  COMinCodeBufSize     = 1000D;
  COMaxCodeBufSize     = 32766D;
  COMinConstBufSize    = 1000D;
  COMaxConstBufSize    = 32766D;
  COMinIdentBufSize    = 1000D;
  (** COMaxIdentBufSize    = 32766D; **)
  COMaxIdentBufSize    = 65532D;
  COMinDiskBufSize     = 512D;
  COMaxDiskBufSize     = 0FFFFFFFFH;
  COMinAdrRelocBufSize = 100D;
  COMaxAdrRelocBufSize = 32766D;

TYPE
  CmdMode = (Config, Show, Defaults);

VAR
  M2FileName  : ADDRESS;
  NameArg     : CARDINAL;
  Mode        : CmdMode;
  M2File      : FILE;
  argo        : ARRAY [0..9] OF FormatArg;
  ch          : CHAR;
  CompOptInitialized     : BOOLEAN; (* TRUE = initialize, FALSE = not init *)
  CompOptGlobalVarAbsAdr : BOOLEAN;
  CompOptGenRefFile      : BOOLEAN;
  CompOptClrModuleKey    : BOOLEAN;
  CompOptRangeChk        : BOOLEAN;
  CompOptOverflowChk     : BOOLEAN;
  CompOptDiskBufSize     : LONGCARD;
  CompOptAdrRelocBufSize : LONGCARD;
  CompOptIdentBufSize    : LONGCARD;
  CompOptConstBufSize    : LONGCARD;
  CompOptCodeBufSize     : LONGCARD;
  CompOptHeapSize        : LONGCARD;

(* printf - a short form for printf *)
PROCEDURE printf(s: ARRAY OF CHAR);
BEGIN
  CPrintStdOut.printf(ADR(s), argo);
END printf;

(* Init - initialize program *)
PROCEDURE Init();
BEGIN
  openstdio();
END Init;

(* Cleanup - cleanup before exiting *)
PROCEDURE Cleanup();
VAR b : BOOLEAN;
BEGIN
  IF (M2File # NIL) THEN
    b := fclose(M2File);
  END;
  closestdio();
END Cleanup;

PROCEDURE fatal(s: ARRAY OF CHAR);
BEGIN
  argo[0].L := ADR(s);
  printf("ERROR: %s\n");
  Cleanup();
  HALT;
END fatal;

(* getl - get a long from a file *)
PROCEDURE getl(stream: FILE): LONGCARD;
VAR
  conv : RECORD
           CASE : CARDINAL OF 
           | 0: h, l : CARDINAL;
           | 1: hl   : LONGCARD;
           END;
         END;
BEGIN
  conv.h := getw(stream);
  conv.l := getw(stream);
  RETURN (conv.hl);
END getl;

(* putl - put a long to a file *)
PROCEDURE putl(long: LONGCARD; stream: FILE);
VAR
  r : INTEGER;
  conv : RECORD
           CASE : CARDINAL OF 
           | 0: h, l : CARDINAL;
           | 1: hl   : LONGCARD;
           END;
         END;
BEGIN
  conv.hl := long;
  r := putw(conv.h, stream);
  r := putw(conv.l, stream);
END putl;

PROCEDURE ReadString(VAR s: ARRAY OF CHAR);
VAR i : CARDINAL; ch : CHAR;
BEGIN
  i := 0;
  LOOP
    ch := CHR(getchar());
    IF (ch = "\n") THEN EXIT; END;
    s[i] := CAP(ch);
    INC(i);
  END;
  s[i] := 0C;
END ReadString;

PROCEDURE TorF(bool: BOOLEAN): ADDRESS;
BEGIN
  IF bool THEN
    RETURN ADR("TRUE");
  ELSE
    RETURN ADR("FALSE");
  END;
END TorF;

PROCEDURE QueryInt(
             VAR val: LONGCARD; min, max: LONGCARD; text: ARRAY OF CHAR
	  );
VAR
  in : ARRAY [0..127] OF CHAR;
  num: LONGCARD;
BEGIN
  argo[0].L := ADR(text);
  argo[1].L := val;
  LOOP
    printf("%s [%lu]: ");
    ReadString(in);
    IF (in[0] = 0C) THEN EXIT; END; (* No Change *)
    IF (ConvStringToNumber(in,num,FALSE,10)) THEN
      IF (num < min) THEN
        printf("Input Too Small!\n");
      ELSIF (num > max) THEN
        printf("Input Too Large!\n");
      ELSE
        val := num; EXIT; 
      END;
    ELSE
      printf("Invalid Input!\n");
    END;
  END;
END QueryInt;

PROCEDURE QueryBool(VAR flag: BOOLEAN; text: ARRAY OF CHAR);
VAR
  in : ARRAY [0..127] OF CHAR;
  t, f : BOOLEAN;
BEGIN
  argo[0].L := ADR(text);
  argo[1].L := TorF(flag);
  LOOP
    printf("%s [%s]: ");
    ReadString(in);
    IF (in[0] = 0C) THEN EXIT; END; (* No Change *)
    t := strcmp(ADR(in), ADR("TRUE")) = 0;
    f := strcmp(ADR(in), ADR("FALSE")) = 0;
    IF (t = TRUE) THEN
      flag := TRUE; EXIT;
    ELSIF (f = TRUE) THEN
      flag := FALSE; EXIT;
    ELSE
      printf("Invalid Input!\n");
    END;
  END;
END QueryBool;

PROCEDURE ReadCodeBlock();
VAR
  b : BOOLEAN;
  l,l1 : LONGCARD;
BEGIN
  IF (getl(M2File) # HUNKCode) THEN
    fatal("M2 File Invalid!");
  END;

  (* Skip over body of code hunk *)
  b := fseek(M2File, getl(M2File) * 4D, 1);

  LOOP
    l := getl(M2File);
    IF (l = HUNKEnd) THEN 
      EXIT; (* Done. *)
    ELSIF (l = HUNKReloc32) THEN
      LOOP
        l := getl(M2File); 		(* # Offsets *)
        IF (l = 0D) THEN EXIT; END;	(* Offsets = 0 then done. *)
        l1 := getl(M2File);		(* Hunk ID *)
        WHILE (l > 0D) DO		(* Read Offsets *)
          l1 := getl(M2File);
          DEC(l);
        END;
      END;
    ELSIF (l = HUNKDebug) THEN
      b := fseek(M2File, getl(M2File) * 4D, 1); (* skip over debug data *)
    ELSIF (l = HUNKSymbol) THEN
      LOOP
        l := getl(M2File);		(* Name Length *)
        IF (l = 0D) THEN EXIT; END; 	(* Done *)
        WHILE (l > 0D) DO
          l1 := getl(M2File);
          DEC(l);
        END;
        l := getl(M2File);		(* Value *)
      END;
    ELSE
      fatal("M2 File Invalid!");
    END;
  END;
END ReadCodeBlock;

PROCEDURE FindConfigHunk();
VAR
  l : LONGCARD;
  TableSize : LONGCARD;
BEGIN
  IF (getl(M2File) # HUNKHeader) THEN
    fatal("M2 File Not An Executable File");
  END;

  l := getl(M2File);	(* Resident Libs (should be 0) *)
  TableSize := getl(M2File);
  IF (TableSize # M2Hunks) THEN
    printf("WARNING: M2 File Has More Hunks Than M2Hunks!\n");
  END;

  l := getl(M2File);	(* First Hunk *)
  l := getl(M2File);	(* Last Hunk *)

  (* Skip over Hunk Table *)
  WHILE (TableSize > 0D) DO
    l := getl(M2File);
    DEC(TableSize);
  END;

  ReadCodeBlock();
  IF (getl(M2File) # HUNKCode) THEN
    fatal("M2 File Invalid!");
  END;
  l := getl(M2File); (* Code Hunk Size *)

END FindConfigHunk;

PROCEDURE ReadConfig();
VAR
  pos : LONGINT;
  b : BOOLEAN;
BEGIN
  pos := ftell(M2File);			(* Remember position *)
  CompOptHeapSize        := getl(M2File);
  CompOptCodeBufSize     := getl(M2File);
  CompOptConstBufSize    := getl(M2File);
  CompOptIdentBufSize    := getl(M2File);
  CompOptAdrRelocBufSize := getl(M2File);
  CompOptDiskBufSize     := getl(M2File);
  CompOptOverflowChk     := BOOLEAN(getc(M2File));
  CompOptRangeChk        := BOOLEAN(getc(M2File));
  CompOptClrModuleKey    := BOOLEAN(getc(M2File));
  CompOptGenRefFile      := BOOLEAN(getc(M2File));
  CompOptGlobalVarAbsAdr := BOOLEAN(getc(M2File));
  CompOptInitialized     := BOOLEAN(getc(M2File));

  b := fseek(M2File, pos, 0);		(* Restore to beginning of config *)
END ReadConfig;

PROCEDURE WriteConfig();
VAR i : INTEGER;
BEGIN
  CompOptInitialized := TRUE;
  putl(CompOptHeapSize, M2File);
  putl(CompOptCodeBufSize, M2File);
  putl(CompOptConstBufSize, M2File);
  putl(CompOptIdentBufSize, M2File);
  putl(CompOptAdrRelocBufSize, M2File);
  putl(CompOptDiskBufSize, M2File);
  i := putc(CHAR(CompOptOverflowChk), M2File);
  i := putc(CHAR(CompOptRangeChk), M2File);
  i := putc(CHAR(CompOptClrModuleKey), M2File);
  i := putc(CHAR(CompOptGenRefFile), M2File);
  i := putc(CHAR(CompOptGlobalVarAbsAdr), M2File);
  i := putc(CHAR(CompOptInitialized), M2File);
END WriteConfig;

PROCEDURE DefConfig();
BEGIN
  (* Set defaults for Compiler Options *)
  CompOptHeapSize        := CODHeapSize;
  CompOptCodeBufSize     := CODCodeBufSize;
  CompOptConstBufSize    := CODConstBufSize;
  CompOptIdentBufSize    := CODIdentBufSize;
  CompOptDiskBufSize     := CODDiskBufSize;
  CompOptAdrRelocBufSize := CODAdrRelocBufSize;
  CompOptRangeChk        := CODRangeChk;
  CompOptOverflowChk     := CODOverflowChk;
  CompOptGlobalVarAbsAdr := CODGlobalVarAbsAdr;
  CompOptGenRefFile      := CODGenRefFile;
  CompOptClrModuleKey    := CODClrModuleKey;
END DefConfig;

PROCEDURE ShowConfig();
BEGIN
  argo[0].L := CompOptHeapSize;
  printf("Heap Size..............................%lu\n");

  argo[0].L := CompOptCodeBufSize;
  printf("Code Buffer Size.......................%lu\n");

  argo[0].L := CompOptConstBufSize;
  printf("Constant Buffer Size...................%lu\n");

  argo[0].L := CompOptIdentBufSize;
  printf("Identifier Buffer Size.................%lu\n");

  argo[0].L := CompOptDiskBufSize;
  printf("Disk Buffer Size.......................%lu\n");

  argo[0].L := CompOptAdrRelocBufSize;
  printf("Address Relocation Buffer Size.........%lu\n");

  argo[0].L := TorF(CompOptRangeChk);
  printf("Range Checking.........................%s\n");

  argo[0].L := TorF(CompOptOverflowChk);
  printf("Overflow Checking......................%s\n");

  argo[0].L := TorF(CompOptGlobalVarAbsAdr);
  printf("Global Variables Absolute Addressing...%s\n");

  argo[0].L := TorF(CompOptGenRefFile);
  printf("Generate Reference File................%s\n");

  argo[0].L := TorF(CompOptClrModuleKey);
  printf("Clear Module Key.......................%s\n");
END ShowConfig;

PROCEDURE QueryConfig();
BEGIN
  QueryInt(CompOptHeapSize,COMinHeapSize,COMaxHeapSize,
           "Heap Size");
  QueryInt(CompOptCodeBufSize,COMinCodeBufSize,COMaxCodeBufSize,
           "Code Buffer Size");
  QueryInt(CompOptConstBufSize,COMinConstBufSize,COMaxConstBufSize,
           "Constant Buffer Size");
  QueryInt(CompOptIdentBufSize,COMinIdentBufSize,COMaxIdentBufSize,
           "Identifier Buffer Size");
  QueryInt(CompOptDiskBufSize,COMinDiskBufSize,COMaxDiskBufSize,
           "Disk Buffer Size");
  QueryInt(CompOptAdrRelocBufSize,COMinAdrRelocBufSize,COMaxAdrRelocBufSize,
           "Address Relocation Buffer Size");
  QueryBool(CompOptRangeChk, "Range Checking");
  QueryBool(CompOptOverflowChk, "Overflow Checking");
  QueryBool(CompOptGlobalVarAbsAdr, "Global Variables Absolute Addressing");
  QueryBool(CompOptGenRefFile, "Generate Reference File");
  QueryBool(CompOptClrModuleKey, "Clear Module Key");
END QueryConfig;

PROCEDURE ConfigCompiler();
BEGIN
  M2File := fopen(M2FileName, ADR("r+")); (* Open for read/write *)
  IF (M2File = NIL) THEN
    fatal("M2 File Not Opened!");
  END;

  FindConfigHunk();
  ReadConfig();
  IF (Mode = Defaults) OR ((Mode = Config) AND (NOT CompOptInitialized)) THEN
    DefConfig();
  END;
  argo[0].L := M2FileName;
  printf("Compiler FileName: %s\n");
  ShowConfig();
  IF (Mode = Defaults) OR ((Mode = Config) AND (NOT CompOptInitialized)) THEN
    printf("=== Default Configuration Set ===\n");
  END;
  IF (Mode # Show) THEN
    IF (Mode = Config) THEN
      QueryConfig();
    END;
    WriteConfig();
  END;

  IF NOT fclose(M2File) THEN
    M2File := NIL;
    fatal("Error Writing To M2 File!");
  END;
  M2File := NIL;
END ConfigCompiler;

BEGIN
  Init();

  printf(Title);
  IF (argc < 2) OR (argc > 3) THEN
    fatal(MsgUsage);
  END;

  Mode := Config;
  NameArg := 1;
  IF (argc = 3) THEN
    ch := CAP(argv^[1]^[1]);
    IF ((argv^[1]^[0] # "-") OR (argv^[1]^[2] # 0C) OR
        ( (ch # "S") AND (ch # "D") ) ) THEN
      fatal(MsgUsage);
    END;

    IF (ch = "S") THEN (* -s, Show Option *)
      Mode := Show;
    ELSE (* -d, Set Defaults Option *)
      Mode := Defaults;
    END;
    NameArg := 2;
  END;

  M2FileName := argv^[NameArg];
  ConfigCompiler();

  Cleanup();
END M2Config.
