(* Name   : M2FM.MOD
 * Changed: 06/06/87 LF
 * Notes :
 * 1. Changed literal constants "0.0" to FLOAT(0) for easier portability
 *    between different floating point types!
 * 2. Functions have been changed to return result in D0 rather than on top
 *    of stack. Procedures "FOp1" and "FOp2" to handle floating point results
 *    return in reg D0.
 * 3. Turned off compiler switch "D".
 * 4. Changed procedure "FOp1" to set variable "pcLastProcResult" to the "pc"
 *    where the "MOVE.L D0,Dn" is generated.  Changed parameter pushing
 *    sequence to be more efficient by avoiding loading the parameter into D0
 *    before pushing on stack.
 * 5. Changed procedure "FOp2" to set variable "pcLastProcResult" to the "pc"
 *    where the "MOVE.L D0,Dn" is generated. Changed parameter pushing
 *    sequence to be more efficient by avoiding loading the parameters into D0
 *    before pushing on stack.  When the operation returns results in condcodes
 *    a special function is called "RestoreRegsSaveCC" to avoid loosing cond
 *    codes.
 * 6. In procedures "LoadF" and "FMove" commented out code to handle LONGREAL
 *    since they are not currently supported!
 *)

(*$L+,D- relocatable *)
IMPLEMENTATION MODULE M2FM;
  
  FROM SYSTEM IMPORT
          WORD (*, VAL*);
  FROM M2DM IMPORT
          ObjPtr, StrPtr, ConstValue, Object, Structure,
          notyp, undftyp, booltyp, chartyp, cardtyp,
          inttyp, bitstyp, lcardtyp, dbltyp, realtyp, lrltyp,
          proctyp, stringtyp, addrtyp, wordtyp, bytetyp,
          MaxCard, MinInt, MaxInt;
  FROM M2SM IMPORT
           Mark;
  FROM M2HM IMPORT
          D0, D1, SB, MP, SP,
          byte, word, long,
          Condition, RegType, Register,
          WidType, ItemMode, ItSet, Item,
          SimpleT, RealT, SetregMd, SetstkMd,
          GetReg, GetRegPair, Release, ReleaseReg,
          SaveRegs, RestoreRegs, Gea, Ext, StackTop,
          CallSystem, LoadD, LoadX, Move, SetbusyReg, pcLastProcResult;
  FROM M2LM IMPORT
          pc, PutWord;


  CONST
  
     (* System procedure numbers used by the compiler :          *)
     (* These numbers must correspond with the procedure numbers *)
     (* associated with a specific procedure in the definition   *)
     (* module 'System'.                                         *)

     BodyOfSystem        = 0; (* 0 is reserved for module body       *)
     HALTX               = 1; (* System.HALTX  = HALT-statement      *)
     MULU32              = 2; (* System.MULU32 = unsigned long MUL   *)
     DIVU32              = 3; (* System.DIVU32 = unsig. long DIV/MOD *)
     MULS32              = 4; (* System.MULS32 = signed long MUL     *)
     DIVS32              = 5; (* System.DIVS32 = signed long DIV/MOD *)
     FADDs               = 6; (* System.FADDs  = Floating ADD single *)
     FSUBs               = 7; (* System.FSUBs  = Floating SUB single *)
     FMULs               = 8; (* System.FMULs  = Floating MUL single *)
     FDIVs               = 9; (* System.FDIVs  = Floating DIV single *)
     FREMs               = 10;(* System.FREMs  = Floating REM single *)
     FCMPs               = 11;(* System.FDIVs  = Floating CMP single *)
     FNEGs               = 12;(* System.FNEGs  = Floating NEG single *)
     FABSs               = 13;(* System.FABSs  = Floating ABS single *)
     FLOATs              = 14;(* System.FLOATs = FLOAT single        *)
     TRUNCs              = 15;(* System.TRUNCs = TRUNC single        *)
     FADDd               = 16;(* System.FADDd  = Floating ADD double *)
     FSUBd               = 17;(* System.FSUBd  = Floating SUB double *)
     FMULd               = 18;(* System.FMULd  = Floating MUL double *)
     FDIVd               = 19;(* System.FDIVd  = Floating DIV double *)
     FREMd               = 20;(* System.FREMd  = Floating REM double *)
     FCMPd               = 21;(* System.FDIVd  = Floating CMP double *)
     FNEGd               = 22;(* System.FNEGd  = Floating NEG double *)
     FABSd               = 23;(* System.FABSd  = Floating ABS double *)
     FLOATd              = 24;(* System.FLOATd = FLOAT double        *)
     TRUNCd              = 25;(* System.TRUNCd = TRUNC double        *)
     FLONG               = 26;(* System.FLONG  = Floating single to double *)
     FSHORT              = 27;(* System.FSHORT = Floating double to single *)

  CONST

     MOVEMDEC = 044347B; (* MOVEM.L regs,-(SP) *)
     MOVEMSTD = 044300B; (* MOVEM.L regs,ea    *)
     MOVEMINC = 046337B; (* MOVEM.L (SP)+,regs *)
     MOVEMLDD = 046300B; (* MOVEM.L ea,regs    *)   
     MVEMSP   = 027400B; (* MOVE.L  ea,-(SP)   *)
     MVESPP   = 020037B; (* MOVE.L  (SP)+,ea   *)
     MOVEL    = 020000B; (* MOVE.L  ea,ea      *)
     MOVELIMM = 020074B; (* MOVE.L  #imm,ea    *)


  PROCEDURE err(n : CARDINAL);
    (* local synonym for M2SM.Mark to save space! *)
  BEGIN
     Mark(n);
  END err;

  PROCEDURE Put16(w : WORD);
    (* local synonym for M2LM.PutWord to save space! *)
  BEGIN
    PutWord(w);
  END Put16;

  PROCEDURE XSHIFT(x, n : CARDINAL) : CARDINAL;
  BEGIN
    WHILE n > 0 DO INC(x, x); DEC(n) END;
    RETURN x
  END XSHIFT;

  PROCEDURE SetfltMd(VAR x : Item; fR : Register; ftyp : StrPtr);
  BEGIN
    WITH x DO
      IF ftyp = realtyp THEN     (* for single real          *)
        SetregMd(x, fR, ftyp);   (* resulting mode = DregMd! *)
      ELSE 
        typ := ftyp;             (* for double real          *)
        mode := fltMd; FR := fR; (* resulting mode = fltMd ! *)
      END;
    END (*WITH*);
  END SetfltMd;

  PROCEDURE LoadF(VAR x : Item);
    (* Load x into a Floating-Point-Register.                        *)
    (* The current implementation simulates Floating-Point-Registers *)
    (* by means of one (single) D-Register or a (double) D-Register- *)
    (* Pair.                                                         *)
    VAR Dn : Register; ea : CARDINAL;
  BEGIN
    WITH x DO
      IF typ = realtyp THEN (* single real *) LoadD(x)
(* LongReals Not Supported Yet!
      ELSE (* double real *) 
        (* Assert( typ = lrltyp ); *)
        IF mode <= stkMd THEN
          GetRegPair(Dn);
          Gea(x,ea);
          IF mode = conMd THEN
            (* Note : NO immeditate's for MOVEM! *)
            Put16(MOVELIMM + Dn*1000B);
            Put16(val.D0); Put16(val.D1);
            Put16(MOVELIMM + (Dn+1)*1000B);
            Put16(val.D2); Put16(val.D3);
          ELSE 
            Put16(MOVEMLDD + ea);
            Put16(XSHIFT(3, Dn));
            Ext(x);
          END;
          Release(x); (* NOW release old registers! *)
          mode := fltMd; FR := Dn;
        ELSIF mode <> fltMd THEN
          err(239); Release(x);
          mode := fltMd; FR := D0;
        END;
*)
      END;
    END (*WITH*);
  END LoadF;

  PROCEDURE FMove(VAR x, y : Item);
    (* move REAL type x  --->>  REAL type y *)
    (* perform floating type moves :        *)
    (*        memory    to   memory         *)
    (*        register  to   memory         *)
    (*        memory    to   register       *)
    (* The current implementation simulates Floating-Point-Registers *)
    (* by means of one (single) D-Register or a (double) D-Register- *)
    (* Pair.                                                         *)

    VAR Dn : Register; ea : CARDINAL;
  BEGIN
    (* Assert( y.mode <> conMd ); *)
    (* Assert( x.typ  =  y.typ ); *)
    IF x.typ = realtyp THEN (* single real *) Move(x,y)
(* LongReal not supported yet!
    ELSIF (x.mode <> stkMd) OR (y.mode <> stkMd) THEN (* double real *)
      (* Assert( x.typ = lrltyp ); *)
      IF (x.mode <= stkMd) THEN
        (* Preload floating value to scratch D0/D1 : *)
        (* Don't waste D-pool-Registers !            *)
        WITH x DO
          IF mode = conMd THEN
            (* Note : NO immeditate's for MOVEM! *)
            Put16(MOVELIMM + D0*1000B);
            Put16(val.D0); Put16(val.D1);
            Put16(MOVELIMM + D1*1000B);
            Put16(val.D2); Put16(val.D3);
          ELSE
            Gea(x,ea); 
            Put16(MOVEMLDD + ea);
            Put16(3); (* register list for D0/D1 *)
            Ext(x);
            Release(x); (* NOW release old registers! *)
          END;
          mode := fltMd; FR := D0;
        END (*WITH x*);
      END (*Preload*);
      IF (x.mode <= stkMd) & (y.mode = fltMd) THEN
        (* memory to register : *)
        Dn := y.FR;
        Gea(x,ea);
        Put16(MOVEMLDD + ea);
        Put16(XSHIFT(3, Dn));
        Ext(x);
      ELSIF (x.mode = fltMd) & (y.mode <= stkMd) THEN
        (* register to memory : *)
        Dn := x.FR;
        IF y.mode = stkMd THEN
          Put16(MOVEMDEC);
          Put16(XSHIFT(3, 14 - Dn));
        ELSE
          Gea(y,ea);
          Put16(MOVEMSTD + ea);
          Put16(XSHIFT(3, Dn));
          Ext(y);
        END;
      ELSIF (x.mode = fltMd) & (y.mode = fltMd) THEN
        (* register to register : *)
        Dn := y.FR;
        IF x.FR <> Dn THEN
          Put16(MOVEL + Dn*1000B + x.FR);
          Put16(MOVEL + (Dn+1)*1000B + (x.FR+1));
        END;
      ELSE (* illegal modes *) err(241)
      END;
*)
    END (*double*);
  END FMove;

  PROCEDURE FOp1(op : CARDINAL; VAR x : Item);
    VAR regs : BITSET; y : Item; rtyp : StrPtr; sz : CARDINAL; Dn : Register;
  BEGIN
    (* Assert( RealT(x) ); *)       (* even for FLOATs/FLOATd *)
      CASE op OF
        (* define resulting type *)
        FNEGs,  FABSs  : rtyp := realtyp;
      | FNEGd,  FABSd  : rtyp := lrltyp;
      | TRUNCs, TRUNCd : rtyp := dbltyp;
      | FLOATs, FSHORT : rtyp := realtyp;
      | FLOATd, FLONG  : rtyp := lrltyp;
      END;

      (* Release Operand Temporarily to avoid unnecessarily saving registers. *)
      Release(x);
      SaveRegs(regs);               (* save busy registers *)

      (* Reset registers used by operand to busy! *)
      IF x.mode IN ItSet{RindMd,RidxMd,AregMd,DregMd} THEN
        SetbusyReg(x.R);
      END;
      IF x.mode = RidxMd THEN
        SetbusyReg(x.RX)
      END;

      SetstkMd(y, x.typ);
      FMove(x,y);                   (* push parameter onto stack *)
      Release(x);                   (* now release the parameter *)
      CallSystem(op);               (* call the function in System *)

      IF regs # {} THEN RestoreRegs(regs); END; (* restore busy registers *)

      (* result in reg D0 or D0/D1, must be copied to a pool register *)
      sz := VAL(CARDINAL,rtyp^.size);
      IF NOT (sz IN {4,8}) THEN err(200); END;
      IF sz IN {4} THEN (* long result D0 *)
        GetReg(Dn,Dreg);
        SetregMd(x, Dn, rtyp);
        SetregMd(y, D0, rtyp);

        (* Remember position of MOVE.L D0, Dn *)
        pcLastProcResult := pc;

        Move(y,x);
      ELSE (* sz = 8, double-longword result D0/D1 *)
        GetRegPair(Dn);
        x.mode := fltMd;
        x.FR   := Dn;
        y.typ  := rtyp;
        y.mode := fltMd;
        y.FR   := D0; (* D0/D1 *)
        FMove(y,x);
      END;
  END FOp1;

  (* Restore register but don't destroy Cond Codes! *)
  (* This is almost exactly like M2HM.RestoreRegs *)
  PROCEDURE RestoreRegsSaveCC(saveSet : BITSET);
    (* restore the registers given by saveSet. *)
    VAR r, lr : Register; x, reglist, n : CARDINAL;
  BEGIN
    r := D0; n := 0; x := 1; reglist := 0;
    REPEAT (* from D0 up to SP-1 *)
      IF r IN saveSet THEN
        INC(n); lr := r;
        INC(reglist, x);
      END;
      INC(x, x); INC(r);
    UNTIL r = SP + 8;
    IF saveSet <> {} THEN (* Always use MOVEM so that Cond Codes Not Destroyed*)
      Put16(MOVEMINC); Put16(reglist);
    END;
  END RestoreRegsSaveCC;

  PROCEDURE FOp2(op : CARDINAL; VAR x, y : Item);
    CONST D0 = 0; D7 = 7;
    VAR regs : BITSET; z : Item; rtyp : StrPtr; sz : CARDINAL; Dn : Register;
  BEGIN
    CASE op OF
      (* define resulting type *)
      FADDs, FSUBs, FMULs, FDIVs, FREMs : rtyp := realtyp;
    | FADDd, FSUBd, FMULd, FDIVd, FREMd : rtyp := lrltyp;
    | FCMPs, FCMPd                      : rtyp := notyp;
    END;

    (* Release Operands Temporarily to avoid unnecessarily saving registers. *)
    Release(x);
    Release(y);
    SaveRegs(regs);               (* save busy registers *)

    (* Reset registers used by operands to busy! *)
    IF x.mode IN ItSet{RindMd,RidxMd,AregMd,DregMd} THEN
      SetbusyReg(x.R);
    END;
    IF x.mode = RidxMd THEN SetbusyReg(x.RX) END;
    IF y.mode IN ItSet{RindMd,RidxMd,AregMd,DregMd} THEN
      SetbusyReg(y.R);
    END;
    IF y.mode = RidxMd THEN SetbusyReg(y.RX) END;

    SetstkMd(z, x.typ);
    FMove(x,z);                   (* push x-parameter onto stack *)
    Release(x);                   (* now release the x-parameter *)
    SetstkMd(z, y.typ);
    FMove(y,z);                   (* push y-parameter onto stack *)
    Release(y);                   (* now release the y-parameter *)
    CallSystem(op);               (* call the function in System *)

    (* This allows the Release(y) in GenOp() to do nothing! *)
    SetregMd(y, D0, rtyp);

    (* Caution : for FCMPs/FCMPd result is in the CCR :     *)
    (* -------   avoid the restoring of a single D-Register *)
    (*           (eventually done by M2HM.RestoreRegs)      *)
    (*           because this would destroy the CCR !       *)
    IF (regs # {}) THEN (* restore busy registers *)
      IF (regs * {D0..D7} # {}) AND (rtyp = notyp) THEN
        RestoreRegsSaveCC(regs); (* Does not destroy condition codes! *)
      ELSE
        RestoreRegs(regs);  (* normally restore busy registers *)
      END;
    END;
    
    (* result in reg D0 or D0/D1, must be copied to a pool register *)
    IF rtyp # notyp THEN
      sz := VAL(CARDINAL,rtyp^.size);
      IF NOT (sz IN {4,8}) THEN err(200); END;
      IF sz IN {4} THEN (* long result D0 *)
        GetReg(Dn,Dreg);
        SetregMd(x, Dn, rtyp);
        SetregMd(z, D0, rtyp);

        (* Remember position of MOVE.L D0, Dn *)
        pcLastProcResult := pc;

        Move(z,x);
      ELSE (* sz = 8, double-longword result D0/D1 *)
        GetRegPair(Dn);
        x.typ  := rtyp;
        x.mode := fltMd;
        x.FR   := Dn;
        z.typ  := rtyp;
        z.mode := fltMd;
        z.FR   := D0; (* D0/D1 *)
        FMove(z,x);
      END;
    END;
  END FOp2;

  PROCEDURE FNeg1(VAR x : Item);
  BEGIN
    IF x.typ = realtyp THEN FOp1(FNEGs,x) ELSE FOp1(FNEGd,x) END;
  END FNeg1;
  
  PROCEDURE FAbs1(VAR x : Item);
  BEGIN
    IF x.typ = realtyp THEN FOp1(FABSs,x) ELSE FOp1(FABSd,x) END;
  END FAbs1;
  
  PROCEDURE Float1(VAR x : Item);
    (* Int/Double to Single-Real conversion : *)
  BEGIN
    LoadX(x,long);
    x.typ := realtyp; (* essential for FOp1 ! *)
    FOp1(FLOATs,x);
  END Float1;
  
  PROCEDURE Trunc1(VAR x : Item);
    (* Single-Real to Int/Double conversion : *)
  BEGIN
    FOp1(TRUNCs,x);
    (* M2TM defines resulting type : Int or LongInt *)
    LoadD(x); (* pop long value from stack *)
  END Trunc1;

  PROCEDURE FloatD1(VAR x : Item);
    (* Int/Double to Long-Real conversion : *)
  BEGIN
    LoadX(x,long);
    x.typ := realtyp; (* essential for FOp1 ! *)
    FOp1(FLOATd,x);
  END FloatD1;
  
  PROCEDURE TruncD1(VAR x : Item);
    (* Long-Real to Double conversion : *)
  BEGIN
    FOp1(TRUNCd,x);
  END TruncD1;

  PROCEDURE FLong(VAR x : Item);
    (* Single-Real to Long-Real conversion : *)
  BEGIN
    FOp1(FLONG,x);
  END FLong;

  PROCEDURE FShort(VAR x : Item);
    (* Long-Real to Single-Real conversion : *)
  BEGIN
    FOp1(FSHORT,x);
  END FShort;

  PROCEDURE FAdd2(VAR x, y : Item);
    VAR i : CARDINAL;
  BEGIN
    IF x.typ = realtyp THEN i := FADDs ELSE i := FADDd END;
    FOp2(i,x,y);
  END FAdd2;
  
  PROCEDURE FSub2(VAR x, y : Item);
    VAR i : CARDINAL;
  BEGIN
    IF x.typ = realtyp THEN i := FSUBs ELSE i := FSUBd END;
    FOp2(i,x,y);
  END FSub2;
  
  PROCEDURE FMul2(VAR x, y : Item);
    VAR i : CARDINAL;
  BEGIN
    IF x.typ = realtyp THEN i := FMULs ELSE i := FMULd END;
    FOp2(i,x,y);
  END FMul2;
  
  PROCEDURE FDiv2(VAR x, y : Item);
    VAR i : CARDINAL;
  BEGIN
    IF x.typ = realtyp THEN
      IF (y.mode = conMd) & (y.val.R = FLOAT(0)) THEN err(205) END;
      i := FDIVs;
    ELSE
      i := FDIVd;
    END;
    FOp2(i,x,y);
  END FDiv2;
  
  PROCEDURE FRem2(VAR x, y : Item);
    VAR i : CARDINAL;
  BEGIN
    IF x.typ = realtyp THEN
      IF (y.mode = conMd) & (y.val.R = FLOAT(0)) THEN err(205) END;
      i := FREMs;
    ELSE
      i := FREMd;
    END;
    FOp2(i,x,y);
  END FRem2;

  PROCEDURE FCmp2(VAR x, y : Item);
    VAR i : CARDINAL;
  BEGIN
    IF x.typ = realtyp THEN i := FCMPs ELSE i := FCMPd END;
    FOp2(i,x,y);
  END FCmp2;


END M2FM.
