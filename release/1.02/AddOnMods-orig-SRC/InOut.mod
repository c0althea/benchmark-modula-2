(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: InOut.MOD                         Version: Amiga.00.00             *
 * Created: 11/19/86   Updated: 12/25/87   Author: Leon Frenkel             *
 * Description: Standard InOut module as described in appendix 2 of         *
 *  Programming in Modula-2 by N. Wirth.                                    *
 * Notes:                                                                   *
 * 1. Changed DEL to be BACKSPACE key.                                      *
 * 2. Added buffering of certain write functions via WriteBuf and FlushBuf. *
 * 3. Added Echo flag for controling echoing in ReadString().               *
 ****************************************************************************)

IMPLEMENTATION MODULE InOut;
  IMPORT Terminal;
  FROM SYSTEM IMPORT WORD;
  FROM FileSystem IMPORT File, Response, Lookup,
       ReadChar, ReadWord, WriteChar, WriteWord, Close;

(*$L+,D-*)

  CONST ESC = 33C; DEL = 10C;(*BackSpace Key*)  NL = 79;
  VAR ch: CHAR;
      name: ARRAY [0..NL] OF CHAR; (* also used as output buffer *)
      openIn, openOut: BOOLEAN;
      bufIdx: CARDINAL; (* buffer index *)

  (* ================================================================== *)
  (* Procedures For Improving Output Performance Of InOut Module        *)

  (* Flush Output Buffer *)
  PROCEDURE FlushBuf();
  BEGIN
    name[bufIdx] := 0C; (* terminate string *)
    WriteString(name);
    bufIdx := 0; (* reset buffer index *)
  END FlushBuf;

  (* Send Char To Output Buffer *)
  PROCEDURE WriteBuf(c: CHAR);
  BEGIN
    name[bufIdx] := c; INC(bufIdx);
    IF (bufIdx = HIGH(name)) THEN (* flush out buffer *)
      FlushBuf();
    END;
  END WriteBuf;
  (* ================================================================== *)

  PROCEDURE ReadName(ext: ARRAY OF CHAR);
    VAR i,j: CARDINAL;
  BEGIN
    i := 0;
    REPEAT Terminal.Read(ch);
      IF ch = DEL THEN
        IF i > 0 THEN
          Terminal.Write(DEL); (* Backup one char *)
          Terminal.Write(" "); (* Clear char *)
          Terminal.Write(DEL); (* Backup again *)
          DEC(i);
        END
      ELSIF (ch > " ") & (i < NL) THEN
        Terminal.Write(ch); name[i] := ch; INC(i);
      END
    UNTIL (ch # DEL) AND (ch <= " ");
    IF (0 < i) & (i < NL) & (name[i-1] = ".") THEN
      (*append extension*) j := 0;
      REPEAT name[i] := ext[j];
        Terminal.Write(ext[j]); INC(i); INC(j);
      UNTIL (j > CARDINAL(HIGH(ext))) OR (i = NL)
    END ;
    name[i] := 0C
  END ReadName;

  PROCEDURE OpenInput(defext: ARRAY OF CHAR);
  BEGIN
    REPEAT Terminal.WriteString("in> "); ReadName(defext);
      IF ch # ESC THEN
        Lookup(in,name,FALSE);
        IF in.res # done THEN
          Terminal.WriteString(" not found")
        END
      END ;
      Terminal.WriteLn
    UNTIL (in.res = done) OR (ch = ESC);
    openIn := ch # ESC; Done := openIn
  END OpenInput;

  PROCEDURE OpenOutput(defext: ARRAY OF CHAR);
  BEGIN
    REPEAT Terminal.WriteString("out> "); ReadName(defext);
      IF ch # ESC THEN
        Lookup(out,name,TRUE);
      END ;
      Terminal.WriteLn
    UNTIL (out.res = done) OR (ch = ESC);
    openOut := ch # ESC; Done := openOut
  END OpenOutput;

  PROCEDURE OpenInputFile(filename: ARRAY OF CHAR);
  BEGIN
    Lookup(in,filename,FALSE);
    Done   := in.res = done;
    openIn := Done;
  END OpenInputFile;

  PROCEDURE OpenOutputFile(filename: ARRAY OF CHAR);
  BEGIN
    Lookup(out,filename,TRUE);
    Done    := out.res = done;
    openOut := Done;
  END OpenOutputFile;

  PROCEDURE CloseInput;
  BEGIN
    IF openIn  THEN Close(in); openIn := FALSE END
  END CloseInput;

  PROCEDURE CloseOutput;
  BEGIN
    IF openOut THEN Close(out); openOut := FALSE END
  END CloseOutput;

  PROCEDURE Read(VAR ch: CHAR);
  BEGIN
    IF openIn THEN
      ReadChar(in,ch); Done := NOT in.eof
    ELSE Terminal.Read(ch)
    END
  END Read;

  PROCEDURE ReadWrd(VAR w: WORD);
  BEGIN
    IF openIn THEN
      ReadWord(in,w); Done := NOT in.eof
    ELSE Done := FALSE
    END
  END ReadWrd;

  PROCEDURE ReadString(VAR s: ARRAY OF CHAR);
    VAR i,n: CARDINAL; ch: CHAR;
  BEGIN i := 0; n := HIGH(s);
    IF openIn THEN
      REPEAT ReadChar(in,ch) UNTIL (ch > " ") OR in.eof;
      REPEAT
        IF i <= n THEN
          s[i] := ch; INC(i);
        END ;
        ReadChar(in,ch)
      UNTIL ch <= " "
    ELSE
      REPEAT Terminal.Read(ch) UNTIL (ch > " ") OR (ch = ESC);
      WHILE (ch = DEL) OR (ch > " ") DO
        IF ch = DEL THEN
          IF i > 0 THEN
            DEC(i);
            IF (Echo) THEN
              Terminal.Write(DEL); (* Backup one char *)
              Terminal.Write(" "); (* Clear char *)
              Terminal.Write(DEL); (* Backup again *)
            END;
          END
        ELSE
          IF i <= n THEN s[i] := ch; INC(i); 
            IF (Echo) THEN Terminal.Write(ch); END;
          END
        END ;
        Terminal.Read(ch)
      END
    END ;
    IF i <= n THEN s[i] := 0C END ;
    termCH := ch
  END ReadString;

  PROCEDURE ReadInt(VAR x: INTEGER);
    VAR i: INTEGER; n: CARDINAL;
      ch: CHAR; neg: BOOLEAN;
      buf: ARRAY [0..9] OF CHAR;

    PROCEDURE next;
    BEGIN ch := buf[n]; INC(n);
    END next;

  BEGIN ReadString(buf); n := 0; next;
    WHILE ch = " " DO next END ;
    IF ch = "-" THEN
      neg := TRUE; next
    ELSE neg := FALSE;
      IF ch = "+" THEN next END
    END ;
    IF ("0" <= ch) & (ch <= "9") THEN
      i := 0; Done := TRUE;
      REPEAT i := 10*i + (INTEGER(ch) - 60B); next
      UNTIL (ch < "0") OR ("9" < ch);
      IF neg THEN x := -i ELSE x := i END
    ELSE Done := FALSE
    END
  END ReadInt;

  PROCEDURE ReadCard(VAR x: CARDINAL);
    VAR i,n: CARDINAL;
      ch: CHAR;
      buf: ARRAY [0..9] OF CHAR;

    PROCEDURE next;
    BEGIN ch := buf[n]; INC(n);
    END next;

  BEGIN ReadString(buf); n := 0; next;
    WHILE ch = " " DO next END ;
    IF ("0" <= ch) & (ch <= "9") THEN
      i := 0; Done := TRUE;
      REPEAT i := 10*i + (CARDINAL(ch) - 60B); next
      UNTIL (ch < "0") OR ("9" < ch);
      x := i
    ELSE Done := FALSE
    END
  END ReadCard;

  PROCEDURE Write(ch: CHAR);
  BEGIN
    IF openOut THEN WriteChar(out,ch)
      ELSE Terminal.Write(ch)
    END
  END Write;

  PROCEDURE WriteWrd(w: WORD);
  BEGIN
    IF openOut THEN WriteWord(out,w)
      ELSE Done := FALSE
    END
  END WriteWrd;

  PROCEDURE WriteLn;
  BEGIN
    IF openOut THEN WriteChar(out, EOL)
      ELSE Terminal.WriteLn
    END
  END WriteLn;

  PROCEDURE WriteString(s: ARRAY OF CHAR);
    VAR i: CARDINAL; ch: CHAR;
  BEGIN
    IF openOut THEN
      i := 0;
      WHILE (i <= HIGH(s)) AND (s[i] # 0C) DO
        WriteChar(out, s[i]);
        INC(i);
      END;
    ELSE Terminal.WriteString(s)
    END
  END WriteString;

  PROCEDURE WriteInt(x: INTEGER; n: CARDINAL);
    VAR i, x0: CARDINAL;
      a: ARRAY [0..6] OF CHAR;
  BEGIN i := 0; x0 := ABS(x);
    REPEAT a[i] := CHAR(x0 MOD 10 + 60B);
      x0 := x0 DIV 10; INC(i);
    UNTIL x0 = 0;
    IF x < 0 THEN a[i] := "-"; INC(i); END ;
    WHILE n > i DO
      DEC(n); WriteBuf(" ")
    END ;
    REPEAT DEC(i); WriteBuf(a[i]) UNTIL i = 0;
    FlushBuf();
  END WriteInt;

  PROCEDURE WriteCard(x,n: CARDINAL);
    VAR i: CARDINAL;
      a: ARRAY [0..6] OF CARDINAL;
  BEGIN i := 0;
    REPEAT a[i] := x MOD 10; x := x DIV 10; INC(i);
    UNTIL x = 0;
    WHILE n > i DO
      DEC(n); WriteBuf(" ")
    END ;
    REPEAT DEC(i); WriteBuf(CHAR(a[i]+60B)) UNTIL i = 0;
    FlushBuf();
  END WriteCard;

  PROCEDURE WriteOct(x,n: CARDINAL);
    VAR i: CARDINAL;
      a: ARRAY [0..6] OF CARDINAL;
  BEGIN i := 0;
    REPEAT a[i] := x MOD 8; x := x DIV 8; INC(i);
    UNTIL i = 5;
    IF x = 0 THEN a[i] := 0 ELSE a[i] := 1 END ;
    INC(i);
    WHILE n > i DO
      DEC(n); WriteBuf(" ")
    END ;
    REPEAT DEC(i); WriteBuf(CHAR(a[i]+60B)) UNTIL i = 0;
    FlushBuf();
  END WriteOct;

  PROCEDURE WriteHex(x,n: CARDINAL);

    PROCEDURE HexDig(d: CARDINAL);
    BEGIN d := d MOD 16;
      IF d < 10 THEN INC(d, 60B); ELSE INC(d, 67B); END ;
      WriteBuf(CHAR(d))
    END HexDig;

  BEGIN
    WHILE n > 4 DO
      DEC(n); WriteBuf(" ")
    END ;
    HexDig(x DIV 1000H); HexDig(x DIV 100H);
    HexDig(x DIV 10H);  HexDig(x);
    FlushBuf();
  END WriteHex;

BEGIN
  (* openIn := FALSE; openOut := FALSE; Done := FALSE *)
  (* bufIdx := 0; *)
  Echo := TRUE;
END InOut.

