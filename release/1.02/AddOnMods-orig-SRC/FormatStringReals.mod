(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: FormatStringReals.MOD             Version: Amiga.00.00             *
 * Created: 02/23/87   Updated: 02/24/87   Author: Leon Frenkel             *
 * Description: By importing this module the Format() function in module    *
 *  FormatString will suport formating of the REAL type using %e,%f,%g.     *
 ****************************************************************************)

IMPLEMENTATION MODULE FormatStringReals;

FROM SYSTEM IMPORT ADR, BYTE, LONGWORD;
FROM FormatString IMPORT RealConvFunc;
FROM RealConversions IMPORT ConvRealToString, RealToStringFormat;

(*$L+*)

TYPE
RealConvFuncType=PROCEDURE(REAL,VAR ARRAY OF CHAR,CARDINAL,RealToStringFormat);
VAR
  a : POINTER TO RealConvFuncType;
BEGIN
  a := ADR(RealConvFunc);
  a^ := ConvRealToString;
END FormatStringReals.
