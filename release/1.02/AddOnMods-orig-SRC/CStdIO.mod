(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CStdIO.MOD                        Version: Amiga.00.00             *
 * Created: 02/20/87   Updated: 02/23/87   Author: Leon Frenkel             *
 * Description: From the "C" language standard library, functions for       *
 *  standard IO. These functions perform buffered IO.                       *
 ****************************************************************************)

IMPLEMENTATION MODULE CStdIO;

FROM SYSTEM   IMPORT ADDRESS, ADR, BYTE, TSIZE;
FROM AmigaDOS IMPORT Open, IsInteractive, ModeOldFile;
FROM CFileIO  IMPORT FileDesc, FileDescRecord,
                     open, close, read, write, lseek, 
                     errno, ENOMEM, EINVAL, EOF, oNOCLOSE,
                     oRDONLY, oWRONLY, oRDWR, oCREAT, oTRUNC, oEXCL, oAPPEND;
FROM Memory   IMPORT AllocMem, FreeMem, MemReqSet, MemClear;
FROM System   IMPORT StdInput, StdOutput;

(*$L+*)

CONST
  LF = 012C;

(* PRIVATE functions *)

PROCEDURE AllocBuffer(fp: FILE);
VAR
  newBufPtr : ADDRESS;
BEGIN
  LOOP
    IF IsInteractive(fp^.fdesc^.handle) THEN
      EXIT; (* unbuffered *)
    END;  
    newBufPtr := AllocMem(BufferSize, MemReqSet{});
    IF newBufPtr = NIL THEN
      EXIT; (* unbuffered *)
    END;
    WITH fp^ DO
      blength := BufferSize;
      INCL(flags, fBUF);
      bbase := newBufPtr;
    END;
    RETURN;
  END; (* LOOP *)
  WITH fp^ DO
    blength := 1D;
    bbase := ADR(bchar);
  END;
  RETURN;
END AllocBuffer;

PROCEDURE FlushBuffer(fp: FILE; opt: INTEGER): INTEGER;
VAR
  size : LONGINT;
  ch   : CHAR;
BEGIN
  WITH fp^ DO
    LOOP
      IF fIOERR IN flags THEN
        RETURN(EOF);
      END;
      IF fDIRTY IN flags THEN
        size := bp - bbase;
        IF write(fdesc, bbase, size) # size THEN
          EXIT; (* error *)
        END;
      END;
      IF opt = -1 THEN
        EXCL(flags, fDIRTY);
        bend := NIL;
        bp   := NIL;
        RETURN (0);
      END;
      IF bbase = NIL THEN
        AllocBuffer(fp);
      END;
      IF blength = 1D THEN
        ch := CHR(opt);
        IF write(fdesc, ADR(ch), 1D) # 1D THEN
          EXIT; (* error *)
        END;
        RETURN (opt);
      END;
      bp := bbase;
      bend := bbase + blength;
      INCL(flags, fDIRTY);
      bp^ := CHR(opt);
      INC(bp);
      RETURN (INTEGER(CHR(opt))); (* opt & $00FF *)
    END; (* LOOP *)
    INCL(flags, fIOERR);
    bend := NIL;
    bp   := NIL;
    RETURN(EOF);
  END;
END FlushBuffer;

(* PUBLIC functions *)

PROCEDURE clearerr(stream: FILE);
BEGIN
  WITH stream^ DO
    EXCL(flags, fIOERR);
    EXCL(flags, fEOF);
  END;
END clearerr;

PROCEDURE closestdio;
VAR
  r : BOOLEAN;
BEGIN
  r := fclose(stdin);
  r := fclose(stdout);
  r := fclose(stderr);
END closestdio;

PROCEDURE fclose(stream: FILE): BOOLEAN;
VAR
  err : BOOLEAN;
BEGIN
  err := FALSE;
  WITH stream^ DO
    IF fDIRTY IN flags THEN
      err := FlushBuffer(stream,-1) # 0;
    END;
    IF NOT close(fdesc) THEN
      err := TRUE; (* error *)
    END;
    IF fBUF IN flags THEN
      FreeMem(bbase, blength);
    END;
  END; (* WITH *)
  FreeMem(stream, TSIZE(FILERecord));
  RETURN NOT err; (* return: TRUE = ok, FALSE = err *)
END fclose;

PROCEDURE fdopen(file: FileDesc): FILE;
VAR
  fp : FILE;
BEGIN
  fp := AllocMem(TSIZE(FILERecord), MemReqSet{MemClear});
  IF fp = NIL THEN
    errno := ENOMEM;
    RETURN (NIL);
  END;
  fp^.fdesc := file;
  RETURN (fp);
END fdopen;

PROCEDURE feof(stream: FILE): BOOLEAN;
BEGIN
  RETURN (fEOF IN stream^.flags);
END feof;

PROCEDURE ferror(stream: FILE): BOOLEAN;
BEGIN
  RETURN (fIOERR IN stream^.flags)
END ferror;

PROCEDURE fflush(stream: FILE): BOOLEAN;
BEGIN
  RETURN (FlushBuffer(stream, -1) = 0);
END fflush;

PROCEDURE fgets(string: ADDRESS; n: INTEGER; stream: FILE): ADDRESS;
VAR
  ich : INTEGER;
  p   : ADDRESS;
BEGIN
  p := string;
  LOOP
    DEC(n);
    IF n <= 0 THEN EXIT; END;
    ich := getc(stream);
    IF ich # EOF THEN
      p^ := CHR(ich);
      INC(p);
      IF ich = ORD(LF) THEN EXIT; END;
    ELSE (* eof *)
      EXIT;
    END;
  END; (* LOOP *)
  p^ := 0C;
  IF (ich = EOF) AND (p = string) THEN
    string := NIL; (* error *)
  END;
  RETURN(string); (* NIL = error,  original string ptr = no error *)
END fgets;

PROCEDURE fileno(stream: FILE): FileDesc;
BEGIN
  RETURN (stream^.fdesc);
END fileno;

PROCEDURE fopen(name: ADDRESS; mode: ADDRESS): FILE;
VAR
  mode0, mode1, mode2 : CHAR;
  fmode : BITSET;
  fp : FILE;
  file : FileDesc;
BEGIN
  mode0 := CHAR(mode^);
  INC(mode);
  mode1 := CHAR(mode^);
  INC(mode);
  mode2 := CHAR(mode^);

  IF (mode1 = "+") AND (mode2 = 0C) THEN
    IF (mode0 = "r") THEN
      fmode := oRDWR;
    ELSIF (mode0 = "w") THEN
      fmode := oRDWR + oCREAT + oTRUNC;
    ELSIF (mode0 = "a") THEN
      fmode := oRDWR + oCREAT + oAPPEND;
    ELSE
      errno := EINVAL;
      RETURN(NIL);
    END;
  ELSIF (mode1 = 0C) THEN
    IF (mode0 = "r") THEN
      fmode := oRDONLY;
    ELSIF (mode0 = "w") THEN
      fmode := oWRONLY + oCREAT + oTRUNC;
    ELSIF (mode0 = "a") THEN
      fmode := oWRONLY + oCREAT + oAPPEND;
    ELSE
      errno := EINVAL;
      RETURN(NIL);
    END;
  ELSE
    errno := EINVAL;
    RETURN(NIL);
  END;

  fp := AllocMem(TSIZE(FILERecord), MemReqSet{MemClear});
  IF fp = NIL THEN
    errno := ENOMEM;
    RETURN (NIL);
  END;

  file := open(name, fmode);
  IF file = NIL THEN
    FreeMem(fp, TSIZE(FILERecord));
    RETURN(NIL);
  END;
  fp^.fdesc := file;    
  RETURN (fp);
END fopen;

PROCEDURE fputs(string: ADDRESS; stream: FILE): INTEGER;
BEGIN
  WHILE CHAR(string^) # 0C DO
    IF (putc(CHAR(string^), stream) = EOF) THEN
      RETURN (EOF);
    END;
    INC(string);
  END;
  RETURN (0);
END fputs;

PROCEDURE fread(buffer: ADDRESS; size, count: LONGINT; stream: FILE): LONGINT;
VAR
  rcount : LONGINT;
  isize  : LONGINT;
  ich    : INTEGER;
BEGIN
  rcount := 0D;
  WHILE count # 0D DO
    isize := size;
    WHILE isize # 0D DO
      ich := getc(stream);
      IF ich = EOF THEN
        RETURN (rcount);
      END;
      buffer^ := CHR(ich);
      INC(buffer);
      DEC(isize);
    END;
    INC(rcount);
    DEC(count);
  END;
  RETURN (rcount);
END fread;

PROCEDURE fseek(stream: FILE; offset: LONGINT; origin: INTEGER): BOOLEAN;
BEGIN
  WITH stream^ DO
    EXCL(flags, fEOF);
    IF fDIRTY IN flags THEN
      IF FlushBuffer(stream, -1) # 0 THEN
        RETURN (FALSE); (* error *)
      END;
    ELSIF (origin = 1) AND (bp # NIL) THEN
      DEC(offset, bend - bp);
    END;
    bp   := NIL;
    bend := NIL;
    RETURN (lseek(fdesc, offset, origin) >= 0D); (* T = no err, F = err *)
  END; (* WITH *)
END fseek;

PROCEDURE ftell(stream: FILE): LONGINT;
VAR
  cpos : LONGINT;
BEGIN
  WITH stream^ DO
    cpos := lseek(fdesc, 0D, 1);
    IF fDIRTY IN flags THEN
      INC(cpos, bp - bbase);
    ELSIF bp # NIL THEN
      DEC(cpos, bend - bp);
    END;
    RETURN (cpos);
  END;
END ftell;

PROCEDURE fwrite(buffer: ADDRESS; size, count: LONGINT; stream: FILE): LONGINT;
VAR
  rcount : LONGINT;
  isize  : LONGINT;
BEGIN
  rcount := 0D;
  WHILE count # 0D DO
    isize := size;
    WHILE isize # 0D DO
      IF putc(CHAR(buffer^), stream) = EOF THEN
        RETURN (rcount);
      END;
      INC(buffer);
      DEC(isize);
    END;
    INC(rcount);
    DEC(count);
  END;
  RETURN (rcount);
END fwrite;

PROCEDURE getc(stream: FILE): INTEGER;
VAR
  res : INTEGER;
  l   : LONGINT;
BEGIN
  WITH stream^ DO
    IF bp >= bend THEN
      IF flags >= FileFlagsSet{fEOF, fIOERR} THEN
        RETURN (EOF);
      END;
      EXCL(flags, fDIRTY);
      IF bbase = NIL THEN
        AllocBuffer(stream);
      END;
      l := read(fdesc, bbase, blength);
      IF l < 0D THEN (* -1 means error *)
        INCL(flags, fIOERR);
        RETURN (EOF);
      ELSIF l = 0D THEN
        INCL(flags, fEOF);
        RETURN (EOF);
      END;
      bp := bbase;
      bend := bbase + LONGCARD(l);
    END;
    res := INTEGER(bp^);
    INC(bp);
    RETURN (res);
  END;
END getc;

PROCEDURE getchar(): INTEGER;
BEGIN
  RETURN getc(stdin);
END getchar;

PROCEDURE gets(string: ADDRESS): ADDRESS;
VAR
  p   : ADDRESS;
  ich : INTEGER;
BEGIN
  p := string;
  LOOP
    ich := getchar();
    IF (ich # EOF) AND (ich # ORD(LF)) THEN
      p^ := CHR(ich);
      INC(p);
    ELSE (* eof or '\n' *)
      EXIT;
    END;
  END; (* LOOP *)
  p^ := 0C;
  IF (ich = EOF) AND (p = string) THEN
    string := NIL; (* error *)
  END;
  RETURN(string); (* NIL = error, string = no error *)
END gets;

PROCEDURE getw(stream: FILE): INTEGER;
VAR
  ich1, ich2 : INTEGER;
BEGIN
    ich1 := getc(stream);
    ich2 := getc(stream);
    IF (ich1 = EOF) OR (ich2 = EOF) THEN
      RETURN (EOF);
    END;
    RETURN ((CARDINAL(ich1 * 256)) + CARDINAL(ich2));
END getw;

PROCEDURE putc(c: CHAR; stream: FILE): INTEGER;
BEGIN
  WITH stream^ DO
    IF bp >= bend THEN
      RETURN (FlushBuffer(stream, INTEGER(c)));
    END;
    bp^ := c;
    INC(bp);
    RETURN (INTEGER(c));
  END;
END putc;

PROCEDURE putchar(c: CHAR): INTEGER;
BEGIN
  RETURN putc(c, stdout);
END putchar;

PROCEDURE puts(string: ADDRESS): INTEGER;
BEGIN
  WHILE CHAR(string^) # 0C DO
    IF (putchar(CHAR(string^)) = -1) THEN
      RETURN (-1);
    END;
    INC(string);
  END;
  RETURN putchar(LF);
END puts;

PROCEDURE putw(word: INTEGER; stream: FILE): INTEGER;
BEGIN
  IF (putc(CHR(CARDINAL(word) DIV 256), stream) < 0) THEN
    RETURN (EOF);
  END;
  IF (putc(CHR(word), stream) < 0) THEN
    RETURN (EOF);
  END;
  RETURN (word);
END putw;

PROCEDURE setbuf(stream: FILE; buffer: ADDRESS; size: LONGCARD);
BEGIN
  WITH stream^ DO
    IF buffer # NIL THEN
      bbase := buffer;
      blength := size;
    ELSE
      bbase := ADR(bchar);
      blength := 1D;
    END;
  END;
END setbuf;

PROCEDURE ungetc(c: CHAR; stream: FILE): INTEGER;
BEGIN
  WITH stream^ DO
    IF bp <= bbase THEN
      RETURN (EOF);
    END;
    DEC(bp);
    bp^ := c;
    RETURN (INTEGER(c));
  END;
END ungetc;

PROCEDURE openstdio;
VAR
  aFileDesc : FileDesc;
BEGIN
  aFileDesc := AllocMem(TSIZE(FileDescRecord), MemReqSet{});
  WITH aFileDesc^ DO
    handle := StdInput;
    mode   := oNOCLOSE + oRDONLY;
  END;
  stdin := fdopen(aFileDesc);
  AllocBuffer(stdin);

  aFileDesc := AllocMem(TSIZE(FileDescRecord), MemReqSet{});
  WITH aFileDesc^ DO
    handle := StdOutput;
    mode   := oNOCLOSE + oWRONLY;
  END;
  stdout := fdopen(aFileDesc);
  AllocBuffer(stdout);

  aFileDesc := AllocMem(TSIZE(FileDescRecord), MemReqSet{});
  WITH aFileDesc^ DO
    handle := Open(ADR("*"), ModeOldFile);
    mode   := oWRONLY;
  END;
  stderr := fdopen(aFileDesc);
  AllocBuffer(stderr);
END openstdio;

BEGIN (* main *)
  BufferSize := BUFSIZ;

END CStdIO.
