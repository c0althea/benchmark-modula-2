(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: ILBMR.MOD                         Version: Amiga.00.00             *
 * Created: 04/21/87   Updated: 12/25/87   Author: Leon Frenkel             *
 * Description: Functions for reading InterLeaved BitMap raster image.      *
 ****************************************************************************)

IMPLEMENTATION MODULE ILBMR;

FROM SYSTEM   IMPORT ADR, ADDRESS, BYTE, TSIZE, SHIFT;
FROM Graphics IMPORT BitMap;
FROM IFF      IMPORT IFFP, GroupContext, ClientError, IFFOkay, szNotYetKnown,
		     BadFORM, ChunkMoreBytes;
FROM IFFR     IMPORT IFFReadBytes;
FROM ILBM     IMPORT BitMapHeader, Point2D, DestMerge, SpritePrecedence,
		     ColorRegister, sizeofColorRegister, MaxAmDepth,
		     Masking, Compression, cmpByteRun1, cmpNone,
		     IDBMHD, IDGRAB, IDDEST, IDSPRT, IDCMAP, IDBODY,
		     x320x200Aspect, y320x200Aspect, x320x400Aspect,
		     y320x400Aspect, x640x200Aspect, y640x200Aspect,
		     x640x400Aspect, y640x400Aspect,
		     RowBytes, mskHasMask;
FROM Memory   IMPORT CopyMem;
FROM Packer   IMPORT MaxPackedSize;
FROM UnPacker IMPORT UnPackRow;  

(*$L+*)

PROCEDURE GetBMHD(VAR con: GroupContext; VAR bmHdr: BitMapHeader): IFFP;
BEGIN
  RETURN IFFReadBytes(con, ADR(bmHdr), TSIZE(BitMapHeader));
END GetBMHD;

PROCEDURE GetGRAB(VAR con: GroupContext; VAR point2D: Point2D): IFFP;
BEGIN
  RETURN IFFReadBytes(con, ADR(point2D), TSIZE(Point2D));
END GetGRAB;

PROCEDURE GetDEST(VAR con: GroupContext; VAR destMerge: DestMerge): IFFP;
BEGIN
  RETURN IFFReadBytes(con, ADR(destMerge), TSIZE(DestMerge));
END GetDEST;

PROCEDURE GetSPRT(VAR con: GroupContext; VAR sprPrec: SpritePrecedence): IFFP;
BEGIN
  RETURN IFFReadBytes(con, ADR(sprPrec), TSIZE(SpritePrecedence));
END GetSPRT;

(* pNColorRegs is passed in as a pointer to the number of ColorRegisters
 * caller has space to hold.  GetCMAP sets to the number actually read.*)
PROCEDURE GetCMAP(VAR ilbmContext: GroupContext; colorMap: ADDRESS;
                  VAR pNColorRegs: BYTE): IFFP;
VAR
  nColorRegs : BYTE;
  iffp : IFFP;
  colorReg : ColorRegister;
  cMap : POINTER TO CARDINAL;
BEGIN
  cMap := colorMap;

  nColorRegs := BYTE(ilbmContext.ckHdr.ckSize DIV LONGINT(sizeofColorRegister));
  IF CARDINAL(pNColorRegs) < CARDINAL(nColorRegs) THEN
    nColorRegs := pNColorRegs;
  END;
  pNColorRegs := nColorRegs;  (* Set to the number actually there. *)

  WHILE CARDINAL(nColorRegs) > 0 DO
    iffp := IFFReadBytes(ilbmContext, ADR(colorReg), sizeofColorRegister);
    IF iffp # IFFOkay THEN (* CheckIFFP() *)
      RETURN (iffp);
    END;
    cMap^ := ((CARDINAL(colorReg.red  ) DIV 16) * 256) +
             ((CARDINAL(colorReg.green) DIV 16) *  16) +
             ((CARDINAL(colorReg.blue ) DIV 16)      );

    INC(ADDRESS(cMap), TSIZE(CARDINAL));
    DEC(CHAR(nColorRegs));
  END;

  RETURN (IFFOkay);
END GetCMAP;


(* NOTE: This implementation could be a LOT faster if it used more of the
 * supplied buffer. It would make far fewer calls to IFFReadBytes (and
 * therefore to DOS Read) and to movemem. *)
PROCEDURE GetBODY(VAR context: GroupContext;  VAR bitmap: BitMap;
                  mask: ADDRESS;  VAR bmHdr: BitMapHeader;
                  buffer: ADDRESS;  bufsize: LONGINT): IFFP;
VAR
  iffp : IFFP;
  srcPlaneCnt : CARDINAL;
  srcRowBytes : CARDINAL;
  bufRowBytes : LONGINT;
  nRows       : CARDINAL;
  compression : Compression;
  iPlane, iRow, nEmpty : CARDINAL;
  nFilled : CARDINAL;
  buf, nullDest, nullBuf : ADDRESS;
  pDest : POINTER TO ADDRESS;
  planes : ARRAY [0..MaxSrcPlanes-1] OF ADDRESS; (* Array of ptrs to planes/mask*)
BEGIN
  srcPlaneCnt := CARDINAL(bmHdr.nPlanes);
  srcRowBytes := RowBytes(bmHdr.w);
  bufRowBytes := MaxPackedSize(srcRowBytes);
  nRows := bmHdr.h;
  compression := bmHdr.compression;

  IF CARDINAL(compression) > CARDINAL(cmpByteRun1) THEN
    RETURN (ClientError);
  END;

  (* Complain if client asked for a conversion GetBODY doesn't handle.*)
  IF (srcRowBytes # bitmap.BytesPerRow) OR
     (bufsize < bufRowBytes * 2D) OR
     (srcPlaneCnt > MaxSrcPlanes) THEN
    RETURN (ClientError);
  END;

  IF nRows > bitmap.Rows THEN
    nRows := bitmap.Rows;
  END;

  (* Initialize array "planes" with bitmap ptrs; NULL in empty slots.*)
  iPlane := 0;
  WHILE iPlane < CARDINAL(bitmap.Depth) DO
    planes[iPlane] := bitmap.Planes[iPlane];

    INC(iPlane);
  END;

  WHILE iPlane < MaxSrcPlanes DO
    planes[iPlane] := NIL;

    INC(iPlane);
  END;

  (* Copy any mask plane ptr into corresponding "planes" slot.*)
  IF CARDINAL(bmHdr.masking) = CARDINAL(mskHasMask) THEN
    IF mask # NIL THEN
      planes[srcPlaneCnt] := mask;  (* If there are more srcPlanes than
               * dstPlanes, there will be NULL plane-pointers before this.*)
    ELSE
      planes[srcPlaneCnt] := NIL;  (* In case more dstPlanes than src.*)
    END;
    INC(srcPlaneCnt);  (* Include mask plane in count.*)
  END;

  (* Setup a sink for dummy destination of rows from unwanted planes.*)
  nullDest := buffer;
  INC(buffer, srcRowBytes);
  DEC(bufsize,srcRowBytes);

  (* Read the BODY contents into client's bitmap.
   * De-interleave planes and decompress rows.
   * MODIFIES: Last iteration modifies bufsize.*)
  buf := buffer + LONGCARD(bufsize); (* Buffer is currently empty. *)
  iRow := nRows;
  WHILE iRow > 0 DO
    iPlane := 0;
    WHILE iPlane < srcPlaneCnt DO
      pDest := ADR(planes[iPlane]);

      (* Establish a sink for any unwanted plane.*)
      IF pDest^ = NIL THEN
        nullBuf := nullDest;
        pDest   := ADR(nullBuf);
      END;

      (* Read in at least enough bytes to uncompress next row.*)
      nEmpty := buf - buffer;		(* size of empty part of buffer. *)
      nFilled := bufsize - LONGINT(nEmpty); (* this part has data. *)
      IF (LONGINT(nFilled) < bufRowBytes) THEN
	(* Need to read more.*)

        (* Move the existing data to the front of the buffer.*)
	(* Now covers range buffer[0]..buffer[nFilled-1].*)
        CopyMem(buf, buffer, nFilled); (* Could be moving 0 bytes *)

        IF LONGINT(nEmpty) > ChunkMoreBytes(context) THEN
          (* There aren't enough bytes left to fill the buffer.*)
          nEmpty := ChunkMoreBytes(context);
          bufsize := nFilled + nEmpty; 
        END;

        (* Append new data to the existing data.*)
        iffp := IFFReadBytes(context, buffer + LONGCARD(nFilled), nEmpty);
        IF iffp # IFFOkay THEN (* CheckIFFP() *)
          RETURN (iffp);
        END;

        buf     := buffer;
        nFilled := bufsize;
        nEmpty  := 0;
      END;

      (* Copy uncompressed row to destination plane.*)
      IF CARDINAL(compression) = CARDINAL(cmpNone) THEN
        IF nFilled < srcRowBytes THEN
          RETURN (BadFORM);
        END;
        CopyMem(buf, pDest^, srcRowBytes);
        INC(buf, srcRowBytes);
        INC(pDest^, srcRowBytes);
      ELSE (* Decompress row to destination plane.*)
        IF UnPackRow(buf, pDest^, nFilled, srcRowBytes) THEN
          RETURN (BadFORM);
        END;
      END;

      INC(iPlane);
    END;

    DEC(iRow);
  END;

  RETURN (IFFOkay);
END GetBODY;

END ILBMR.
