(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: IFFR.MOD                          Version: Amiga.00.00             *
 * Created: 04/20/87   Updated: 04/20/87   Author: Leon Frenkel             *
 * Description: IFF file read functions.                                    * 
 ****************************************************************************)

IMPLEMENTATION MODULE IFFR;

FROM SYSTEM   IMPORT ADR, ADDRESS, TSIZE;
FROM AmigaDOS IMPORT FileHandle, OffsetEnd, OffsetCurrent, OffsetBeginning;
FROM IFF      IMPORT ChunkMoreBytes,
		     ClientFramePtr,
		     EndMark, ShortChunk, ClientError,
		     IFFP, ID, GroupContext, NullChunk, ChunkHeader,
		     NoFile, DOSError, IFFOkay, NotIFF, BadIFF,
		     FORM, CAT, LIST, PROP;
FROM GIO      IMPORT GSeek, GRead;

(*$L+*)


(* ======== PRIVATE ======== *)


(* Returns the length of the file or else a negative IFFP error code
 * (NO_FILE or DOS_ERROR). AmigaDOS-specific implementation.
 * SIDE EFFECT: Thanks to AmigaDOS, we have to change the file's position
 * to find its length.
 * Now if Amiga DOS maintained fh_End, we'd just do this:
 *    fileLength = (FileHandle* )BADDR(file)->fh_End; *)
PROCEDURE FileLength(file: FileHandle): LONGINT;
VAR
  fileLength : LONGINT;
  r          : LONGINT;
BEGIN
  fileLength := NoFile;

  IF (file > 0D) THEN
    r := GSeek(file, 0D, OffsetEnd);	(* Seek to end of file. *)
    fileLength := GSeek(file, 0D, OffsetCurrent);
    (* Returns position BEFORE the seek, which is #bytes in file. *)
    IF (fileLength < 0D) THEN
      fileLength := DOSError;	(* DOS being absurd. *)
    END;
  END;

  RETURN (fileLength);
END FileLength;

(* Skip over bytes in a context. Won't go backwards.
 * Updates context->position but not context->bytesSoFar.
 * This implementation is AmigaDOS specific.*)
PROCEDURE SkipFwd(VAR context: GroupContext; bytes: LONGINT): IFFP;
VAR
  iffp : IFFP;
BEGIN
  iffp := IFFOkay;

  IF bytes > 0D THEN
    IF GSeek(context.file, bytes, OffsetCurrent) = -1D THEN
      iffp := BadIFF;
    ELSE
      INC(context.position, bytes);
    END;
  END;

  RETURN (iffp);
END SkipFwd;


(* ======== PUBLIC ======== *)

PROCEDURE OpenRIFF(File: FileHandle; VAR new: GroupContext;
                   clientframe: ADDRESS): IFFP;
VAR
  iffp : IFFP;
  r    : LONGINT;
BEGIN
  iffp := IFFOkay;

  WITH new DO
    parent       := NIL;	(* "whole file" has no parent. *)
    clientFrame  := clientframe;
    file         := File;
    position     := 0D;
    subtype      := NullChunk;
    bytesSoFar   := 0D;
    ckHdr.ckID   := NullChunk;
    ckHdr.ckSize := 0D;
  END;

  (* Set new.bound and go to the file's beginning. *)
  new.bound := FileLength(File);
  IF new.bound < 0D THEN
    iffp := new.bound;			(* File system error! *)
  ELSIF new.bound < LONGINT(TSIZE(ChunkHeader)) THEN
    iffp := NotIFF;			(* Too small for an IFF file. *)
  ELSE
    r := GSeek(File, 0D, OffsetBeginning); (* Go to file start. *)
  END;

  RETURN (iffp);
END OpenRIFF;


PROCEDURE OpenRGroup(VAR Parent, new: GroupContext): IFFP;
VAR
  iffp : IFFP;
BEGIN
  iffp := IFFOkay;

  WITH new DO
    parent       := ADR(Parent);
    clientFrame  := Parent.clientFrame;
    file         := Parent.file;
    position     := Parent.position;
    bound        := Parent.position + ChunkMoreBytes(Parent);
    subtype      := NullChunk;
    bytesSoFar   := 0D;
    ckHdr.ckID   := NullChunk;
    ckHdr.ckSize := 0D;
  END;

  IF (new.bound > Parent.bound) OR (ODD(new.bound)) THEN
    iffp := BadIFF;
  END;
  RETURN (iffp);
END OpenRGroup;

PROCEDURE CloseRGroup(VAR old: GroupContext): IFFP;
VAR
  Position : LONGINT;
BEGIN
  WITH old DO
    IF parent # NIL THEN (* Not context for whole file *)
      Position := position;
      WITH parent^ DO
        INC(bytesSoFar, Position - position);
        position := Position;
      END;
    END;
  END;
  RETURN (IFFOkay);
END CloseRGroup;

PROCEDURE GetChunkHdr(VAR context: GroupContext): ID;
VAR
  iffp : IFFP;
  remaining : LONGINT;
  r : LONGINT;
BEGIN
  (* Skip remainder of previous chunk & padding. *)
  iffp := SkipFwd(context, ChunkMoreBytes(context) + LONGINT(ODD(context.ckHdr.ckSize)) );

  IF iffp # IFFOkay THEN (* CheckIFFP *)
    RETURN (iffp);
  END;

  (* Set up to read the new header. *)
  WITH context DO
    ckHdr.ckID := BadIFF; (* Until we know it's okay, mark it BAD *)
    subtype    := NullChunk;
    bytesSoFar := 0D;

    (* Generate a psuedo-chunk if at end-of-context. *)
    remaining := bound - position;
    IF remaining = 0D THEN
      ckHdr.ckSize := 0D;
      ckHdr.ckID   := EndMark;

    (* BAD_IFF if not enough bytes in the context for a ChunkHeader.*)
    ELSIF LONGINT(TSIZE(ChunkHeader)) > remaining THEN
      ckHdr.ckSize := remaining;

    ELSE (* Read the chunk header (finally). *)
      r := GRead(file, ADR(ckHdr), LONGINT(TSIZE(ChunkHeader)));
      IF r = -1D THEN
        ckHdr.ckID := DOSError;
        RETURN (DOSError);
      ELSIF r = 0D THEN
        ckHdr.ckID := BadIFF;
        RETURN (BadIFF);
      END;

      (* Check: Top level chunk must be LIST or FORM or CAT. *)
      IF parent = NIL THEN
        IF NOT ((ckHdr.ckID = FORM) OR (ckHdr.ckID = LIST) OR
                (ckHdr.ckID = CAT)) THEN
          ckHdr.ckID := NotIFF;
          RETURN (NotIFF);
        END;
      END;

      (* Update the context. *)
      INC(position, TSIZE(ChunkHeader));
      DEC(remaining, TSIZE(ChunkHeader));


      (* Non-positive ID values are illegal and used for error codes.*)
      (* We could check for other illegal IDs...*)
      IF ckHdr.ckID <= 0D THEN
        ckHdr.ckID := BadIFF;
      ELSIF (ckHdr.ckSize < 0D) OR (ckHdr.ckSize > remaining) THEN
        ckHdr.ckSize := remaining;
        ckHdr.ckID   := BadIFF;
      ELSE
        IF (ckHdr.ckID = LIST) OR (ckHdr.ckID = FORM) OR
           (ckHdr.ckID = PROP) OR (ckHdr.ckID = CAT ) THEN
          iffp := IFFReadBytes(context, ADR(subtype), LONGINT(TSIZE(ID)));
          IF iffp # IFFOkay THEN
            ckHdr.ckID := iffp;
          END;
        END;
      END;
    END;
    RETURN (ckHdr.ckID);
  END; (* WITH *)
END GetChunkHdr;

PROCEDURE IFFReadBytes(VAR context: GroupContext; buffer: ADDRESS; 
                       nBytes: LONGINT): IFFP;
VAR
  iffp : IFFP;
  r : LONGINT;
BEGIN
  iffp := IFFOkay;

  IF nBytes < 0D THEN
    iffp := ClientError;
  ELSIF nBytes > ChunkMoreBytes(context) THEN
    iffp := ShortChunk;
  ELSIF nBytes > 0D THEN
    r := GRead(context.file, buffer, nBytes);
    IF r = -1D THEN
      iffp := DOSError;
    ELSIF r = 0D THEN
      iffp := BadIFF;
    ELSE
      INC(context.position, nBytes);
      INC(context.bytesSoFar, nBytes);
    END;
  END;
  RETURN (iffp);
END IFFReadBytes;

PROCEDURE SkipGroup(VAR context: GroupContext): IFFP;
  (* Nothing to do, thanks to GetChunkHdr *)
BEGIN
  RETURN (IFFOkay);
END SkipGroup;

PROCEDURE ReadIFF(file: FileHandle; clientframe: ADDRESS): IFFP;
VAR
  iffp : IFFP;
  context : GroupContext;
  clientF : ClientFramePtr;
  r : LONGINT;
BEGIN
  clientF := clientframe;
  iffp := OpenRIFF(file, context, clientframe);
(*  context.clientFrame := clientFrame; (done in OpenRIFF) *)

  IF iffp = IFFOkay THEN
    iffp := GetChunkHdr(context);
    IF iffp = FORM THEN
      iffp := clientF^.getForm(context);
    ELSIF iffp = LIST THEN
      iffp := clientF^.getList(context);
    ELSIF iffp = CAT THEN
      iffp := clientF^.getCat(context);
    END;
  END;

  r := CloseRGroup(context);
  IF iffp > 0D THEN		(* Make sure we don't return an ID.*)
    iffp := NotIFF;             (* GetChunkHdr should've caught this. *)
  END;
  RETURN (iffp);
END ReadIFF;

PROCEDURE ReadIList(VAR parent: GroupContext; clientframe: ADDRESS): IFFP;
VAR
  listContext: GroupContext;
  clientF : ClientFramePtr;
  iffp : IFFP;
  propOk : BOOLEAN;
  r : LONGINT;
BEGIN
  clientF := clientframe;

  propOk := TRUE;

  iffp := OpenRGroup(parent, listContext);
  IF iffp # IFFOkay THEN (* CheckIFFP *)
    RETURN (iffp);
  END;

  (* One special case test lets us handle CATs as well as LISTs. *)
  IF parent.ckHdr.ckID = CAT THEN
    propOk := FALSE;
  ELSE
    listContext.clientFrame := clientframe;
  END;

  REPEAT
    iffp := GetChunkHdr(listContext);
    IF iffp = PROP THEN
      IF propOk THEN
        iffp := clientF^.getProp(listContext);
      ELSE
        iffp := BadIFF;
      END;
    ELSIF iffp = FORM THEN
      iffp := clientF^.getForm(listContext);
    ELSIF iffp = LIST THEN
      iffp := clientF^.getList(listContext);
    ELSIF iffp = CAT THEN
      iffp := clientF^.getCat(listContext);
    END;
    
    IF listContext.ckHdr.ckID # PROP THEN
      propOk := FALSE; (* No PROPs allowed after this point. *)
    END;
  UNTIL NOT (iffp = IFFOkay);
  r := CloseRGroup(listContext);

  IF iffp > 0D THEN	(* Only chunk types above are allowed in a LIST/CAT.*)
    iffp := BadIFF;
  END;
  IF iffp = EndMark THEN
    iffp := IFFOkay;
  END;
  RETURN (iffp);
END ReadIList;

PROCEDURE ReadICat(VAR parent: GroupContext): IFFP;
BEGIN
  (* By special arrangement with the ReadIList implement'n, this is trivial.*)
  RETURN ( ReadIList(parent, NIL) );
END ReadICat;

PROCEDURE GetFChunkHdr(VAR context: GroupContext): ID;
VAR
  id : ID;
BEGIN
  id := GetChunkHdr(context);
  IF id = PROP THEN
    id := BadIFF;
    context.ckHdr.ckID := id;
  END;
  RETURN (id);
END GetFChunkHdr;

PROCEDURE GetF1ChunkHdr(VAR context: GroupContext): ID;
VAR
  id : ID;
  clientframe : ClientFramePtr;
BEGIN
  clientframe := context.clientFrame;

  id := GetChunkHdr(context);
  IF id = PROP THEN
    id := BadIFF;
  ELSIF id = FORM THEN
    id := clientframe^.getForm(context);
  ELSIF id = LIST THEN
    id := clientframe^.getList(context);
  ELSIF id = CAT THEN
    id := clientframe^.getCat(context);
  END;

  context.ckHdr.ckID := id;
  RETURN (id);
END GetF1ChunkHdr;

PROCEDURE GetPChunkHdr(VAR context: GroupContext): ID;
VAR
  id : ID;
BEGIN
  id := GetChunkHdr(context);

  IF (id = LIST) OR (id = FORM) OR (id = PROP) OR (id = CAT) THEN
    id := BadIFF;
    context.ckHdr.ckID := id;
  END;

  RETURN (id);
END GetPChunkHdr;

END IFFR.
