(****************************************************************************
 *                    MODULA-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: PortsUtil.MOD                     Version: Amiga.00.00             *
 * Created: 01/04/87   Updated: 01/16/87   Author: Leon Frenkel             *
 * Description: Exec Support Procedures for Ports.                          *
 ****************************************************************************)

IMPLEMENTATION MODULE PortsUtil;

FROM SYSTEM IMPORT ADR, ADDRESS, BYTE, TSIZE;
FROM Lists  IMPORT NewList;
FROM Memory IMPORT AllocMem, FreeMem, MemReqSet, MemPublic, MemClear;
FROM Nodes  IMPORT NTMsgPort, NodePtr;
FROM Ports  IMPORT AddPort, RemPort, MsgPort, MsgPortPtr, PASignal;
FROM Tasks  IMPORT AllocSignal, FreeSignal, SignalRange, AnySignal, NoSignals,
                   CurrentTask, FindTask;

(*$L+*)

PROCEDURE CreatePort(name: ADDRESS; pri: INTEGER): MsgPortPtr;
VAR sigBit : SignalRange;
    port   : MsgPortPtr;
BEGIN
  sigBit := AllocSignal(AnySignal);
  IF sigBit = NoSignals THEN RETURN (NIL) END;

  port := AllocMem(TSIZE(MsgPort), MemReqSet{MemClear,MemPublic});
  IF port = NIL THEN
    FreeSignal(sigBit);
    RETURN (NIL);
  END;

  WITH port^ DO
    mpNode.lnName := name;
    mpNode.lnPri  := BYTE(pri);
    mpNode.lnType := NTMsgPort;

    mpFlags       := PASignal;
    mpSigBit      := BYTE(sigBit);
    mpSigTask     := FindTask(CurrentTask);
  END;

  IF name # NIL THEN
    AddPort(port^);
  ELSE
    NewList(port^.mpMsgList);
  END;
  RETURN (port);
END CreatePort;


PROCEDURE DeletePort(VAR port: MsgPort);
BEGIN
  WITH port DO
    IF mpNode.lnName # NIL THEN RemPort(port); END;
    mpNode.lnType    := BYTE(0FFH);
    mpMsgList.lhHead := NodePtr(0FFFFFFFFH);
    FreeSignal(SignalRange(mpSigBit));
    FreeMem(ADR(port), TSIZE(MsgPort));
  END;
END DeletePort;

END PortsUtil.
