(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CScanStdIn.DEF                    Version: Amiga.00.00             *
 * Created: 02/24/87   Updated: 02/24/87   Author: Leon Frenkel             *
 * Description: From the "C" language standard library, functions for       *
 *  doing formated input from the "stdin" stream, through module CStdIO.    *
 ****************************************************************************)

IMPLEMENTATION MODULE CScanStdIn;

FROM SYSTEM IMPORT ADDRESS;
FROM CStdIO IMPORT stdin, getc, ungetc;
FROM ScanString IMPORT Scan, ScanArg;

(*$L+,D-*)

CONST
  EOF = -1;

VAR
  last: INTEGER;

PROCEDURE GetCh(flag: BOOLEAN): INTEGER;
BEGIN
  IF NOT flag THEN (* get new char *)
    last := getc(stdin);
  ELSE (* put back char *)
    IF last # EOF THEN (* can't put back EOF *)
      last := ungetc(CHR(last),stdin);
    END;
  END;
  RETURN (last);
END GetCh;

PROCEDURE scanf(formatStr: ADDRESS; VAR args: ARRAY OF ScanArg): INTEGER;
BEGIN
  last := 0;
  RETURN Scan(formatStr, args, GetCh);
END scanf;

END CScanStdIn.
