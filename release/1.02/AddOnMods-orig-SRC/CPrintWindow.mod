(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CPrintWindow.MOD                  Version: Amiga.00.00             *
 * Created: 04/06/87   Updated: 05/21/87   Author: Leon Frenkel             *
 * Description: From the "C" language standard library, functions for       *
 *  doing formated output to a window using the module SimpleConsole.       *
 ****************************************************************************)

IMPLEMENTATION MODULE CPrintWindow;

FROM SYSTEM        IMPORT ADDRESS, BYTE, TSIZE, ADR;
FROM FormatString  IMPORT FormatArg, Format;
FROM Intuition     IMPORT Window, WindowPtr;
FROM SimpleConsole IMPORT PutBuf;

(*$L+*)

TYPE 
  (* Output buffer type *)
  Buffer = ARRAY [0..127] OF CHAR;

VAR
  bufPtr : POINTER TO Buffer;
  bufIdx : INTEGER;
  wPtr   : WindowPtr;

PROCEDURE WriteBuf;
BEGIN
  PutBuf(wPtr^, bufPtr, bufIdx);
END WriteBuf;

PROCEDURE StoreCh(c: CHAR);
BEGIN
  bufPtr^[bufIdx] := c;
  INC(bufIdx);
  IF bufIdx = TSIZE(Buffer) THEN
    WriteBuf;
    bufIdx := 0;
  END;
END StoreCh;

PROCEDURE wprintf(VAR window: ARRAY OF BYTE; (* Window *)
                  formatStr: ADDRESS;
                  VAR args: ARRAY OF FormatArg);
VAR
  l      : INTEGER;
  OutBuf : Buffer;
BEGIN
  wPtr := ADR(window);
  bufPtr := ADR(OutBuf);
  bufIdx := 0;
  l := Format(formatStr, args, StoreCh);
  WriteBuf;
END wprintf;


END CPrintWindow.
