(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: Storage.MOD                       Version: Amiga.00.00             *
 * Created: 12/03/86   Updated: 01/06/88   Author: Leon Frenkel             *
 * Description: Memory management module as defined in appendix 2 of        *
 *  "Programming in Modula-2" by N. Wirth. This module is included to       *
 *  allow programs written on other systems to be ported and vice versa,    *
 *  to access the full memory management capabilities of the Amiga use the  *
 *  module "Memory".                                                        *
 ****************************************************************************)

IMPLEMENTATION MODULE Storage;
FROM SYSTEM IMPORT ADDRESS;
FROM Memory IMPORT AllocMem, AvailMem, FreeMem, MemReqSet;

(*$L+*)

PROCEDURE ALLOCATE(VAR a: ADDRESS; size: LONGCARD);
BEGIN
  a := AllocMem(size, MemReqSet{});
END ALLOCATE;

PROCEDURE DEALLOCATE(VAR a: ADDRESS; size: LONGCARD);
BEGIN
  FreeMem(a, size);
END DEALLOCATE;

PROCEDURE Available(size: LONGCARD): BOOLEAN;
VAR
  p : ADDRESS;
BEGIN
  p := AllocMem(size, MemReqSet{});
  IF (p # NIL) THEN
    FreeMem(p, size);
  END;
  RETURN(p # NIL);
END Available;

END Storage.
