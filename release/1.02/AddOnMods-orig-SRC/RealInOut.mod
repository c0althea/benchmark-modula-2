(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: RealInOut.MOD                     Version: Amiga.00.00             *
 * Created: 01/14/87   Updated: 07/24/87   Author: Leon Frenkel             *
 * Description: Standard RealInOut module as described in appendix 2 of     *
 *  Programming in Modula-2 by N. Wirth.                                    *
 ****************************************************************************)

IMPLEMENTATION MODULE RealInOut;

FROM InOut IMPORT ReadString, WriteString;
FROM RealConversions IMPORT RealToStringFormat, ConvStringToReal, 
                            ConvRealToString;

(*$L+*)

PROCEDURE ReadReal(VAR x: REAL);
VAR s: ARRAY [0..79] OF CHAR;
BEGIN
  ReadString(s);
  x := ConvStringToReal(s);
END ReadReal;


PROCEDURE WriteReal(x: REAL; n: CARDINAL);
VAR
  s1 : ARRAY [0..79] OF CHAR;
  s2 : ARRAY [0..79] OF CHAR;
  i  : INTEGER;
BEGIN
  ConvRealToString(x, s2, 7, ShortestForm);
  i := 0;
  WHILE (n > 0) AND (s2[i] # 0C) DO
    INC(i);
    DEC(n);
  END;
  i := 0;
  WHILE (n > 0) DO
    s1[i] := " ";
    INC(i);
    DEC(n);
  END;
  s1[i] := 0C;
  WriteString(s1); (* output pading blanks *)
  WriteString(s2); (* output digits *)
END WriteReal;

BEGIN
  Done := TRUE;
END RealInOut.
