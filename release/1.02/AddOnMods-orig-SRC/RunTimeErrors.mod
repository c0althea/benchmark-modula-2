(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: RunTimeErrors.MOD                 Version: Amiga.00.00             *
 * Created: 02/26/87   Updated: 02/26/87   Author: Leon Frenkel             *
 * Description: This module is usefull for debuging purposes. Whenever a    *
 *  run-time error occures a window will be displayed showing the context   *
 *  of the error and giving the user the option of continuing,aborting, etc.*
 ****************************************************************************)

IMPLEMENTATION MODULE RunTimeErrors;

FROM SYSTEM IMPORT ADR, ADDRESS, BYTE, SHIFT, INLINE, SETREG, REG;
FROM System IMPORT ExecBase, DOSBase;

(*$L+,D-*)

(* Module: AmigaDOS *)
CONST
  ModeNewFile = 1006D;

(* Module: Nodes *)
TYPE
  NodePtr = POINTER TO Node;
  Node = RECORD
           lnSucc : NodePtr;  (* ptr to next node in the list *)
           lnPred : NodePtr;  (* ptr to previous node in the list *)
           lnType : BYTE;     (* defines the type of the node *)
           lnPri  : BYTE;     (* specifies the priority of the node *)
           lnName : ADDRESS;  (* points the name of the node or NIL *)
         END;

(* Module: Lists *)
  ListPtr = POINTER TO List;
  List = RECORD
           lhHead     : NodePtr; (* points to the first node in the list *)
           lhTail     : NodePtr; (* is always NIL *)
           lhTailPred : NodePtr; (* points to the last node in the list *)
           lhType     : BYTE;    (* defines the type of nodes within the list *)
           lpad       : BYTE;    (* structure alignment byte *)
         END;

(* Module: Tasks *)
CONST
  CurrentTask = NIL; (* Passed to FindTask or RemTask to indicate current task*)

  AnySignal = -1; (* Passed to AllocSignal to return any signal *)
  NoSignals = -1; (* Returned by AllocSignal to indicate no signals *)
  MaxSignal = 31; (* Highest signal number *)
  AnyTrap   = -1; (* Passed to AllocTrap to return any trap *)
  NoTraps   = -1; (* Returned by AllocTrap to indicate no traps *)
  MaxTrap   = 15; (* Highest trap number *)

TYPE
  SignalRange = [AnySignal..MaxSignal];
  SignalSet   = SET OF [0..MaxSignal];

  TrapRange   = [AnyTrap..MaxTrap];
  TrapSet     = SET OF [0..MaxTrap];
  
  TaskFlags = (TBProcTime, TB1, TB2, TB3, TBStackChk, TBExcept, TBSwitch,
               TBLaunch);
  TaskFlagsSet = SET OF TaskFlags;

  TaskState = (TSInvalid, TSAdded, TSRun, TSReady, TSWait, TSExcept, TSRemoved);
  TaskStateSet = SET OF TaskState;

  TaskPtr = POINTER TO Task;
  Task = RECORD
           tcNode       : Node;
           tcFlags      : TaskFlagsSet;
           tcState      : TaskStateSet;
           tcIDNestCnt  : BYTE;      (* intr disabled nesting *)
           tcTDNestCnt  : BYTE;      (* task disabled nesting *)
           tcSigAlloc   : SignalSet; (* signals allocated *)
           tcSigWait    : SignalSet; (* signals we are waiting for *)
           tcSigRecvd   : SignalSet; (* signals we have received *)
           tcSigExcept  : SignalSet; (* signals we take excepts for *)
           tcTrapAlloc  : TrapSet;   (* traps allocated *)
           tcTrapAble   : TrapSet;   (* traps enabled *)
           tcExceptData : ADDRESS;   (* points to except data *)
           tcExceptCode : PROC;      (* points to except code *)
           tcTrapData   : ADDRESS;   (* points to trap code *)
           tcTrapCode   : PROC;      (* points to trap data *)
           tcSPReg      : ADDRESS;   (* stack pointer *)
           tcSPLower    : ADDRESS;   (* stack lower bound *)
           tcSPUpper    : ADDRESS;   (* stack upper bound + 2 *)
           tcSwitch     : PROC;      (* task losing CPU *)
           tcLaunch     : PROC;      (* task getting CPU *)
           tcMemEntry   : List;      (* allocated memory *)
           tcUserData   : ADDRESS;   (* per task data *)
         END;



CONST
  D0 = 0; D1 = 1; D2 = 2; D3 = 3; D4 = 4; D5 = 5; D6 = 6; D7 = 7;
  A0 = 8; A1 = 9; A2 =10; A3 =11; A4 =12; A5 =13; A6 =14; A7 =15;
  RTS  = 4E75H;
  RTE  = 4E73H;
  UNLK = 4E5DH;
  MOVE_L_Inc_A7_TO_D0 = 0201FH;
  MOVE_L_D0_TO_Dec_A7 = 02F00H;
  ADDQ_L_4_TO_A7 = 588FH;
  JMP_A0 = 04ED0H;
  MOVE_SR_TO_D0 = 040C0H;

  LF = 012C;


VAR
  Installed      : BOOLEAN; (* FALSE = not installed, TRUE = installed *)
  Old_Trap_Valid : BOOLEAN; (* FALSE = TrapCode is 0,TRUE = TrapCode not 0 *)
  Old_TrapCode   : PROC;    (* old tc_TrapCode *) 
  Old_TrapData   : ADDRESS; (* old tc_TrapData *)
  MyTask         : TaskPtr;
  Digits         : POINTER TO ARRAY [0..15] OF CHAR;
  StringPtr      : ADDRESS;
  SRReg          : BITSET;
  SPReg          : POINTER TO CARDINAL;
  PCReg          : LONGCARD;
  High           : CARDINAL;
  Low            : CARDINAL;
  regs           : ARRAY [D0..A7] OF LONGCARD;
  window         : ADDRESS;
  trapNum        : CARDINAL;
  tempReg        : LONGCARD;
  ch             : CHAR;

(*************************** Utilitity Functions *****************************)

(* Disable - disable interrupt processing *)
PROCEDURE Disable;
CONST
  LVODisable = 0FF88H; 
BEGIN
  SETREG(A6, ExecBase);
  INLINE(4EAEH, LVODisable); (* JSR LVODisable(A6) *)
END Disable;

(* Enable - enable interrupt processing *)
PROCEDURE Enable;
CONST
  LVOEnable = 0FF82H;
BEGIN
  SETREG(A6, ExecBase);
  INLINE(4EAEH, LVOEnable); (* JSR LVOEnable(A6) *)
END Enable;

(* UserState - switch to user state from super state *)
PROCEDURE UserState(sysStack: ADDRESS);
CONST
  LVOUserState = 0FF64H;
BEGIN
  SETREG(A6, ExecBase);
  SETREG(D0, sysStack);
  INLINE(4EAEH, LVOUserState); (* JSR LVOUserState(A6) *)
END UserState;

(* FindTask - return ptr to task structure *)
PROCEDURE FindTask(name: ADDRESS): TaskPtr;
CONST
  LVOFindTask = 0FEDAH;
BEGIN
  SETREG(A6, ExecBase);
  SETREG(A1, name);
  INLINE(4EAEH, LVOFindTask); (* JSR LVOFindTask(A6) *)
  (* result in D0 *)
END FindTask;

PROCEDURE Open(name: ADDRESS; mode: LONGINT): ADDRESS;
CONST
  LVOOpen = 0FFE2H;
BEGIN
  SETREG(A6, DOSBase);
  SETREG(D1, name);
  SETREG(D2, mode);
  INLINE(4EAEH, LVOOpen); (* JSR LVOOpen(A6) *)
  (* result in D0 *)
END Open;

PROCEDURE Close(handle: ADDRESS);
CONST
  LVOClose = 0FFDCH;
BEGIN
  SETREG(A6, DOSBase);
  SETREG(D1, handle);
  INLINE(4EAEH, LVOClose); (* JSR LVOClose(A6) *)
END Close;

PROCEDURE DOSWrite(file: ADDRESS; buffer: ADDRESS; length: LONGINT): LONGINT;
CONST
  LVOWrite = 0FFD0H;
BEGIN
  SETREG(A6, DOSBase);
  SETREG(D1, file);
  SETREG(D2, buffer);
  SETREG(D3, length);
  INLINE(4EAEH, LVOWrite); (* JSR LVOWrite(A6) *)
END DOSWrite;

PROCEDURE DOSRead(file: ADDRESS; buffer: ADDRESS; length: LONGINT): LONGINT;
CONST
  LVORead = 0FFD6H;
BEGIN
  SETREG(A6, DOSBase);
  SETREG(D1, file);
  SETREG(D2, buffer);
  SETREG(D3, length);
  INLINE(4EAEH, LVORead); (* JSR LVORead(A6) *)
END DOSRead;

PROCEDURE ConvToHex(val: LONGCARD; width: CARDINAL);
VAR
  shift : INTEGER;
  dig   : CARDINAL;
BEGIN
  shift := (width - 1) * 4;
  REPEAT
    dig := CARDINAL(SHIFT(val, -shift)) MOD 16;
    StringPtr^ := Digits^[dig];
    INC(StringPtr);
    DEC(shift, 4);
  UNTIL (shift < 0);
END ConvToHex;


PROCEDURE FormatReg(reg: CARDINAL);
VAR
  rch : CHAR;
  rnum: CARDINAL;
BEGIN 
  IF reg <= D7 THEN
    rch := "D";
    rnum := reg;
  ELSE
    rch := "A";
    rnum := reg - A0;
  END;

  StringPtr^ := rch;
  INC(StringPtr);
  StringPtr^ := CHR(CARDINAL("0") + rnum);
  INC(StringPtr);
  StringPtr^ := "=";
  INC(StringPtr);
  ConvToHex(regs[reg],8);
END FormatReg;

PROCEDURE FormatSRBit(sym: CHAR; bit: CARDINAL);
BEGIN
  StringPtr^ := sym; INC(StringPtr);
  StringPtr^ := "="; INC(StringPtr);
  ConvToHex(LONGCARD(bit IN SRReg),1);
  StringPtr^ := " "; INC(StringPtr);
END FormatSRBit;

PROCEDURE WriteString(s: ARRAY OF CHAR);
VAR result : LONGINT;
    length : INTEGER;
BEGIN
  length := 0;
  WHILE s[length] # 0C DO
    INC(length);
  END;
  result := DOSWrite(window, ADR(s), LONGINT(length));
END WriteString;

PROCEDURE Read(VAR ch: CHAR);
VAR
  result : LONGINT;
BEGIN
  result := DOSRead(window, ADR(ch), 1D);
END Read;

(*****************************************************************************)

PROCEDURE DisplayError;
VAR r    : CARDINAL;
    tstr : POINTER TO ARRAY [0..255] OF CHAR;
    buf  : ARRAY [0..299] OF CHAR;
BEGIN
  window := Open(ADR("RAW:0/0/432/70/M2SCS: ErrorProcessor (c) 1987 by Leon Frenkel"), ModeNewFile);

  CASE trapNum OF
  |  2 		:  tstr := ADR("bus error");
  |  3 		:  tstr := ADR("address error");
  |  4 		:  tstr := ADR("illegal instruction");
  |  5 		:  tstr := ADR("zero divide");
  |  6 		:  tstr := ADR("CHK instruction");
  |  7 		:  tstr := ADR("TRAPV instruction");
  |  8 		:  tstr := ADR("privilege violation");
  |  9 		:  tstr := ADR("trace");
  | 10 		:  tstr := ADR("line 1010 emulator");
  | 11 		:  tstr := ADR("line 1111 emulator");
  | 32..47 	:  tstr := ADR("TRAP instruction");
  ELSE
    tstr := ADR("invalid trap number(?)");
  END;

  StringPtr := ADR(buf);

  FOR r := D0 TO D3 DO
    FormatReg(r);
    StringPtr^ := " ";
    INC(StringPtr);
  END;
  DEC(StringPtr);
  StringPtr^ := LF;
  INC(StringPtr);

  FOR r := D4 TO D7 DO
    FormatReg(r);
    StringPtr^ := " ";
    INC(StringPtr);
  END;
  DEC(StringPtr);
  StringPtr^ := LF;
  INC(StringPtr);

  FOR r := A0 TO A3 DO
    FormatReg(r);
    StringPtr^ := " ";
    INC(StringPtr);
  END;
  DEC(StringPtr);
  StringPtr^ := LF;
  INC(StringPtr);

  FOR r := A4 TO A7 DO
    FormatReg(r);
    StringPtr^ := " ";
    INC(StringPtr);
  END;
  DEC(StringPtr);
  StringPtr^ := LF;
  INC(StringPtr);

  StringPtr^ := "P"; INC(StringPtr);
  StringPtr^ := "C"; INC(StringPtr);
  StringPtr^ := "="; INC(StringPtr);
  ConvToHex(PCReg,8);
  StringPtr^ := " "; INC(StringPtr);

  StringPtr^ := "S"; INC(StringPtr);
  StringPtr^ := "R"; INC(StringPtr);
  StringPtr^ := "="; INC(StringPtr);
  ConvToHex(LONGCARD(SRReg),4);
  StringPtr^ := " "; INC(StringPtr);

  FormatSRBit("T",15);
  FormatSRBit("S",13);

  StringPtr^ := "I"; INC(StringPtr);
  StringPtr^ := "="; INC(StringPtr);
  ConvToHex(LONGCARD(CARDINAL(SRReg * {10,9,8}) DIV 256),1);
  StringPtr^ := " "; INC(StringPtr);

  FormatSRBit("X",4);
  FormatSRBit("N",3);
  FormatSRBit("Z",2);
  FormatSRBit("V",1);
  FormatSRBit("C",0);

  DEC(StringPtr);
  StringPtr^ := LF;
  INC(StringPtr);

  (* null-term string *)
  StringPtr^ := 0C; 
  WriteString(buf);

  WriteString("Exception: Vector=");
  StringPtr := ADR(buf);
  ConvToHex(LONGCARD(trapNum),2);
  StringPtr^ := 0C;
  WriteString(buf);
  WriteString("   Name=");
  WriteString(tstr^);
  WriteString("\nSelect: C = Continue  E = Exit  P = Propagate");
  LOOP
    Read(ch);
    ch := CAP(ch);
    IF (ch = "C") OR (ch = "E") OR (ch = "P") THEN EXIT; END;
  END;
  Close(window);

END DisplayError;



(* ErrorProcessor - called by ROM Kernel Exec at Supervisory Level *)
(*  all registers are preserved! *)
PROCEDURE ErrorProcessor;
BEGIN
  INLINE(UNLK); (* UNLINK A5 *)
  SPReg    := ADDRESS(REG(A7));
  INLINE(48E7H,0FFFEH); (* MOVEM.L D0-D7/A0-A6,-(SP) *)

  regs[D0] := REG(D0);
  regs[D1] := REG(D1);
  regs[D2] := REG(D2);
  regs[D3] := REG(D3);
  regs[D4] := REG(D4);
  regs[D5] := REG(D5);
  regs[D6] := REG(D6);
  regs[D7] := REG(D7);
  regs[A0] := REG(A0);
  regs[A1] := REG(A1);
  regs[A2] := REG(A2);
  regs[A3] := REG(A3);
  regs[A4] := REG(A4);
  regs[A5] := REG(A5);
  regs[A6] := REG(A6);
  INLINE(4E68H);       (* MOVE.L USP,A0  *)
  regs[A7] := REG(A0); (* User Stack Ptr *)

  INC(ADDRESS(SPReg), 2);
  trapNum := SPReg^;
  INC(ADDRESS(SPReg), 2);
  SRReg := BITSET(SPReg^);
  INC(ADDRESS(SPReg), 2);
  High := SPReg^;
  INC(ADDRESS(SPReg), 2);
  Low  := SPReg^;
  PCReg := (LONGCARD(High) * 65536D) + LONGCARD(Low);

  DisplayError;

  IF (ch = "P") AND Old_Trap_Valid THEN (* Propagete Error *)
    INLINE(4CDFH,07FFFH);   (* MOVEM.L (SP)+,D0-D7/A0-D6 *)
    tempReg := REG(D0);  (* preserve D0 *)
    SETREG(D0, Old_TrapCode);
    INLINE(MOVE_L_D0_TO_Dec_A7); (* MOVE.L D0,-(SP) *)
    SETREG(D0, tempReg); (* restore D0 *)
    INLINE(RTS); (* RTS (jumps to next trap handler) *)
  END;

  IF (ch = "E") THEN (* EXIT To DOS *)
    INLINE(4CDFH,07FFFH);   (* MOVEM.L (SP)+,D0-D7/A0-D6 *)
    INLINE(ADDQ_L_4_TO_A7); (* ADDQ.L #4,A7 (pop exception # from stack) *)
    RemoveErrorHandler;     (* disconnect error processor *)
    SPReg := ADDRESS(REG(A7));
    UserState(SPReg);
    HALT;
  END;

  (* "C" = continue execution *)
  INLINE(4CDFH,07FFFH);   (* MOVEM.L (SP)+,D0-D7/A0-D6 *)
  INLINE(ADDQ_L_4_TO_A7); (* ADDQ.L #4,A7 (pop exception # from stack) *)
  INLINE(RTE);
END ErrorProcessor;


PROCEDURE InstallErrorHandler;
BEGIN
  IF NOT Installed THEN 
  Disable;
    MyTask := FindTask(CurrentTask);
    WITH MyTask^ DO
      Old_TrapCode := tcTrapCode;
      Old_TrapData := tcTrapData;
      Old_Trap_Valid := ADDRESS(Old_TrapCode) # NIL;
      tcTrapCode := ErrorProcessor;
      tcTrapData := NIL;
    END;
    Installed := TRUE;
  Enable;
  END;
END InstallErrorHandler;

PROCEDURE RemoveErrorHandler;
BEGIN
  IF Installed THEN
  Disable;
    WITH MyTask^ DO 
      tcTrapCode := Old_TrapCode;
      tcTrapData := Old_TrapData;
    END;
    Installed := FALSE;
  Enable;
  END;
END RemoveErrorHandler;

BEGIN
  Digits := ADR("0123456789ABCDEF");
END RunTimeErrors.
