(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CFileIOUtil.MOD                   Version: Amiga.00.00             *
 * Created: 02/23/87   Updated: 02/23/87   Author: Leon Frenkel             *
 * Description: From the "C" language standard library, functions for       *
 *  lo-level file io.                                                       *
 ****************************************************************************)

IMPLEMENTATION MODULE CFileIOUtil;

FROM SYSTEM IMPORT ADDRESS;
FROM CFileIO IMPORT FileDesc, errno, EEXIST;
FROM AmigaDOS IMPORT IsInteractive, Lock, UnLock, Rename, IoErr, DeleteFile, 
                     AccessRead, FileLock;

(*$L+*)

PROCEDURE isatty(file: FileDesc): BOOLEAN;
BEGIN
  RETURN (IsInteractive(file^.handle));
END isatty;

PROCEDURE rename(old, new: ADDRESS): BOOLEAN;
VAR 
  lk : FileLock;
BEGIN
  lk := Lock(new, AccessRead);
  IF lk # NIL THEN
    UnLock(lk);
    errno := EEXIST;
    RETURN(FALSE); (* error *)
  END;
  IF NOT Rename(old, new) THEN
    errno := IoErr();
    RETURN(FALSE); (* error *)
  END;
  RETURN(TRUE); (* no error *)
END rename;

PROCEDURE unlink(name: ADDRESS): BOOLEAN;
BEGIN
  IF NOT DeleteFile(name) THEN
    errno := IoErr();
    RETURN (FALSE); (* error *)
  END;
  RETURN (TRUE); (* no error *)
END unlink;

END CFileIOUtil.
