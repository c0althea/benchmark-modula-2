(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: DMAHardware.MOD                   Version: Amiga.00.00             *
 * Created: 11/29/86   Updated: 11/29/86   Author: Leon Frenkel             *
 * Description: Amiga DMA control definitions.                              *
 ****************************************************************************)

IMPLEMENTATION MODULE DMAHardware;

FROM CustomHardware IMPORT custom, Custom;

(*$L+*)

PROCEDURE OnDisplay;
BEGIN
  custom^.dmacon := {DMASetClr, DMARaster};
END OnDisplay;

PROCEDURE OffDisplay;
BEGIN
  custom^.dmacon := {DMARaster};
END OffDisplay;

PROCEDURE OnSprite;
BEGIN
  custom^.dmacon := {DMASetClr, DMASprite};
END OnSprite;

PROCEDURE OffSprite;
BEGIN
  custom^.dmacon := {DMASprite};
END OffSprite;


END DMAHardware.
