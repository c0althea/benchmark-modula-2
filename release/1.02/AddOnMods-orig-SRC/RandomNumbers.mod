(****************************************************************************
 *                    MODULA-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: RandomNumbers.MOD                 Version: Amiga.00.00             *
 * Created: 01/13/87   Updated: 06/06/87   Author: Leon Frenkel             *
 * Description: Pseudo-Random Number Generator.                             *
 ****************************************************************************)

IMPLEMENTATION MODULE RandomNumbers;

(*$L+*)

CONST
  m  = 100000000D;
  m1 = 10000D;
  b  = 31415821D;

PROCEDURE Random(max: LONGCARD): LONGCARD;
BEGIN
  Seed := ((Seed * b) + 1D) MOD m;
  RETURN ((Seed DIV m1) * max) DIV m1;
END Random;

BEGIN
  Seed := 7390431D;
END RandomNumbers.
