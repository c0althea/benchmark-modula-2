(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                      (c) 1986, 1987 by Leon Frenkel                      *
 ****************************************************************************
 * Name: SimpleGadgets.MOD                 Version: Amiga.00.00             *
 * Created: 04/17/87   Updated: 12/25/87   Author: Leon Frenkel             *
 * Description: Simplified Intuition gadget functions.                      *
 ****************************************************************************)

IMPLEMENTATION MODULE SimpleGadgets;

FROM SYSTEM    IMPORT ADDRESS, TSIZE, ADR, BYTE;
FROM Intuition IMPORT
	Gadget, GadgetPtr,
	StringInfo, StringInfoPtr,
	PropInfo, PropInfoPtr,
	PropInfoFlags, PropInfoFlagsSet,
	GadgetFlags, GadgetFlagsSet,
	GadgetActivation, GadgetActivationSet,
	GadgHComp,
	GadgetTypeSet, BoolGadget, StrGadget, PropGadget,
	GzzGadget, ReqGadget,
	GadgetMutualExcludeSet,
	IntuiText, IntuiTextPtr,
	Border, BorderPtr,
	AutoFrontPen, AutoBackPen, AutoDrawMode,
	Image, ImagePtr,
	IntuiTextLength;
FROM Memory    IMPORT AllocMem, FreeMem, MemReqSet, MemClear;
FROM Rasters   IMPORT Jam1, Jam2;

(*$L+*)

TYPE
  MemBlockPtr = POINTER TO MemBlock;
  MemBlock = RECORD
               Next : MemBlockPtr;
               Size : CARDINAL;
             END;

VAR
  MemAllocList   : MemBlockPtr;	(* list of structures currently allocated *)
  MemListTail    : MemBlockPtr;

  MemoryOk       : BOOLEAN; (* TRUE = memory ok, FALSE = ran out of memory *)

  GadHead        : GadgetPtr; 		(* head of gadget linked list *)

  GadFlags       : GadgetFlagsSet;	(* Global Gadget Flags *)
  GadActivation  : GadgetActivationSet; (* Global Activation Flags *)
  GadType        : GadgetTypeSet;	(* Gadget Type Flags *)
  GadID          : CARDINAL;		(* Next gadget ID *)

(* ====== PRIVATE ======= *)
PROCEDURE Free(blockList: MemBlockPtr);
VAR
  block : MemBlockPtr;
BEGIN
  WHILE (blockList # NIL) DO
    block := blockList;
    blockList := blockList^.Next;
    FreeMem(block, block^.Size);
  END;
END Free;

PROCEDURE Alloc(size: CARDINAL): ADDRESS;
VAR
  block : MemBlockPtr;
BEGIN
  IF MemoryOk THEN
    INC(size, TSIZE(MemBlock));
    block := AllocMem(size, MemReqSet{MemClear});
    IF block # NIL THEN
      block^.Next := MemAllocList;
      block^.Size := size;
      MemAllocList := block;
    ELSE
      Free(MemAllocList);
      LastGadget := NIL;
      MemoryOk := FALSE;
    END;
  END;
  RETURN (ADDRESS(block) + LONGCARD(TSIZE(MemBlock)));
END Alloc;

PROCEDURE LinkGadget(newGad: GadgetPtr);
BEGIN
  IF GadHead = NIL THEN
    GadHead := newGad;
    MemListTail := ADR(newGad^) - LONGCARD(TSIZE(MemBlock));
  ELSE
    LastGadget^.NextGadget := newGad;
  END;
  LastGadget := newGad;

  INC(GadID);
END LinkGadget;


(* ====== PUBLIC ======== *)

PROCEDURE AddGadgetTextButton(left, top: INTEGER; name: ADDRESS);
VAR
  newGad : GadgetPtr;
  newTxt : IntuiTextPtr;
  newBor : BorderPtr;
  newXY  : POINTER TO ARRAY [0..4] OF RECORD x, y: INTEGER; END;
  txtLeft, txtTop, gadWidth, gadHeight : INTEGER;
  boxWidth, boxHeight : INTEGER;
BEGIN
  newGad := Alloc(TSIZE(Gadget));
  newTxt := Alloc(TSIZE(IntuiText));
  newBor := Alloc(TSIZE(Border));
  newXY  := Alloc(SIZE(newXY^));
  IF MemoryOk THEN
    WITH newTxt^ DO
      FrontPen  := BYTE(1);
      BackPen   := BYTE(0);
      DrawMode  := Jam1;
      LeftEdge  := 4;
      TopEdge   := 1;
      ITextFont := GadgetTextAttr;
      IText     := name;
      NextText  := NIL;
    END;
    gadWidth  := IntuiTextLength(newTxt^) + 8;
    gadHeight := 10;

    boxWidth  := gadWidth + 1;
    boxHeight := gadHeight + 1;

    WITH newBor^ DO
      LeftEdge   := -1;
      TopEdge    := -1;
      FrontPen   := BYTE(1);
      BackPen    := BYTE(0);
      DrawMode   := Jam1;
      Count      := BYTE(5);
      XY         := newXY;
      NextBorder := NIL;
    END;
    WITH newXY^[0] DO
      x := 0;
      y := 0;
    END;
    WITH newXY^[1] DO
      x := boxWidth;
      y := 0;
    END;
    WITH newXY^[2] DO
      x := boxWidth;
      y := boxHeight;
    END;
    WITH newXY^[3] DO
      x := 0;
      y := boxHeight;
    END;
    WITH newXY^[4] DO
      x := 0;
      y := 0;
    END;

    WITH newGad^ DO
      NextGadget    := NIL;
      LeftEdge      := left;
      TopEdge       := top;
      Width         := gadWidth;
      Height        := gadHeight;
      Flags         := GadFlags;
      Activation    := GadActivation;
      GadgetType    := GadType + BoolGadget;

      GadgetRender  := NIL;
      IF (GadgetBorder) THEN (* Draw Border *)
        GadgetRender := newBor;
      END;

      SelectRender  := NIL;
      GadgetText    := newTxt;
      MutualExclude := GadgetMutualExcludeSet{};
      SpecialInfo   := NIL;
      GadgetID      := GadID;
      UserData      := NIL;
    END;

    LinkGadget(newGad);
  END;
END AddGadgetTextButton;

PROCEDURE AddGadgetImageButton(left, top: INTEGER; VAR image: Image);
VAR
  newGad : GadgetPtr;
BEGIN
  newGad := Alloc(TSIZE(Gadget));

  IF MemoryOk THEN
    WITH newGad^ DO
      NextGadget    := NIL;
      LeftEdge      := left;
      TopEdge       := top;
      Width         := image.Width;
      Height        := image.Height;
      Flags         := GadFlags + GadgetFlagsSet{GadgImage};
      Activation    := GadActivation;
      GadgetType    := GadType + BoolGadget;
      GadgetRender  := ADR(image);
      SelectRender  := NIL;
      GadgetText    := NIL;
      MutualExclude := GadgetMutualExcludeSet{};
      SpecialInfo   := NIL;
      GadgetID      := GadID;
      UserData      := NIL;
    END;

    LinkGadget(newGad);
  END;
END AddGadgetImageButton;


PROCEDURE AddGadgetString(left, top: INTEGER; dispField, bufSize: CARDINAL;
                          initStr: ADDRESS);
VAR
  newGad : GadgetPtr;
  newStr : StringInfoPtr;
  newBor : BorderPtr;
  newXY  : POINTER TO ARRAY [0..4] OF RECORD x, y: INTEGER; END;
  newBuf : ADDRESS;
  newUndo: ADDRESS;
  boxWidth, boxHeight : INTEGER;
  cp: ADDRESS;
BEGIN
  newGad := Alloc(TSIZE(Gadget));
  newStr := Alloc(TSIZE(StringInfo));
  newBuf := Alloc(bufSize);
  newUndo:= Alloc(bufSize);
  newBor := Alloc(TSIZE(Border));
  newXY  := Alloc(SIZE(newXY^));

  IF MemoryOk THEN
    WITH newBor^ DO
      LeftEdge   := -2;
      TopEdge    := -2;
      FrontPen   := BYTE(1);
      BackPen    := BYTE(0);
      DrawMode   := Jam1;
      Count      := BYTE(5);
      XY         := newXY;
      NextBorder := NIL;
    END;
    boxWidth  := (dispField * 8) + 4;
    boxHeight := 11;
    WITH newXY^[0] DO
      x := 0;
      y := 0;
    END;
    WITH newXY^[1] DO
      x := boxWidth;
      y := 0;
    END;
    WITH newXY^[2] DO
      x := boxWidth;
      y := boxHeight;
    END;
    WITH newXY^[3] DO
      x := 0;
      y := boxHeight;
    END;
    WITH newXY^[4] DO
      x := 0;
      y := 0;
    END;

    newBuf^  := 0C;
    newUndo^ := 0C;
    WITH newStr^ DO
      Buffer     := newBuf;
      UndoBuffer := newUndo;
      BufferPos  := 0;
      MaxChars   := bufSize;
      DispPos    := 0;
    END;

    WITH newGad^ DO
      NextGadget    := NIL;
      LeftEdge      := left;
      TopEdge       := top;
      Width         := dispField * 8;
      Height        := 10;
      Flags         := GadFlags;
      Activation    := GadActivation;
      GadgetType    := GadType + StrGadget;

      GadgetRender  := NIL;
      IF (GadgetBorder) THEN (* Draw Border *)
        GadgetRender  := newBor;
      END;
      SelectRender  := NIL;
      GadgetText    := NIL;
      MutualExclude := GadgetMutualExcludeSet{};
      SpecialInfo   := newStr;
      GadgetID      := GadID;
      UserData      := NIL;
    END;

    (* Copy initStr into buffer *)
    IF (initStr # NIL) THEN
      cp := newBuf;
      WHILE (CHAR(initStr^) # 0C) DO;
        cp^ := initStr^;
        INC(cp);
        INC(initStr);
      END;
      (* No need to terminate since buffer was initialized to 0 *)
    END;

    LinkGadget(newGad);
  END;
END AddGadgetString;

PROCEDURE AddGadgetInteger(left, top: INTEGER; dispField, bufSize: CARDINAL;
                           initValue: LONGINT);
VAR
  i             : CARDINAL;
  stringInfoPtr : StringInfoPtr;
  digitStr      : ARRAY [0..31] OF CHAR;

  PROCEDURE NumToStr(n: LONGINT);
  BEGIN
    IF (n > 9D) THEN
      NumToStr(n DIV 10D);
    END;
    digitStr[i] := CHR((n MOD 10D) + LONGINT('0')); INC(i);
  END NumToStr;

BEGIN
  i := 0;
  IF (initValue < 0D) THEN (* Generate minus sign *)
    digitStr[0] := "-";
    i := 1;
  END;
  NumToStr(ABS(initValue));
  digitStr[i] := 0C; (* terminate string *)

  AddGadgetString(left, top, dispField, bufSize, ADR(digitStr));
  IF MemoryOk THEN
    INCL(LastGadget^.Activation, LongInt);
    stringInfoPtr := LastGadget^.SpecialInfo;
    stringInfoPtr^.LongInt := initValue;
  END;
END AddGadgetInteger;

PROCEDURE AddGadgetProp(left, top, width, height: INTEGER;  hOn, vOn: BOOLEAN;
                        hPot, vPot, hBody, vBody: CARDINAL);
VAR
  newGad : GadgetPtr;
  newProp: PropInfoPtr;
  newImg : ImagePtr;
BEGIN
  newGad  := Alloc(TSIZE(Gadget));
  newProp := Alloc(TSIZE(PropInfo));
  newImg  := Alloc(TSIZE(Image));

  IF MemoryOk THEN
    WITH newProp^ DO
      Flags := PropInfoFlagsSet{AutoKnob};
      IF hOn THEN
        INCL(Flags, FreeHoriz);
      END;
      IF vOn THEN
        INCL(Flags, FreeVert);
      END;      
      HorizPot  := hPot;
      VertPot   := vPot;
      HorizBody := hBody;
      VertBody  := vBody;
    END;

    WITH newGad^ DO
      NextGadget    := NIL;
      LeftEdge      := left;
      TopEdge       := top;
      Width         := width;
      Height        := height;
      Flags         := GadFlags;
      Activation    := GadActivation;
      GadgetType    := GadType + PropGadget;
      GadgetRender  := newImg;
      SelectRender  := NIL;
      GadgetText    := NIL;
      MutualExclude := GadgetMutualExcludeSet{};
      SpecialInfo   := newProp;
      GadgetID      := GadID;
      UserData      := NIL;
    END;

    LinkGadget(newGad);
  END;
END AddGadgetProp;


PROCEDURE BeginGadgetList;
BEGIN
  GadHead       := NIL;
  LastGadget    := NIL;
  MemAllocList  := NIL;
  MemoryOk      := TRUE;
  GadID         := FirstGadgetID;
  GadFlags      := DefaultGadgetFlags;
  GadActivation := DefaultGadgetActivation;
  GadType       := GadgetTypeSet{};
  IF GadgetTypeGzz THEN
    GadType := GzzGadget;
  END;
  IF GadgetTypeReq THEN
    GadType := ReqGadget;
  END;
END BeginGadgetList;

PROCEDURE EndGadgetList(): GadgetPtr;
BEGIN
  IF MemoryOk THEN
    MemListTail^.Next := MemAllocList;

    RETURN (GadHead);
  ELSE
    RETURN (NIL);
  END;
END EndGadgetList;

PROCEDURE FreeGadgetList(VAR gadget: Gadget);
VAR
  start, end : MemBlockPtr;
BEGIN
  end := ADR(gadget) - LONGCARD(TSIZE(MemBlock));
  start := end^.Next;
  end^.Next := NIL;
  Free(start);
END FreeGadgetList;

PROCEDURE GadgetOpt(flags: GadgetFlagsSet; activ: GadgetActivationSet;
                    exclude: GadgetMutualExcludeSet);
BEGIN
  IF (LastGadget # NIL) THEN
    WITH LastGadget^ DO
      Flags         := flags;
      Activation    := activ;
      MutualExclude := exclude;
    END;
  END;
END GadgetOpt;

PROCEDURE GlobalGadgetOpt(flags: GadgetFlagsSet; activ: GadgetActivationSet);
BEGIN
  GadFlags := flags;
  GadActivation := activ;
END GlobalGadgetOpt;

BEGIN
(*GadgetTypeGzz := FALSE;*)
(*GadgetTypeReq := FALSE;*)
  DefaultGadgetFlags := GadgHComp;
  DefaultGadgetActivation := GadgetActivationSet{RelVerify};
  GadgetBorder := TRUE;
END SimpleGadgets.
