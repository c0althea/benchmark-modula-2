(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: FileSystem.MOD                    Version: Amiga.00.00             *
 * Created: 11/19/86   Updated: 08/01/87   Author: Leon Frenkel             *
 * Description: This module represents file I/O at a level above the        *
 *  AmigaDOS module. This module is described in appendix 2 of Programming  *
 *  in Modula-2 by N. Wirth. For purposes of efficency this module performs *
 *  it own buffering. This resultst in much faster I/O operations.          *
 ****************************************************************************)

IMPLEMENTATION MODULE FileSystem;

FROM SYSTEM IMPORT ADDRESS, BYTE, WORD, LONG, ADR;
FROM AmigaDOS IMPORT FileHandle, ModeOldFile, ModeNewFile, OffsetBeginning,
     OffsetCurrent, OffsetEnd; IMPORT AmigaDOS;
FROM Memory IMPORT AllocMem, FreeMem, MemReqSet;

(*$L+,D-*)

CONST
  DefaultBufferSize = 1024;

(**********************)
(* private procedures *)
(**********************)

(* FlushBuffer - save current contents of buffer *)
PROCEDURE FlushBuffer(VAR f: File);
VAR
  r : LONGCARD;
BEGIN
  WITH f DO
    IF (res # done) THEN (* abort on error *)
       RETURN;
    END;

    IF filePos # bufferBase THEN
      r := AmigaDOS.Seek(handle, bufferBase, OffsetBeginning);
    END;

    filePos := bufferBase + bufferEnd;
    bufferChanged := FALSE;
    r := AmigaDOS.Write(handle, bufferPtr, bufferEnd);
    IF r = -1D THEN
      res := notdone;
      err := AmigaDOS.IoErr();
    END;
  END; (* WITH *)
END FlushBuffer;

(* LoadNewBuffer - flush old buffer if changed then load new buffer starting
 *  from a new position in the file *)
PROCEDURE LoadNewBuffer(VAR f: File; newpos: LONGCARD);
VAR
  r : LONGCARD;
BEGIN
  WITH f DO
    IF bufferChanged THEN FlushBuffer(f); END;
    IF (res # done) THEN (* abort on error *)
      bufferOffset := 0D; (* reset to start of buffer *)
      RETURN; 
    END;

    IF filePos # newpos THEN
      r := AmigaDOS.Seek(handle, newpos, OffsetBeginning);
    END;
    r := AmigaDOS.Read(handle, bufferPtr, bufferSize);
    IF r = -1D THEN
      res := notdone;
      err := AmigaDOS.IoErr();
      r := bufferSize; (* new bufferEnd *)
    END;

    bufferBase := newpos;
    bufferOffset := 0;
    bufferEnd := r;

    (* Calculate new filePos after read operation *)
    filePos := bufferBase + bufferEnd;
  END; (* WITH *)
END LoadNewBuffer;

(*********************)
(* public procedures *)
(*********************)

PROCEDURE Close(VAR f: File);
BEGIN
  WITH f DO
    IF bufferChanged THEN FlushBuffer(f); END;
    AmigaDOS.Close(handle);
    FreeMem(bufferPtr, bufferSize);
  END; (* WITH *)
END Close;

PROCEDURE Delete(filename: ARRAY OF CHAR);
VAR
  r : BOOLEAN;
BEGIN
    r := AmigaDOS.DeleteFile(ADR(filename));
END Delete;

PROCEDURE Lookup(VAR f: File; filename: ARRAY OF CHAR; new: BOOLEAN);
VAR
  r : LONGCARD;
BEGIN
  WITH f DO
    res           := done;
    eof           := FALSE;
    err           := 0;
    filePos       := 0;
    bufferSize    := BufferSize;
    bufferPtr     := AllocMem(bufferSize, MemReqSet{});
    bufferBase    := 0;
    bufferOffset  := 0;
    bufferEnd     := 0;
    bufferChanged := FALSE;

    IF bufferPtr = NIL THEN 
      res := nomemory;
      RETURN;
    END;

    IF (new) THEN (* Open new file *)
      handle := AmigaDOS.Open(ADR(filename), ModeNewFile);
      IF handle = NIL THEN
        res := notdone;
        err := AmigaDOS.IoErr();
      END;
    ELSE (* Open old file *)
      handle := AmigaDOS.Open(ADR(filename), ModeOldFile);
      IF handle = NIL THEN
        res := notdone;
        err := AmigaDOS.IoErr();
      ELSE
        LoadNewBuffer(f, 0D);
      END;
    END;
    IF res # done THEN
      FreeMem(bufferPtr, bufferSize);
    END;
  END; (* WITH *)
END Lookup;

PROCEDURE SetPos(VAR f: File; pos: LONGCARD);
BEGIN
  WITH f DO
    IF ((pos >= bufferBase) AND (pos < bufferBase + bufferEnd)) THEN
      bufferOffset := pos - bufferBase;
    ELSE
      LoadNewBuffer(f, pos);
    END;
  END; (* WITH *)
END SetPos;

PROCEDURE GetPos(VAR f: File; VAR pos: LONGCARD);
BEGIN
  WITH f DO
    pos := bufferBase + bufferOffset;
  END; (* WITH *)
END GetPos;

PROCEDURE Length(VAR f: File; VAR length: LONGCARD);
VAR
  p : LONGCARD;
BEGIN
  (* if end off file is within current buffer then simply calculate length
   * if the data in the buffer does not occupie the entire buffer then this
   * buffer represents the end of the file
   *)
  WITH f DO
    IF bufferEnd < bufferSize THEN
      length := bufferBase + bufferEnd;
    ELSE
      (* end of file is outside of buffer so Seek() must be used *)
      (* then seek to original position to restore *)
      p      := AmigaDOS.Seek(handle, 0D, OffsetEnd);
      length := AmigaDOS.Seek(handle, filePos, OffsetBeginning);
    END;
  END; (* WITH *)
END Length;

PROCEDURE ReadWord(VAR f: File; VAR w: WORD);
VAR
  conv : RECORD
           CASE :CARDINAL OF
           |0:      w : WORD;
           |1: ch, cl : CHAR;
           END;
         END;
BEGIN
  ReadChar(f, conv.ch);
  ReadChar(f, conv.cl);
  w := conv.w;
END ReadWord;

PROCEDURE WriteWord(VAR f: File; w: WORD);
VAR
  conv : RECORD
           CASE :CARDINAL OF
           |0:      w : WORD;
           |1: ch, cl : CHAR;
           END;
         END;
BEGIN
  conv.w := w;
  WriteChar(f, conv.ch);
  WriteChar(f, conv.cl);
END WriteWord;

PROCEDURE ReadChar(VAR f: File; VAR ch: CHAR);
VAR
  p : ADDRESS;
BEGIN
  (* If current position is < end of buffer pos then read char, advance ptr,
   * check if we have crossed into the next buffer?
   *)
  WITH f DO
    IF bufferOffset < bufferEnd THEN
      p := bufferPtr + ADDRESS(bufferOffset);
      ch := CHAR(p^);
      INC(bufferOffset);
      IF bufferOffset = bufferSize THEN
        LoadNewBuffer(f, bufferBase + bufferOffset);
      END;
    ELSE (* we are trying to read from the end of the file *)
      res := notdone;
      eof := TRUE;
      ch  := 0C;
    END;
  END; (* WITH *)
END ReadChar;

PROCEDURE WriteChar(VAR f: File; ch: CHAR);
VAR
  p : ADDRESS;
BEGIN
  WITH f DO
    (* Mark buffer as modified *)
    bufferChanged := TRUE;

    (* Output char to buffer *)
    p := bufferPtr + ADDRESS(bufferOffset);
    p^ := BYTE(ch);

    (* if current pos = end of buffer then move the end of buffer ptr *)
    IF bufferOffset = bufferEnd THEN
      INC(bufferEnd);
    END;
    (* move the current pos ptr *)
    INC(bufferOffset);

    (* if current pos = buffer size then we are crossing into the next buffer *)
    IF bufferOffset = bufferSize THEN
      LoadNewBuffer(f, bufferBase + bufferOffset);
    END;
  END; (* WITH *)
END WriteChar;

BEGIN
  (* Set initial buffer size *)
  BufferSize := DefaultBufferSize;
END FileSystem.
