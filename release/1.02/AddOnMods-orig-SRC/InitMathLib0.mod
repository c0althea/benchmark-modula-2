(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: InitMathLib0.MOD                  Version: Amiga.00.00             *
 * Created: 06/29/87   Updated: 06/29/87   Author: Leon Frenkel             *
 * Description: Provides for initialization of the module "MathLib0".       *
 ****************************************************************************)

IMPLEMENTATION MODULE InitMathLib0;

FROM SYSTEM IMPORT
	ADR;
FROM Libraries IMPORT
	OpenLibrary, CloseLibrary;
FROM MathLib0 IMPORT
	MathTransName, MathTransBase;

(*$L+*)

PROCEDURE OpenMathLib0(): BOOLEAN;
BEGIN
  MathTransBase := OpenLibrary(ADR(MathTransName), 0D);
  RETURN (MathTransBase # NIL);
END OpenMathLib0;

PROCEDURE CloseMathLib0();
BEGIN
  CloseLibrary(MathTransBase^);
END CloseMathLib0;

END InitMathLib0.
