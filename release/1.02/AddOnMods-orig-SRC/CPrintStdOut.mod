(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CPrintStdOut.MOD                  Version: Amiga.00.00             *
 * Created: 02/23/87   Updated: 02/23/87   Author: Leon Frenkel             *
 * Description: From the "C" language standard library, functions for       *
 *  doing formated output to the "stdout" stream, throught module CStdIO.   *
 ****************************************************************************)

IMPLEMENTATION MODULE CPrintStdOut;

FROM SYSTEM IMPORT ADDRESS, TSIZE, ADR;
FROM FormatString IMPORT FormatArg, Format;
FROM CFileIO IMPORT write;
FROM CStdIO IMPORT fwrite, stdout;

(*$L+,D-*)

TYPE 
  (* Output buffer type *)
  Buffer = ARRAY [0..127] OF CHAR;

VAR
  bufPtr : POINTER TO Buffer;
  bufIdx : INTEGER;

PROCEDURE WriteBuf;
VAR 
  r : LONGINT;
BEGIN
  (* This function could use fwrite() but, when the stream is connected to *)
  (* a window, using fwrite() would produce very slow output!              *)
  IF stdout^.blength = 1D THEN (* unbuffered stream *)
    r := write(stdout^.fdesc, bufPtr, bufIdx);
  ELSE (* buffered stream *)
    r := fwrite(bufPtr, bufIdx, 1D, stdout);
  END;
END WriteBuf;

PROCEDURE StoreCh(c: CHAR);
VAR
  r : INTEGER;
BEGIN
  bufPtr^[bufIdx] := c;
  INC(bufIdx);
  IF bufIdx = TSIZE(Buffer) THEN
    WriteBuf;
    bufIdx := 0;
  END;
END StoreCh;

PROCEDURE printf(formatStr: ADDRESS; VAR args: ARRAY OF FormatArg);
VAR
  r      : INTEGER;
  OutBuf : Buffer;
BEGIN
  bufPtr := ADR(OutBuf);
  bufIdx := 0;
  r := Format(formatStr, args, StoreCh);
  WriteBuf;
END printf;

END CPrintStdOut.
