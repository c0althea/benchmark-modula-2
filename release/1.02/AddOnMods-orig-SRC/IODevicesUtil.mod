(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: IODevicesUtil.DEF                 Version: Amiga.00.00             *
 * Created: 01/04/87   Updated: 01/16/87   Author: Leon Frenkel             *
 * Description: Exec Support Procedures for Device I/O.                     *
 ****************************************************************************)

IMPLEMENTATION MODULE IODevicesUtil;

FROM SYSTEM    IMPORT ADR, ADDRESS, BYTE, TSIZE;
FROM IODevices IMPORT IOStdReq, IOStdReqPtr, IORequestPtr, DevicePtr, UnitPtr;
FROM Memory    IMPORT AllocMem, FreeMem, MemReqSet, MemPublic, MemClear;
FROM Nodes     IMPORT NTMessage;
FROM Ports     IMPORT MsgPort;

(*$L+*)

PROCEDURE CreateExtIO(VAR ioReplyPort: MsgPort; size: LONGINT): ADDRESS;
VAR req: IORequestPtr;
BEGIN
  IF ADR(ioReplyPort) = NIL THEN RETURN (NIL); END;

  req := AllocMem(size, MemReqSet{MemClear, MemPublic});
  IF req = NIL THEN RETURN (NIL); END;

  WITH req^ DO
    ioMessage.mnNode.lnType := NTMessage;
    ioMessage.mnLength      := CARDINAL(size);
    ioMessage.mnReplyPort   := ADR(ioReplyPort);
  END;
  RETURN (req);
END CreateExtIO;


PROCEDURE CreateStdIO(VAR ioReplyPort: MsgPort): IOStdReqPtr;
BEGIN
  RETURN CreateExtIO(ioReplyPort, TSIZE(IOStdReq));
END CreateStdIO;

PROCEDURE DeleteExtIO(ioReq: ADDRESS);
VAR req: IORequestPtr;
BEGIN
  req := ioReq;
  IF req # NIL THEN
    WITH req^ DO
      ioMessage.mnNode.lnType := BYTE(0FFH);
      ioDevice                := DevicePtr(0FFFFFFFFH);
      ioUnit                  := UnitPtr(0FFFFFFFFH);
      FreeMem(req, LONGCARD(ioMessage.mnLength));
    END;
  END;
END DeleteExtIO;


PROCEDURE DeleteStdIO(ioStdReq: IOStdReqPtr);
BEGIN
  DeleteExtIO(ioStdReq);
END DeleteStdIO;

END IODevicesUtil.

