(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CPrintBuffer.MOD                  Version: Amiga.00.00             *
 * Created: 01/18/87   Updated: 02/24/87   Author: Leon Frenkel             *
 * Description: From the "C" language standard library, functions for       *
 *  doing formated output to a Buffer.                                      *
 ****************************************************************************)

IMPLEMENTATION MODULE CPrintBuffer;

FROM SYSTEM       IMPORT ADDRESS;
FROM FormatString IMPORT FormatArg, Format;

(*$L+,D-*)

VAR
  bp : ADDRESS;

PROCEDURE StoreCh(c: CHAR);
BEGIN
  bp^ := c;  
  INC(bp);
END StoreCh;

PROCEDURE sprintf(destStr: ADDRESS; formatStr: ADDRESS;
                  VAR args: ARRAY OF FormatArg): CARDINAL;
VAR
  l : CARDINAL;
BEGIN
  bp := destStr;
  l := Format(formatStr, args, StoreCh);  
  bp^ := 0C; (* null-terminate string *)
  RETURN (l); (* length *)
END sprintf;

END CPrintBuffer.
