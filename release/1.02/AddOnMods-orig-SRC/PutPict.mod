(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: PutPict.MOD                       Version: Amiga.00.00             *
 * Created: 04/22/87   Updated: 04/22/87   Author: Leon Frenkel             *
 * Description: Output an ILBM raster image file.                           *
 ****************************************************************************)

IMPLEMENTATION MODULE PutPict;

FROM SYSTEM   IMPORT ADDRESS, TSIZE, ADR;
FROM AmigaDOS IMPORT FileHandle;
FROM GIO      IMPORT GWriteDeclare, GWriteUndeclare;
FROM Graphics IMPORT BitMap;
FROM IFF      IMPORT IFFP, IFFOkay, DOSError, GroupContext, szNotYetKnown,
		     FORM;
FROM IFFW     IMPORT OpenWIFF, EndWGroup, PutCk, StartWGroup, CloseWGroup;
FROM ILBM     IMPORT BitMapHeader, mskNone, cmpByteRun1, IDILBM, IDBMHD;
FROM ILBMW    IMPORT PutCMAP, PutBODY, InitBMHdr;

(*$L+*)

CONST
  MaxDepth = 5;
  BODYBufSize = 512D;

VAR
  ifferror : IFFP;


PROCEDURE IffErr(): IFFP;
VAR
  i : IFFP;
BEGIN
  i := ifferror;
  ifferror := 0D;
  RETURN (i);
END IffErr;

PROCEDURE PutPicture(file: FileHandle; VAR bm: BitMap; pageW, pageH: CARDINAL;
                 colorMap: ADDRESS; buffer: ADDRESS; bufsize: LONGINT): BOOLEAN;
VAR
  bmHdr : BitMapHeader;
  fileContext, formContext : GroupContext;
BEGIN
  ifferror := InitBMHdr(bmHdr, bm, mskNone, cmpByteRun1, 0, pageW, pageH);

  (* use buffered write for speedup, if it is big-enough for both
   * PutBODY's buffer and a gio buffer.*)
  IF (ifferror = IFFOkay) AND (bufsize > 2D * BODYBufSize) THEN
    IF GWriteDeclare(file, buffer+BODYBufSize, bufsize-BODYBufSize) < 0D THEN
      ifferror := DOSError;
    END;
    bufsize := BODYBufSize;
  END;

  IF ifferror = IFFOkay THEN
    ifferror := OpenWIFF(file, fileContext, szNotYetKnown);
  END;
  IF ifferror = IFFOkay THEN
    ifferror := StartWGroup(fileContext,FORM,szNotYetKnown,IDILBM,formContext);
  END;
  IF ifferror = IFFOkay THEN
    ifferror := PutCk(formContext,IDBMHD,TSIZE(BitMapHeader), ADR(bmHdr));
  END;
  IF colorMap # NIL THEN
    IF ifferror = IFFOkay THEN
      ifferror := PutCMAP(formContext, colorMap, bm.Depth);
    END;
  END;
  IF ifferror = IFFOkay THEN
    ifferror := PutBODY(formContext, bm, NIL, bmHdr, buffer, bufsize);
  END;
  IF ifferror = IFFOkay THEN
    ifferror := EndWGroup(formContext);
  END;
  IF ifferror = IFFOkay THEN
    ifferror := CloseWGroup(fileContext);
  END;
  IF (GWriteUndeclare(file) < 0D) AND (ifferror = IFFOkay) THEN
    ifferror := DOSError;
  END;
  RETURN (ifferror # IFFOkay);
END PutPicture;

END PutPict.
