(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                      (c) 1986, 1987 by Leon Frenkel                      *
 ****************************************************************************
 * Name: SimpleWindows.MOD                 Version: Amiga.00.00             *
 * Created: 04/03/87   Updated: 07/22/87   Author: Leon Frenkel             *
 * Description: Simplified Intuition Window functions.                      *
 ****************************************************************************)

IMPLEMENTATION MODULE SimpleWindows;

FROM SYSTEM IMPORT
	ADDRESS, BYTE, LONGWORD;
FROM GraphicsBase IMPORT
	GfxBasePtr;
FROM Intuition IMPORT
	NewWindow, Window, WindowPtr, IDCMPFlagsSet, 
	WindowFlagsSet, GadgetPtr, ScreenPtr, WBenchScreen,
	CustomScreen, ScreenFlagsSet,
	OpenWindow, CloseWindow;
FROM System IMPORT
	GfxBase;

PROCEDURE CreateWindow(x, y, width, height: INTEGER; 
                       IDCMP: LONGWORD; (* IDCMPFlagsSet *)
                       windowFlags: LONGWORD; (* WindowFlagsSet *)
                       gadgets: ADDRESS; (* GadgetPtr *)
                       screen: ADDRESS; (* ScreenPtr; *)
                       title: ADDRESS): ADDRESS; (* WindowPtr *)
VAR
  new : NewWindow;
BEGIN
  WITH new DO
    LeftEdge    := x;
    TopEdge     := y;
    Width       := width;
    Height      := height;
    DetailPen   := WindowDetailPen;
    BlockPen    := WindowBlockPen;
    IDCMPFlags  := IDCMPFlagsSet(IDCMP);
    Flags       := WindowFlagsSet(windowFlags);
    FirstGadget := gadgets;
    CheckMark   := WindowCheckMark;
    Title       := title;
    Screen      := screen;
    BitMap      := WindowBitMap;
    MinWidth    := WindowMinWidth;
    MinHeight   := WindowMinHeight;
    MaxWidth    := WindowMaxWidth;
    MaxHeight   := WindowMaxHeight;
    IF (screen = NIL) THEN
      Type := WBenchScreen;
    ELSE
      Type := CustomScreen;
    END;
  END;
  RETURN OpenWindow(new);
END CreateWindow;

VAR
  gfx: GfxBasePtr;

BEGIN
  gfx := GfxBase;

  WindowDetailPen := BYTE(-1);
  WindowBlockPen  := BYTE(-1);

(* WindowCheckMark := NIL; *)
(* WindowBitMap := NIL; *)

  WindowMinWidth := 40;
  WindowMaxWidth := gfx^.NormalDisplayColumns;
  WindowMinHeight:= 40;
  WindowMaxHeight:= gfx^.NormalDisplayRows;
END SimpleWindows.
