(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CScanWindow.MOD                   Version: Amiga.00.00             *
 * Created: 04/06/87   Updated: 05/21/87   Author: Leon Frenkel             *
 * Description: From the "C" language standard library, functions for       *
 *  doing formated input from a window using the module SimpleConsole.      * 
 ****************************************************************************)

IMPLEMENTATION MODULE CScanWindow;

FROM SYSTEM        IMPORT ADDRESS, BYTE, ADR;
FROM Intuition     IMPORT Window, WindowPtr;
FROM ScanString    IMPORT ScanArg, Scan;
IMPORT SimpleConsole;

(*$L+*)

CONST
  EOF = -1;

VAR
  wPtr : WindowPtr;

PROCEDURE GetCh(flag: BOOLEAN): INTEGER;
BEGIN
  IF NOT flag THEN
    RETURN INTEGER(SimpleConsole.GetCh(wPtr^));
  ELSE
    SimpleConsole.GetAgain(wPtr^);
    RETURN (0); (* Dummy value *)
  END;
END GetCh;

PROCEDURE wscanf(VAR window: ARRAY OF BYTE; (* Window *)
                 formatStr: ADDRESS;
                 VAR args: ARRAY OF ScanArg): INTEGER;
BEGIN
  wPtr := ADR(window);
  RETURN Scan(formatStr, args, GetCh);
END wscanf;

END CScanWindow.
