(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CScanFile.MOD                     Version: Amiga.00.00             *
 * Created: 01/20/87   Updated: 02/24/87   Author: Leon Frenkel             *
 * Description: From the "C" language standard library functions for        *
 *  doing formated input from a buffered file.                              *
 ****************************************************************************)

IMPLEMENTATION MODULE CScanFile;

FROM SYSTEM IMPORT ADR, ADDRESS;
FROM CStdIO IMPORT FILE, getc, ungetc;
FROM ScanString IMPORT Scan, ScanArg;

(*$L+,D-*)

CONST
  EOF = -1;

VAR
  fp  : FILE;
  last: INTEGER;

PROCEDURE GetCh(flag: BOOLEAN): INTEGER;
BEGIN
  IF NOT flag THEN (* get new char *)
    last := getc(fp);
  ELSE (* put back char *)
    IF last # EOF THEN (* can't put back EOF *)
      last := ungetc(CHR(last),fp);
    END;
  END;
  RETURN (last);
END GetCh;

PROCEDURE fscanf(f: FILE; formatStr: ADDRESS;
                 VAR args: ARRAY OF ScanArg): INTEGER;
BEGIN
  fp := f;
  last := 0;
  RETURN Scan(formatStr, args, GetCh);
END fscanf;

END CScanFile.
