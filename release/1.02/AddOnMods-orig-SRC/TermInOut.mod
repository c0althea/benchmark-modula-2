(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: TermInOut.MOD                     Version: Amiga.00.00             *
 * Created: 08/07/87   Updated: 12/25/87   Author: Leon Frenkel             *
 * Description: A very small version of InOut for output to terminal ONLY!  *
 ****************************************************************************)

IMPLEMENTATION MODULE TermInOut;

FROM SYSTEM IMPORT ADDRESS, SETREG, INLINE, ADR;
FROM System IMPORT DOSBase, StdInput, StdOutput;

(*$L+,D-*)

  CONST ESC = 33C; DEL = 10C;(*BackSpace Key*)  NL = 79;
  VAR name: ARRAY [0..NL] OF CHAR; (* used as output buffer *)
      bufIdx: CARDINAL; (* buffer index *)

(* ========================== Local AmigaDOS Stuff ===================== *)
CONST
  D0 = 0; D1 = 1; D2 = 2; D3 = 3; D4 = 4; D5 = 5; D6 = 6; D7 = 7;
  A0 = 8; A1 = 9; A2 =10; A3 =11; A4 =12; A5 =13; A6 =14; A7 =15;

PROCEDURE DOSWrite(buffer: ADDRESS; length: CARDINAL);
CONST
  LVOWrite = 0FFD0H;
BEGIN
  SETREG(A6, DOSBase);
  SETREG(D3, LONGCARD(length));
  SETREG(D1, StdOutput);
  SETREG(D2, buffer);
  INLINE(4EAEH, LVOWrite); (* JSR LVOWrite(A6) *)
END DOSWrite;
(* ===================================================================== *)

  (* ================================================================== *)
  (* Procedures For Improving Output Performance Of InOut Module        *)

  (* Flush Output Buffer *)
  PROCEDURE FlushBuf();
  BEGIN
    DOSWrite(ADR(name), bufIdx);
    bufIdx := 0; (* reset buffer index *)
  END FlushBuf;

  (* Send Char To Output Buffer *)
  PROCEDURE WriteBuf(c: CHAR);
  BEGIN
    name[bufIdx] := c; INC(bufIdx);
    IF (bufIdx = HIGH(name)) THEN (* flush out buffer *)
      FlushBuf();
    END;
  END WriteBuf;
  (* ================================================================== *)

  PROCEDURE Read(VAR ch: CHAR);
  CONST
    LVORead = 0FFD6H;
  BEGIN
    SETREG(A6, DOSBase);
    SETREG(D2, ADR(ch));
    SETREG(D1, StdInput);
    SETREG(D3, 1D);
    INLINE(4EAEH, LVORead); (* JSR LVORead(A6) *)
  END Read;

  PROCEDURE Write(ch: CHAR);
  BEGIN
    DOSWrite(ADR(ch), 1)
  END Write;

  PROCEDURE ReadString(VAR s: ARRAY OF CHAR);
    VAR i,n: CARDINAL; ch: CHAR;
  BEGIN i := 0; n := HIGH(s);
      REPEAT Read(ch) UNTIL (ch > " ") OR (ch = ESC);
      WHILE (ch = DEL) OR (ch > " ") DO
        IF ch = DEL THEN
          IF i > 0 THEN
            DEC(i);
            IF (Echo) THEN
              Write(DEL); (* Backup one char *)
              Write(" "); (* Clear char *)
              Write(DEL); (* Backup again *)
            END;
          END
        ELSE
          IF i <= n THEN s[i] := ch; INC(i); 
            IF (Echo) THEN Write(ch); END;
          END
        END ;
        Read(ch)
      END;
      IF i <= n THEN s[i] := 0C END ;
      termCH := ch
  END ReadString;

  PROCEDURE ReadInt(VAR x: INTEGER);
    VAR i: INTEGER; n: CARDINAL;
      ch: CHAR; neg: BOOLEAN;
      buf: ARRAY [0..9] OF CHAR;

    PROCEDURE next;
    BEGIN ch := buf[n]; INC(n);
    END next;

  BEGIN ReadString(buf); n := 0; next;
    WHILE ch = " " DO next END ;
    IF ch = "-" THEN
      neg := TRUE; next
    ELSE neg := FALSE;
      IF ch = "+" THEN next END
    END ;
    IF ("0" <= ch) & (ch <= "9") THEN
      i := 0; Done := TRUE;
      REPEAT i := 10*i + (INTEGER(ch) - 60B); next
      UNTIL (ch < "0") OR ("9" < ch);
      IF neg THEN x := -i ELSE x := i END
    ELSE Done := FALSE
    END
  END ReadInt;

  PROCEDURE ReadCard(VAR x: CARDINAL);
    VAR i,n: CARDINAL;
      ch: CHAR;
      buf: ARRAY [0..9] OF CHAR;

    PROCEDURE next;
    BEGIN ch := buf[n]; INC(n);
    END next;

  BEGIN ReadString(buf); n := 0; next;
    WHILE ch = " " DO next END ;
    IF ("0" <= ch) & (ch <= "9") THEN
      i := 0; Done := TRUE;
      REPEAT i := 10*i + (CARDINAL(ch) - 60B); next
      UNTIL (ch < "0") OR ("9" < ch);
      x := i
    ELSE Done := FALSE
    END
  END ReadCard;

  PROCEDURE WriteLn;
  BEGIN
    Write(EOL);
  END WriteLn;

  PROCEDURE WriteString(s: ARRAY OF CHAR);
    VAR i: CARDINAL;
  BEGIN
    i := 0;
    WHILE (i <= HIGH(s)) AND (s[i] # 0C) DO INC(i); END;
    DOSWrite(ADR(s), i);
  END WriteString;

  PROCEDURE WriteInt(x: INTEGER; n: CARDINAL);
    VAR i, x0: CARDINAL;
      a: ARRAY [0..6] OF CHAR;
  BEGIN i := 0; x0 := ABS(x);
    REPEAT a[i] := CHAR(x0 MOD 10 + 60B);
      x0 := x0 DIV 10; INC(i);
    UNTIL x0 = 0;
    IF x < 0 THEN a[i] := "-"; INC(i); END ;
    WHILE n > i DO
      DEC(n); WriteBuf(" ")
    END ;
    REPEAT DEC(i); WriteBuf(a[i]) UNTIL i = 0;
    FlushBuf();
  END WriteInt;

  PROCEDURE WriteCard(x,n: CARDINAL);
    VAR i: CARDINAL;
      a: ARRAY [0..6] OF CARDINAL;
  BEGIN i := 0;
    REPEAT a[i] := x MOD 10; x := x DIV 10; INC(i);
    UNTIL x = 0;
    WHILE n > i DO
      DEC(n); WriteBuf(" ")
    END ;
    REPEAT DEC(i); WriteBuf(CHAR(a[i]+60B)) UNTIL i = 0;
    FlushBuf();
  END WriteCard;

  PROCEDURE WriteOct(x,n: CARDINAL);
    VAR i: CARDINAL;
      a: ARRAY [0..6] OF CARDINAL;
  BEGIN i := 0;
    REPEAT a[i] := x MOD 8; x := x DIV 8; INC(i);
    UNTIL i = 5;
    IF x = 0 THEN a[i] := 0 ELSE a[i] := 1 END ;
    INC(i);
    WHILE n > i DO
      DEC(n); WriteBuf(" ")
    END ;
    REPEAT DEC(i); WriteBuf(CHAR(a[i]+60B)) UNTIL i = 0;
    FlushBuf();
  END WriteOct;


  PROCEDURE HexDig(d: CARDINAL);
  BEGIN d := d MOD 16;
    IF d < 10 THEN INC(d, 60B); ELSE INC(d, 67B); END ;
    WriteBuf(CHAR(d))
  END HexDig;
  PROCEDURE WriteHex(x,n: CARDINAL);
  BEGIN
    WHILE n > 4 DO
      DEC(n); WriteBuf(" ")
    END ;
    HexDig(x DIV 1000H); HexDig(x DIV 100H);
    HexDig(x DIV 10H);  HexDig(x);
    FlushBuf();
  END WriteHex;

BEGIN
  (* Done := FALSE *)
  (* bufIdx := 0; *)
  Echo := TRUE;
END TermInOut.
