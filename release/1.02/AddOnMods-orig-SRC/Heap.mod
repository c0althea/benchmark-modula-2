(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: Heap.MOD                          Version: Amiga.00.00             *
 * Created: 07/06/87   Updated: 01/06/88   Author: Leon Frenkel             *
 * Description: Memory management module as defined in appendix 2 of        *
 *  "Programming in Modula-2" by N. Wirth. This module is included to       *
 *  allow programs written on other systems to be ported and vice versa,    *
 *  to access the full memory management capabilities of the Amiga use the  *
 *  module "Memory".                                                        *
 *  This module may be used instead of Storage if it is necessary to be     *
 *  able to deallocate all memory allocated so far quickly, by using the    *
 *  FreeHeap() procedure.                                                   *
 ****************************************************************************)

IMPLEMENTATION MODULE Heap;

FROM SYSTEM IMPORT ADDRESS, TSIZE;
FROM Memory IMPORT AllocMem, FreeMem, MemReqSet;

(*$L+*)

TYPE 
  (* Header attached to each memory block allocated *)
  MemBlockPtr = POINTER TO MemBlock;
  MemBlock = RECORD
               Next: MemBlockPtr;
               Prev: MemBlockPtr;
               Size: LONGCARD;
             END;

VAR
  (* Linked List Of Memory Blocks Currently Allocated *)
  Free : MemBlockPtr;

PROCEDURE ALLOCATE(VAR a: ADDRESS; size: LONGCARD);
VAR
  ptr : MemBlockPtr;
BEGIN
  ptr := AllocMem(size + LONGCARD(TSIZE(MemBlock)), MemReqSet{});
  IF (ptr # NIL) THEN (* Memory Allocated *)
    ptr^.Next := Free;
    IF (Free # NIL) THEN
      Free^.Prev := ptr;
    END;
    ptr^.Prev := NIL;
    ptr^.Size := size;
    Free := ptr;
    a := ADDRESS(ptr) + LONGCARD(TSIZE(MemBlock));
  ELSE (* No Memory *)
    a := NIL;
  END;
END ALLOCATE;

PROCEDURE DEALLOCATE(VAR a: ADDRESS; size: LONGCARD);
VAR
  mp, xp, xn: MemBlockPtr;
BEGIN
  mp := a - LONGCARD(TSIZE(MemBlock));
  xp := mp^.Prev;
  xn := mp^.Next;

  IF (xn # NIL) THEN
    xn^.Prev := xp;
  END;

  IF (xp = NIL) THEN
    Free := xn;
  ELSE
    xp^.Next := xn;
  END;
 
  FreeMem(mp, mp^.Size + LONGCARD(TSIZE(MemBlock)));
END DEALLOCATE;

PROCEDURE Available(size: LONGCARD): BOOLEAN;
VAR
  p : ADDRESS;
BEGIN
  p := AllocMem(size, MemReqSet{});
  IF (p # NIL) THEN
    FreeMem(p, size);
  END;
  RETURN(p # NIL);
END Available;

PROCEDURE FreeHeap();
VAR
  mp, xp: MemBlockPtr;
BEGIN
  mp := Free;
  WHILE (mp # NIL) DO
    xp := mp^.Next;
    FreeMem(mp, mp^.Size + LONGCARD(TSIZE(MemBlock)));
    mp := xp;
  END;
  Free := NIL;
END FreeHeap;

END Heap.

