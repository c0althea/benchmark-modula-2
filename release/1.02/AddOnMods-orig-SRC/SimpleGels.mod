(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                      (c) 1986, 1987 by Leon Frenkel                      *
 ****************************************************************************
 * Name: SimpleGels.MOD                 Version: Amiga.00.00                *
 * Created: 04/25/87   Updated: 04/25/87   Author: Leon Frenkel             *
 * Description: Functions for simplified handling of Graphic Elements.      *
 ****************************************************************************)

IMPLEMENTATION MODULE SimpleGels;

FROM SYSTEM IMPORT
	ADDRESS, BYTE,
	TSIZE, ADR;
FROM Gels IMPORT
	VSpriteFlags, VSpriteFlagsSet,
	BobFlags, BobFlagsSet,
	collTable,
	GelsInfo, GelsInfoPtr,
	Bob, BobPtr,
	VSprite, VSpritePtr,
	DBufPacket, DBufPacketPtr,
	InitGels, InitMasks;
FROM Memory IMPORT
	AllocMem, FreeMem,
	MemReqSet, MemPublic, MemChip, MemClear;
FROM Rasters IMPORT
	RastPort,
	AllocRaster, FreeRaster;

(*$L+*)

PROCEDURE CreateGelsInfo(VAR rp: RastPort): GelsInfoPtr;
VAR
  gi : GelsInfoPtr;
  vh : VSpritePtr;
  vt : VSpritePtr;
BEGIN
  gi := AllocMem(TSIZE(GelsInfo) +
                 TSIZE(VSprite) +
                 TSIZE(VSprite) +
                 (TSIZE(CARDINAL) * 8) +
                 (TSIZE(LONGCARD) * 8) +
                 TSIZE(collTable), MemReqSet{MemPublic, MemClear});
  IF (gi # NIL) THEN
    vh := ADDRESS(gi) + LONGCARD(TSIZE(GelsInfo));
    vt := ADDRESS(vh) + LONGCARD(TSIZE(VSprite));
    WITH gi^ DO
      sprRsrvd    := ReservedSprites;
      nextLine    := ADDRESS(vt) + LONGCARD(TSIZE(VSprite));
      lastColor   := ADDRESS(nextLine) + LONGCARD(TSIZE(CARDINAL) * 8);
      collHandler := ADDRESS(lastColor) + LONGCARD(TSIZE(LONGCARD) * 8);
      (* leftmost := 0; already initialized *)
      rightmost   := (rp.BitMap^.BytesPerRow * 8) - 1;
      (* topmost  := 0; already initialized *)
      bottommost  := (rp.BitMap^.Rows) - 1;
    END;
    rp.GelsInfo := gi;
    InitGels(vh^, vt^, gi^);
  END;
  RETURN (gi);
END CreateGelsInfo;

PROCEDURE CreateBob(x, y: INTEGER;  width, height, depth: CARDINAL;
                    image: ADDRESS;  planePick, planeOnOff: BYTE): BobPtr;
VAR
  vs          : VSpritePtr;
  bob         : BobPtr;
  dbPack      : DBufPacketPtr;
  border      : ADDRESS;
  saveBuf     : ADDRESS;
  saveBuf2    : ADDRESS;
  mask        : ADDRESS;
  HeightDepth : CARDINAL;
  MemOk       : BOOLEAN;
  iMask       : BOOLEAN;
BEGIN
  HeightDepth := height * depth;

  iMask := FALSE;
  MemOk := TRUE;
  saveBuf := NIL;
  saveBuf2 := NIL;
  mask := NIL;
  vs := AllocMem(TSIZE(VSprite) +
                 TSIZE(Bob) + 
                 TSIZE(DBufPacket) + 
                 INTEGER(((width + 15) DIV 16) * 2), (* BorderLine buffer *)
                 MemReqSet{MemPublic, MemClear});

  IF (SaveBack IN DefVSpriteFlags) THEN
    saveBuf := AllocRaster(width, HeightDepth);
    IF saveBuf = NIL THEN
      MemOk := FALSE;
    END;
    IF (DoubleBuffer) THEN
      saveBuf2 := AllocRaster(width, HeightDepth);
      IF saveBuf2 = NIL THEN
        MemOk := FALSE;
      END;
    END;
  END;

  IF (CollisionDetection) OR (Overlay IN DefVSpriteFlags) THEN
    mask := AllocRaster(width, height);
    IF mask = NIL THEN
      MemOk := FALSE;
    END;
    iMask := TRUE;
  END;

  IF (vs # NIL) AND (MemOk) THEN
    bob    := ADDRESS(vs) + LONGCARD(TSIZE(VSprite));
    dbPack := ADDRESS(bob) + LONGCARD(TSIZE(Bob));
    border := ADDRESS(dbPack) + LONGCARD(TSIZE(DBufPacket));

    WITH vs^ DO
      Flags      := DefVSpriteFlags;
      Y          := y;
      X          := x;
      Height     := height;
      Width      := (width + 15) DIV 16;
      Depth      := depth;
   (* MeMask     := {}; *)
   (* HitMask    := {}; *)
      ImageData  := image;
      BorderLine := border;
      CollMask   := mask;
      VSBob      := bob;
      PlanePick  := planePick;
      PlaneOnOff := planeOnOff;
    END;
    WITH bob^ DO
      Flags       := DefBobFlags;
      SaveBuffer  := saveBuf;
      ImageShadow := mask;
   (* Before      := NIL; *)
   (* After       := NIL; *)
      BobVSprite  := vs;
   (* BobComp     := NIL; *)

      IF (DoubleBuffer) THEN
        DBuffer := dbPack;
        dbPack^.BufBuffer := saveBuf2;
      END;
    END; (* WITH *)
    IF (iMask) THEN (* Initialize boundary, collision and/or shadow mask *)
      InitMasks(vs^);
    END;
  ELSE (* Not enough memory *)
    IF (vs # NIL) THEN FreeMem(vs, TSIZE(VSprite) +
                                   TSIZE(Bob) + 
                                   TSIZE(DBufPacket) + 
                                   INTEGER(((width + 15) DIV 16) * 2)); END;
    IF (saveBuf # NIL)  THEN FreeRaster(saveBuf, width, HeightDepth); END;
    IF (saveBuf2 # NIL) THEN FreeRaster(saveBuf2, width, HeightDepth); END;
    IF (mask # NIL) THEN FreeRaster(mask, width, height); END;
    bob := NIL;
  END;
  RETURN (bob);
END CreateBob;

PROCEDURE CreateVSprite(x, y:INTEGER;  height: CARDINAL;
                        image: ADDRESS;  sprColorTable: ADDRESS): VSpritePtr;
VAR
  vs   : VSpritePtr;
  mask : ADDRESS;
BEGIN
  (* + 2 - BorderLine buffer *)
  vs := AllocMem(TSIZE(VSprite) + 2, MemReqSet{MemPublic, MemClear});
  IF (vs = NIL) THEN
    RETURN (NIL);  (* Error *)
  END;

  mask := NIL;
  IF (CollisionDetection) THEN
    mask := AllocRaster(16, height);
    IF (mask = NIL) THEN
      FreeMem(vs, TSIZE(VSprite) + 2);
      RETURN (NIL);  (* Error *)
    END;
  END;

  WITH vs^ DO
    Flags      := VSpriteFlagsSet{Vsprite};
    Y          := y;
    X          := x;
    Height     := height;
    Width      := 1;
    Depth      := 2;
 (* MeMask     := {}; *)
 (* HitMask    := {}; *)
    ImageData  := image;
    BorderLine := ADDRESS(vs) + LONGCARD(TSIZE(VSprite));
    CollMask   := mask;
    SprColors  := sprColorTable;
  END;
  IF (CollisionDetection) THEN
    InitMasks(vs^);
  END;

  RETURN (vs);
END CreateVSprite;                        

PROCEDURE DeleteBob(VAR bob: Bob);
VAR
  vs : VSpritePtr;
  HeightDepth : CARDINAL;
  Width : CARDINAL;
BEGIN
  vs := bob.BobVSprite;
  HeightDepth := vs^.Height * vs^.Depth;
  Width := vs^.Width * 16;
  IF (bob.SaveBuffer # NIL) THEN
    FreeRaster(bob.SaveBuffer, Width, HeightDepth);
  END;
  IF (vs^.CollMask # NIL) THEN 
    FreeRaster(vs^.CollMask, Width, vs^.Height);
  END;
  IF (bob.DBuffer # NIL) AND (bob.DBuffer^.BufBuffer # NIL) THEN
    FreeRaster(bob.DBuffer^.BufBuffer, Width, HeightDepth);
  END;
  FreeMem(vs, TSIZE(VSprite) +
              TSIZE(Bob) + 
              TSIZE(DBufPacket) + 
              INTEGER(((Width + 15) DIV 16) * 2));
END DeleteBob;

PROCEDURE DeleteVSprite(VAR vs: VSprite);
BEGIN
  IF (vs.CollMask # NIL) THEN FreeRaster(vs.CollMask, 16, vs.Height); END;
  FreeMem(ADR(vs), TSIZE(VSprite) + 2);
END DeleteVSprite;

PROCEDURE DeleteGelsInfo(VAR gi: GelsInfo);
BEGIN
  FreeMem(ADR(gi), 
          TSIZE(GelsInfo) +
          TSIZE(VSprite) +
          TSIZE(VSprite) +
          (TSIZE(CARDINAL) * 8) +
          (TSIZE(LONGCARD) * 8) +
          TSIZE(collTable));
END DeleteGelsInfo;

BEGIN
  DoubleBuffer       := TRUE;
  CollisionDetection := TRUE;
  DefVSpriteFlags    := VSpriteFlagsSet{SaveBack, Overlay};
  DefBobFlags        := BobFlagsSet{};
  ReservedSprites    := BYTE(0FFH);
END SimpleGels.
