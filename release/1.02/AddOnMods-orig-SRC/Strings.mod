(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: Strings.MOD                       Version: Amiga.00.00             *
 * Created: 12/04/86   Updated: 08/07/87   Author: Leon Frenkel             *
 * Description: Functions for handling null-terminated strings, defined     *
 *  as arrays of chars. This implementation can handle strings of up to     *
 *  32767 chars.                                                            *
 ****************************************************************************)

IMPLEMENTATION MODULE Strings;

(* NOTE: CompareStringCAP() has $D switch enabled because it may converts
 *       its original arguments to upper case. All other procedures have the
 *       $D switch disabled for efficency. *)

(*$L+*)
(*$D-*)

PROCEDURE StringLength(string: ARRAY OF CHAR): CARDINAL;
VAR len: CARDINAL;
BEGIN
  len := 0;
  WHILE (string[len] # 0C) DO
    INC(len);
  END;
  RETURN len;
END StringLength;


PROCEDURE ConvStringToUpperCase(VAR string: ARRAY OF CHAR);
VAR x: CARDINAL;
BEGIN
  x := 0;
  WHILE (string[x] # 0C) DO
    string[x] := CAP(string[x]);
    INC(x);
  END;
END ConvStringToUpperCase;

PROCEDURE CompareString(string1, string2: ARRAY OF CHAR): Relation;
VAR x : INTEGER;

BEGIN
  x := 0;
  LOOP
    IF (string1[x] = string2[x]) THEN
      IF (string1[x] = 0C) THEN RETURN equal; END;
      INC(x);
    ELSE
      EXIT;
    END;
  END;

  IF string1[x] > string2[x] THEN
    RETURN greater;
  ELSE
    RETURN less;
  END;
END CompareString;

(*$D+*)
PROCEDURE CompareStringCAP(string1, string2: ARRAY OF CHAR): Relation;
BEGIN
  ConvStringToUpperCase(string1);
  ConvStringToUpperCase(string2);
  RETURN CompareString(string1, string2);
END CompareStringCAP;
(*$D-*)

PROCEDURE CopyString(VAR toString: ARRAY OF CHAR; fromString: ARRAY OF CHAR);
VAR x : CARDINAL;
BEGIN
  x := 0;
  WHILE (fromString[x] # 0C) AND (x < HIGH(toString)) DO
    toString[x] := fromString[x];
    INC(x);    
  END;
  toString[x] := 0C;
END CopyString;


PROCEDURE ConcatString(VAR toString: ARRAY OF CHAR; fromString: ARRAY OF CHAR);
VAR i, j : CARDINAL;
BEGIN
  i := 0;
  j := StringLength(toString);
  LOOP
    WHILE (fromString[i] # 0C) DO
      IF HIGH(toString) <= j THEN EXIT; END;
      toString[j] := fromString[i];
      INC(j); INC(i);
    END;
    EXIT;
  END;
  toString[j] := 0C; 
END ConcatString;


PROCEDURE InsertSubString(VAR baseString: ARRAY OF CHAR;
                          subString: ARRAY OF CHAR; start: CARDINAL);
VAR subStringLen, baseStringLen : CARDINAL;
    baseStringHigh, baseStringShift : CARDINAL;
    srcShift, dstShift, i, j : CARDINAL;
BEGIN 
  subStringLen    := StringLength(subString);  
  baseStringLen   := StringLength(baseString);
  baseStringHigh  := HIGH(baseString);
  baseStringShift := baseStringLen - start + 1;
  srcShift        := baseStringLen;
  dstShift        := srcShift + subStringLen;

  IF start + subStringLen >= baseStringHigh THEN
    subStringLen := baseStringHigh - start;
    baseString[baseStringHigh] := 0C; (* set new null-term for baseString *)
    baseStringShift := 0; (* dont shift any chars *)
  ELSIF (start + subStringLen) + baseStringShift >= baseStringHigh THEN
    baseString[baseStringHigh] := 0C; (* set new null-term for baseString *)
    baseStringShift := baseStringHigh - (start + subStringLen);
    srcShift := start + baseStringShift - 1;
    dstShift := baseStringHigh - 1;
  END;

  (* shift chars to right to allow room for subString, shift from end of 
   * string towards beginning of string.
   *)
  j := srcShift;
  i := dstShift;
  WHILE (baseStringShift > 0) DO
    baseString[i] := baseString[j];
    DEC(i); DEC(j); DEC(baseStringShift);
  END;

  (* copy the subString into the baseString, subStringLen has been adjusted
   * to fit into the baseString allowing for terminator char.
   *)
  i := start;
  j := 0;
  WHILE (subStringLen > 0) DO
    baseString[i] := subString[j];
    INC(i); INC(j); DEC(subStringLen);
  END;

END InsertSubString;


PROCEDURE OverwriteWithSubString(VAR baseString: ARRAY OF CHAR;
                                 subString: ARRAY OF CHAR; start: CARDINAL);
VAR i, j : CARDINAL;
BEGIN
  i := 0;
  j := start;
    WHILE (subString[i] # 0C) DO
      IF HIGH(baseString) = j THEN baseString[j] := 0C; RETURN; END;
      baseString[j] := subString[i];
      INC(j); INC(i);
    END;
END OverwriteWithSubString;


PROCEDURE DeleteSubString(VAR baseString: ARRAY OF CHAR;
                          fromPosition: CARDINAL; length: CARDINAL);
VAR i, j : INTEGER;
BEGIN
    i := fromPosition + length;
    j := fromPosition;
    WHILE (baseString[i] # 0C) DO
      baseString[j] := baseString[i];
      INC(i); INC(j);
    END;
    baseString[j] := 0C; (* null terminate string *)
END DeleteSubString;


PROCEDURE ExtractSubString(VAR subString: ARRAY OF CHAR;
                           baseString: ARRAY OF CHAR; 
                           fromPosition, length: CARDINAL);
VAR i, j : CARDINAL;
BEGIN
  i := fromPosition;
  j := 0;
  WHILE (length > 0) AND (baseString[i] # 0C) AND (j < HIGH(subString)) DO
    subString[j] := baseString[i];
    INC(i); INC(j); DEC(length);
  END;
  subString[j] := 0C;

END ExtractSubString;


PROCEDURE LocateSubString(baseString, subString: ARRAY OF CHAR;
                          start, end: CARDINAL): INTEGER;
VAR p : CARDINAL;
    i, j : INTEGER;
BEGIN
  p := start;
  WHILE (p <= end) DO
    i := 0;
    j := p;
    LOOP
      IF (subString[i] = 0C) THEN RETURN p; END;
      IF (baseString[j] = 0C) THEN RETURN -1; END;
      IF (baseString[j] # subString[i]) THEN EXIT; END;
      INC(i); INC(j);
    END;
    INC(p);
  END;
  RETURN -1;
END LocateSubString;


PROCEDURE LocateChar(string: ARRAY OF CHAR; c: CHAR; 
                     start, end: CARDINAL): INTEGER;
VAR p : CARDINAL;
BEGIN
  p := start;
  WHILE (p <= end) AND (string[p] # 0C) DO
    IF (string[p] = c) THEN RETURN p; END;
    INC(p);
  END;
  RETURN -1;
END LocateChar;

END Strings.
