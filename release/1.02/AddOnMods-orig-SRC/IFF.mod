(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: IFF.DEF                           Version: Amiga.00.00             *
 * Created: 03/28/87   Updated: 05/08/87   Author: Leon Frenkel             *
 * Description: Defs for IFF-85 Interchange Format Files.                   *
 ****************************************************************************)

IMPLEMENTATION MODULE IFF;

FROM SYSTEM IMPORT TSIZE;

(*$L+*)

PROCEDURE ChunkMoreBytes(VAR gc: GroupContext): LONGINT;
BEGIN
  WITH gc DO
    RETURN (ckHdr.ckSize - bytesSoFar);
  END;
END ChunkMoreBytes;

PROCEDURE WordAlign(size: LONGINT): LONGINT;
TYPE
  LONGSET = SET OF [0..31];
BEGIN
  RETURN LONGINT( LONGSET(size + 1D) * LONGSET{1..31} );
END WordAlign;

PROCEDURE ChunkPSize(datSize: LONGINT): LONGINT;
BEGIN
  RETURN ( WordAlign(datSize) + LONGINT(TSIZE(ChunkHeader)) );
END ChunkPSize;

END IFF.
