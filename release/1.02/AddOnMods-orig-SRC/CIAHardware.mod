(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CIAHardware.MOD                   Version: Amiga.00.00             *
 * Created: 11/27/86   Updated: 11/27/86   Author: Leon Frenkel             *
 * Description: Amiga Complex Interface Adapter definitions.                *
 ****************************************************************************)

IMPLEMENTATION MODULE CIAHardware;

(*$L+*)

BEGIN
  (* initialize to point to hardware registers *)
  ciaa := CIAPtr(00BFE001H);
  ciab := CIAPtr(00BFD000H);
END CIAHardware.
