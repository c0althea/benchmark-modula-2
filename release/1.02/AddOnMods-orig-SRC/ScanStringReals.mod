(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: ScanStringReals.DEF               Version: Amiga.00.00             *
 * Created: 02/24/87   Updated: 02/24/87   Author: Leon Frenkel             *
 * Description: By importing this module the Scan() function in module      *
 *  ScanString will suport conversion of the REAL type using %e and %f.     *
 ****************************************************************************)

IMPLEMENTATION MODULE ScanStringReals;

FROM ScanString IMPORT ConvToRealFunc;
FROM RealConversions IMPORT ConvStringToReal;

(*$L+*)

BEGIN
  ConvToRealFunc := ConvStringToReal;
END ScanStringReals.
