(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                      (c) 1986, 1987 by Leon Frenkel                      *
 ****************************************************************************
 * Name: SimpleIDCMP.MOD                   Version: Amiga.00.00             *
 * Created: 04/18/87   Updated: 07/23/87   Author: Leon Frenkel             *
 * Description: Simplified Intuition Message Handler.                       *
 ****************************************************************************)

IMPLEMENTATION MODULE SimpleIDCMP;

FROM SYSTEM IMPORT
	ADR, ADDRESS, BYTE;
FROM InputEvents IMPORT
	IEQualifierSet;
FROM Intuition IMPORT
	WindowPtr, Gadget, GadgetPtr, IntuiMessagePtr, 
	IDCMPFlags, IDCMPFlagsSet;
FROM Ports IMPORT ReplyMsg;

(*$L+*)

PROCEDURE ProcIMsg(VAR wp: WindowProc; msg: IntuiMessagePtr);
VAR
  md   : MsgData;
  iAdr : ADDRESS;
  code : CARDINAL;
  class: IDCMPFlagsSet;
  win  : WindowPtr;
  gadp : GadgetPtr;
BEGIN

  (* Copy data from IntuiMessage into local variables *)
  WITH msg^ DO
    md.MouseX    := MouseX;
    md.MouseY    := MouseY;
    md.Seconds   := Seconds;
    md.Micros    := Micros;
    md.Qualifier := Qualifier;

    class      := Class;
    code       := Code;
    iAdr       := IAddress;
    win        := IDCMPWindow;
  END;
  IF NOT (class <= IDCMPFlagsSet{SizeVerify, MenuVerify, ReqVerify}) THEN
    ReplyMsg(msg);
  END;

  IF (SizeVerify IN class) THEN
    wp.procSizeVerify(win^, md);
  ELSIF (NewSize IN class) THEN
    wp.procNewSize(win^, md);
  ELSIF (RefreshWindow IN class) THEN
    wp.procRefreshWindow(win^, md);
  ELSIF (MouseButtons IN class) THEN
    wp.procMouseButtons(win^, md, code);
  ELSIF (MouseMove IN class) THEN
    wp.procMouseMove(win^, md);
  ELSIF (GadgetDown IN class) THEN
    gadp := iAdr;
    wp.procGadgetDown(win^, md, gadp^);
  ELSIF (GadgetUp IN class) THEN
    gadp := iAdr;
    wp.procGadgetUp(win^, md, gadp^);
  ELSIF (ReqSet IN class) THEN
    wp.procReqSet(win^, md);
  ELSIF (MenuPick IN class) THEN
    wp.procMenuPick(win^, md, code);
  ELSIF (Closewindow IN class) THEN
    wp.procCloseWindow(win^, md);
  ELSIF (RawKey IN class) THEN
    wp.procRawKey(win^, md, code);
  ELSIF (ReqVerify IN class) THEN
    wp.procReqVerify(win^, md);
  ELSIF (ReqClear IN class) THEN
    wp.procReqClear(win^, md);
  ELSIF (MenuVerify IN class) THEN
    wp.procMenuVerify(win^, md);
  ELSIF (NewPrefs IN class) THEN
    wp.procNewPrefs(win^, md);
  ELSIF (DiskInserted IN class) THEN
    wp.procDiskInserted(win^, md);
  ELSIF (DiskRemoved IN class) THEN
    wp.procDiskRemoved(win^, md);
  ELSIF (WBenchMessage IN class) THEN
    wp.procWBenchMessage(win^, md, code);
  ELSIF (ActiveWindow IN class) THEN
    wp.procActiveWindow(win^, md);
  ELSIF (InactiveWindow IN class) THEN
    wp.procInactiveWindow(win^, md);
  ELSIF (VanillaKey IN class) THEN
    wp.procVanillaKey(win^, md, code);
  ELSIF (IntuiTicks IN class) THEN
    wp.procIntuiTicks(win^, md);
  END;

  IF (class <= IDCMPFlagsSet{SizeVerify, MenuVerify, ReqVerify}) THEN
    ReplyMsg(msg);
  END;

END ProcIMsg;

END SimpleIDCMP.
