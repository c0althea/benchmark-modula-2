(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CPrintFile.MOD                    Version: Amiga.00.00             *
 * Created: 01/18/87   Updated: 02/24/87   Author: Leon Frenkel             *
 * Description: From the "C" language standard library functions for        *
 *  doing formated output to a buffered file.                               *
 ****************************************************************************)

IMPLEMENTATION MODULE CPrintFile;

FROM SYSTEM IMPORT ADR, ADDRESS;
FROM CStdIO IMPORT FILE, putc;
FROM FormatString IMPORT FormatArg, Format;

(*$L+,D-*)

VAR fp : FILE;

PROCEDURE StoreCh(c: CHAR);
VAR
  r : INTEGER;
BEGIN
  r := putc(c, fp);
END StoreCh;

PROCEDURE fprintf(f: FILE; formatStr: ADDRESS; VAR args: ARRAY OF FormatArg);
VAR
  l : INTEGER;
BEGIN
  fp := f;
  l := Format(formatStr, args, StoreCh);
END fprintf;

END CPrintFile.
