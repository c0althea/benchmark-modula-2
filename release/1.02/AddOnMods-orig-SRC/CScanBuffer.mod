(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CScanBuffer.MOD                   Version: Amiga.00.00             *
 * Created: 01/19/87   Updated: 02/24/87   Author: Leon Frenkel             *
 * Description: From the "C" language standard library functions for        *
 *  doing formated input from a buffer.                                     *
 ****************************************************************************)

IMPLEMENTATION MODULE CScanBuffer;

FROM SYSTEM     IMPORT ADDRESS;
FROM ScanString IMPORT Scan, ScanArg;

(*$L-,D-*)

CONST
  EOF = -1;

VAR cp  : POINTER TO CHAR;
    eos : BOOLEAN; (* end of string flag *)

PROCEDURE GetCh(flag: BOOLEAN): INTEGER;
VAR
  c : CHAR;
BEGIN
  IF NOT flag THEN
    c := cp^;
    IF c # 0C THEN
      INC(ADDRESS(cp));
      RETURN (INTEGER(c));
    END;
    eos := TRUE;
  ELSE
    IF NOT eos THEN
      DEC(ADDRESS(cp));
      RETURN (INTEGER(cp^));
    END;
  END;
  RETURN (EOF); (* end of string *)
END GetCh;

PROCEDURE sscanf(srcStr: ADDRESS; formatStr: ADDRESS;
                 VAR args: ARRAY OF ScanArg): INTEGER;
BEGIN
  cp  := srcStr;
  eos := FALSE;
  RETURN Scan(formatStr, args, GetCh);
END sscanf;

END CScanBuffer.
