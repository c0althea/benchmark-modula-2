(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: RemAlloc.MOD                      Version: Amiga.00.00             *
 * Created: 04/22/87   Updated: 05/08/87   Author: Leon Frenkel             *
 * Description: Allocator which remembers the size allocated.               *
 ****************************************************************************)

IMPLEMENTATION MODULE RemAlloc;

FROM SYSTEM IMPORT ADDRESS;
FROM Memory IMPORT MemReqSet, MemClear, MemChip, MemPublic,
		   AllocMem, FreeMem;

(*$L+*)


PROCEDURE RemAlloc(size: LONGCARD; flags: MemReqSet): ADDRESS;
VAR
  p : POINTER TO LONGCARD;
BEGIN
  INC(size, 4);
  p := AllocMem(size, flags);
  IF p # NIL THEN
    p^ := size;
    INC(ADDRESS(p), 4D);
  END;
  RETURN (p);
END RemAlloc;

PROCEDURE ChipAlloc(size: LONGCARD): ADDRESS;
BEGIN
  RETURN RemAlloc(size, MemReqSet{MemClear, MemPublic, MemChip});
END ChipAlloc;

PROCEDURE ChipNoClearAlloc(size: LONGCARD): ADDRESS;
BEGIN
  RETURN RemAlloc(size, MemReqSet{MemPublic, MemChip});
END ChipNoClearAlloc;

PROCEDURE ExtAlloc(size: LONGCARD): ADDRESS;
BEGIN
  RETURN RemAlloc(size, MemReqSet{MemClear, MemPublic});
END ExtAlloc;

PROCEDURE ExtNoClearAlloc(size: LONGCARD): ADDRESS;
BEGIN
  RETURN RemAlloc(size, MemReqSet{MemPublic});
END ExtNoClearAlloc;

PROCEDURE RemFree(p: ADDRESS);
VAR
  l : POINTER TO LONGCARD;
BEGIN
  IF p # NIL THEN
    l := p - 4D;
    FreeMem(l, l^);
  END;
END RemFree;

END RemAlloc.
