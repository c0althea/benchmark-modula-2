(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CScanTerminal.MOD                 Version: Amiga.00.00             *
 * Created: 01/19/87   Updated: 01/20/87   Author: Leon Frenkel             *
 * Description: From the "C" language standard library functions for        *
 *  doing formated input through the Module "Terminal".                     *
 ****************************************************************************)

IMPLEMENTATION MODULE CScanTerminal;

FROM SYSTEM     IMPORT ADR;
FROM ScanString IMPORT Scan, ScanArg;
FROM Terminal   IMPORT Read, ReadAgain; IMPORT Terminal;

(*$L+,D-*)

CONST
  EOF = -1;

VAR
  eof : BOOLEAN;

PROCEDURE GetCh(flag: BOOLEAN): INTEGER;
VAR c: CHAR;
BEGIN
  IF NOT flag THEN
    Read(c);
    IF NOT Terminal.eof THEN
      RETURN INTEGER(c);
    END;
    eof := TRUE;
  ELSE
    IF NOT eof THEN
      ReadAgain;
      RETURN (0); (* Dummy value *)
    END;
  END;
  RETURN (EOF);
END GetCh;

PROCEDURE scanf(formatStr: ARRAY OF CHAR; VAR args: ARRAY OF ScanArg): INTEGER;
BEGIN
  eof := FALSE;
  RETURN Scan(ADR(formatStr), args, GetCh);
END scanf;

END CScanTerminal.
