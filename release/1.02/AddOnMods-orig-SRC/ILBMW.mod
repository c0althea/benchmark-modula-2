(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: ILBMW.MOD                         Version: Amiga.00.00             *
 * Created: 04/20/87   Updated: 12/25/87   Author: Leon Frenkel             *
 * Description: Functions for writing InterLeaved BitMap raster image.      *
 ****************************************************************************)

IMPLEMENTATION MODULE ILBMW;

FROM SYSTEM   IMPORT ADR, ADDRESS, BYTE, TSIZE, SHIFT;
FROM Graphics IMPORT BitMap;
FROM IFF      IMPORT IFFP, GroupContext, ClientError, IFFOkay, szNotYetKnown;
FROM IFFW     IMPORT PutCk, PutCkHdr, PutCkEnd, IFFWriteBytes;
FROM ILBM     IMPORT BitMapHeader, Point2D, DestMerge, SpritePrecedence,
		     ColorRegister, sizeofColorRegister, MaxAmDepth,
		     Masking, Compression, cmpByteRun1, cmpNone,
		     IDBMHD, IDGRAB, IDDEST, IDSPRT, IDCMAP, IDBODY,
		     x320x200Aspect, y320x200Aspect, x320x400Aspect,
		     y320x400Aspect, x640x200Aspect, y640x200Aspect,
		     x640x400Aspect, y640x400Aspect,
		     RowBytes;
FROM Packer   IMPORT MaxPackedSize, PackRow;  

(*$L+*)


PROCEDURE PutBMHD(VAR con: GroupContext; VAR bmHdr: BitMapHeader): IFFP;
BEGIN
  RETURN PutCk(con, IDBMHD, LONGINT(TSIZE(BitMapHeader)), ADR(bmHdr));
END PutBMHD;

PROCEDURE PutGRAB(VAR con: GroupContext; VAR point2D: Point2D): IFFP;
BEGIN
  RETURN PutCk(con, IDGRAB, LONGINT(TSIZE(Point2D)), ADR(point2D));
END PutGRAB;

PROCEDURE PutDEST(VAR con: GroupContext; VAR destMerge: DestMerge): IFFP;
BEGIN
  RETURN PutCk(con, IDDEST, LONGINT(TSIZE(DestMerge)), ADR(destMerge));
END PutDEST;

PROCEDURE PutSPRT(VAR con: GroupContext; VAR sprPrec: SpritePrecedence): IFFP;
BEGIN
  RETURN PutCk(con, IDSPRT, LONGINT(TSIZE(SpritePrecedence)), ADR(sprPrec));
END PutSPRT;

PROCEDURE InitBMHdr(VAR bmHdr: BitMapHeader;  VAR bitmap: BitMap;
                    mask: Masking;  compress: Compression;
                    transColor: CARDINAL; 
                    PageWidth, PageHeight: CARDINAL): IFFP;
VAR
  rowBytes : CARDINAL;
BEGIN
  rowBytes := bitmap.BytesPerRow;

  WITH bmHdr DO
    w                := rowBytes * 8;
    h                := bitmap.Rows;
    x                := 0;             (* Default position is (0,0) *)
    y                := 0;
    nPlanes          := bitmap.Depth;
    masking          := mask;
    compression      := compress;
    pad1             := BYTE(0);
    transparentColor := transColor;
    xAspect          := BYTE(1);
    yAspect          := BYTE(1);
    pageWidth        := PageWidth;
    pageHeight       := PageHeight;

    IF PageWidth = 320 THEN
      IF PageHeight = 200 THEN
        xAspect := x320x200Aspect;
        yAspect := y320x200Aspect;
      ELSIF PageHeight = 400 THEN
        xAspect := x320x400Aspect;
        yAspect := y320x400Aspect;
      END;
    ELSIF PageWidth = 640 THEN
      IF PageHeight = 200 THEN
        xAspect := x640x200Aspect;
        yAspect := y640x200Aspect;
      ELSIF PageHeight = 400 THEN
        xAspect := x640x400Aspect;
        yAspect := y640x400Aspect;
      END;
    END;
  END; (* WITH *)

  IF ODD(rowBytes) THEN
    RETURN (ClientError);
  ELSE
    RETURN (IFFOkay);
  END;
END InitBMHdr;

PROCEDURE PutCMAP(VAR context: GroupContext; colorMap: ADDRESS;
                  depth: BYTE): IFFP;
VAR
  nColorRegs : BYTE;
  iffp : IFFP;
  colorReg : ColorRegister;
  cMap : POINTER TO CARDINAL;
BEGIN
  cMap := colorMap;

  IF CARDINAL(depth) > MaxAmDepth THEN
   depth := BYTE(MaxAmDepth);
  END;
  nColorRegs := BYTE(SHIFT(1, INTEGER(depth)));
  
  iffp := PutCkHdr(context, IDCMAP, INTEGER(nColorRegs) * sizeofColorRegister);
  IF iffp # IFFOkay THEN (* CheckIFFP *)
    RETURN (iffp);
  END;

  WITH colorReg DO
    WHILE CARDINAL(nColorRegs) # 0 DO
      red   := BYTE( BITSET(cMap^ DIV 16) * {4..7} );
      green := BYTE( BITSET(cMap^       ) * {4..7} );
      blue  := BYTE( BITSET(cMap^  *  16) * {4..7} );
      iffp := IFFWriteBytes(context, ADR(colorReg), sizeofColorRegister);
      IF iffp # IFFOkay THEN
        RETURN (iffp);
      END;

      INC(ADDRESS(cMap), TSIZE(CARDINAL));

      DEC(CHAR(nColorRegs));
    END; (* WHILE *)
  END; (* WITH *)

  iffp := PutCkEnd(context);
  RETURN (iffp);
END PutCMAP;

(* NOTE: This implementation could be a LOT faster if it used more of the
 * supplied buffer. It would make far fewer calls to IFFWriteBytes (and
 * therefore to DOS Write). *)
PROCEDURE PutBODY(VAR context: GroupContext;  VAR bitmap: BitMap;
                  mask: ADDRESS;  VAR bmHdr: BitMapHeader;
                  buffer: ADDRESS;  bufsize: LONGINT): IFFP;
VAR
  iffp : IFFP;
  rowBytes : LONGINT;
  dstDepth : INTEGER;
  compression : Compression;
  planeCnt : INTEGER;     (* number of bit planes including mask *)
  iPlane, iRow : INTEGER;
  packedRowBytes : LONGINT;
  buf : ADDRESS;
  planes : ARRAY [0..MaxAmDepth] OF ADDRESS; (* array of ptrs to planes & mask*)
  temp : INTEGER;
BEGIN
  rowBytes := bitmap.BytesPerRow;
  dstDepth := INTEGER(bmHdr.nPlanes);
  compression := bmHdr.compression;

  IF (bufsize < LONGINT(MaxPackedSize(rowBytes))) OR (* Must buffer a comprsd row *)
     (CARDINAL(compression) > CARDINAL(cmpByteRun1)) OR (* bad arg *)
     (bitmap.Rows # bmHdr.h) OR			(* inconsistent *)
     (rowBytes # LONGINT(RowBytes(bmHdr.w))) OR (* inconsistent *)
     (INTEGER(bitmap.Depth) < dstDepth) OR	(* inconsistent *)
     (dstDepth > MaxAmDepth) THEN		(* too many for this routine *)
    RETURN (ClientError);
  END;

  IF mask = NIL THEN
    temp := 0;
  ELSE
    temp := 1;
  END;

  planeCnt := dstDepth + temp;

  (* Copy the ptrs to bit & mask planes into local array "planes" *)
  iPlane := 0;
  WHILE iPlane < dstDepth DO
    planes[iPlane] := bitmap.Planes[iPlane];

    INC(iPlane);
  END;

  IF mask # NIL THEN
    planes[dstDepth] := mask;
  END;

  iffp := PutCkHdr(context, IDBODY, szNotYetKnown);
  IF iffp # IFFOkay THEN (* CkeckIFFP() *)
    RETURN (iffp);
  END;

  (* Write out the BODY contents *)
  iRow := bmHdr.h;
  WHILE iRow > 0 DO
    iPlane := 0;
    WHILE iPlane < planeCnt DO
      (* Write next row. *)
      IF CARDINAL(compression) = CARDINAL(cmpNone) THEN
        iffp := IFFWriteBytes(context, planes[iPlane], rowBytes);
        INC(planes[iPlane], rowBytes);
      ELSE (* Compress and write next row. *)
        buf := buffer;
        packedRowBytes := PackRow(planes[iPlane], buf, rowBytes);
        iffp := IFFWriteBytes(context, buffer, packedRowBytes);
      END;
      IF iffp # IFFOkay THEN (* CheckIFFP() *)
        RETURN (iffp);
      END;

      INC(iPlane);
    END;

    DEC(iRow);
  END;

  (* Finish the chunk *)
  iffp := PutCkEnd(context);
  RETURN (iffp);
END PutBODY;

END ILBMW.
