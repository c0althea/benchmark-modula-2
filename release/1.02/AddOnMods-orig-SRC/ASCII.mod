(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1987 by Leon Frenkel                         *
 ****************************************************************************
 * Name: ASCII.MOD                         Version: Amiga.00.00             *
 * Created: 07/22/87   Updated: 07/24/87   Author: Leon Frenkel             *
 * Description: ASCII codes and functions.                                  *
 ****************************************************************************)

IMPLEMENTATION MODULE ASCII;

(*$L+*)

PROCEDURE CharIsPrintable(Ch: CHAR): BOOLEAN;
BEGIN
  RETURN ((Ch > US) AND (Ch < DEL));
END CharIsPrintable;

PROCEDURE CharIsControl(Ch: CHAR): BOOLEAN;
BEGIN
  RETURN ((Ch <= US) OR (Ch = DEL));
END CharIsControl;

PROCEDURE CharIsASCII(Ch: CHAR): BOOLEAN;
BEGIN
  RETURN (Ch <= DEL);
END CharIsASCII;

END ASCII.
