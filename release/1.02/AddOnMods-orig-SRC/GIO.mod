(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: GIO.MOD                           Version: Amiga.00.00             *
 * Created: 03/28/87   Updated: 03/28/87   Author: Leon Frenkel             *
 * Description: IFF Generic I/O Speech Up Package.                          *
 ****************************************************************************)

IMPLEMENTATION MODULE GIO;

FROM SYSTEM IMPORT ADDRESS;
FROM AmigaDOS IMPORT FileHandle, Open, Close, Read, Write, Seek, OffsetCurrent;
FROM Memory IMPORT CopyMem;

(*$L+*)

VAR
  (* Auto-Initialized to zero. *)
  wFile      : FileHandle;
  wBuffer    : ADDRESS;
  wNBytes    : LONGINT; (* buffer size in bytes *)
  wIndex     : LONGINT; (* index of next available byte *)
  wWaterline : LONGINT; (* Count of # bytes to be written.
                         * Different than wIndex because of GSeek. *)


PROCEDURE GOpen(filename: ADDRESS; openmode: LONGINT): FileHandle;
BEGIN
  RETURN (Open(filename, openmode));
END GOpen;


PROCEDURE GClose(file: FileHandle): LONGINT;
VAR
  signal, signal2 : LONGINT;
BEGIN
  signal := 0D;
  IF (file = wFile) THEN
    signal := GWriteUndeclare(file);
  END;
  Close(file); (* Call Close even if trouble with write. *)

(* Close() does not return a result as indicated in the gio.c program!
  IF (signal2 < 0D) THEN
    signal := signal2;
  END;
*)

  RETURN (signal);
END GClose;

PROCEDURE GRead(file: FileHandle; buffer: ADDRESS; nBytes: LONGINT): LONGINT;
VAR
  signal : LONGINT;
BEGIN
  (* We don't yet read directly from the buffer, so flush it to disk and
   * let the DOS fetch it back. *)
  signal := 0D;
  IF (file = wFile) THEN
    signal := GWriteFlush(file);
  END;
  IF (signal >= 0D) THEN
    signal := Read(file, buffer, nBytes);
  END;
  RETURN (signal);
END GRead;

PROCEDURE GWriteFlush(file: FileHandle): LONGINT;
VAR
  gWrite : LONGINT;
BEGIN
  gWrite := 0D;
  IF ((wFile # NIL) AND (wBuffer # NIL) AND (wIndex > 0D)) THEN
    gWrite := Write(wFile, wBuffer, wWaterline);
  END;
  wWaterline := 0D; (* No matter what, make sure this happens. *)
  wIndex := 0D;
  RETURN (gWrite);
END GWriteFlush;

PROCEDURE GWriteDeclare(file: FileHandle; buffer: ADDRESS; 
                        nBytes: LONGINT): LONGINT;
VAR
  gWrite : LONGINT;
BEGIN
  gWrite := GWriteFlush(wFile); (* Finish any existing usage. *)
  IF ((file = NIL) OR ((file = wFile) AND (buffer = NIL)) OR (nBytes <= 3D)) THEN
    wFile   := NIL;
    wBuffer := NIL;
    wNBytes := 0D;
  ELSE
    wFile   := file;
    wBuffer := buffer;
    wNBytes := nBytes;
  END; 
  RETURN (gWrite);
END GWriteDeclare;

PROCEDURE GWrite(file: FileHandle; buffer: ADDRESS; nBytes: LONGINT): LONGINT;
VAR
  gWrite : LONGINT;
BEGIN
  gWrite := 0D;
  IF ((file = wFile) AND (wBuffer # NIL)) THEN
    IF (wNBytes >= wIndex + nBytes) THEN
      (* Append to wBuffer. *)
      CopyMem(buffer, wBuffer+ADDRESS(wIndex), nBytes);
      INC(wIndex, nBytes);
      IF (wIndex > wWaterline) THEN
        wWaterline := wIndex;
      END;
      nBytes := 0D; (* Indicate data has been swallowed. *)
    ELSE
      (* We are about to overwrite any data above wIndex, up to at least
       * buffer end. *)
      wWaterline := wIndex;
      gWrite := GWriteFlush(file); (* Write data out in proper order. *)
    END;
  END;
  IF ((nBytes > 0D) AND (gWrite >= 0D)) THEN
    INC(gWrite, Write(file, buffer, nBytes)); 
  END;
  RETURN (gWrite);
END GWrite;

PROCEDURE GSeek(file: FileHandle; position: LONGINT; mode: LONGINT): LONGINT;
VAR
  gSeek, newWIndex : LONGINT;
BEGIN
  gSeek := -2D;
  newWIndex := wIndex + position;
  IF ((file = wFile) AND (wBuffer # NIL)) THEN
    IF ((mode = OffsetCurrent) AND
        (newWIndex >= 0D) AND (newWIndex <= wWaterline)) THEN
      gSeek := wIndex; (* Okay; return *OLD* position *)
      wIndex := newWIndex;
    ELSE
      (* We don't even try to optimize the other cases. *)
      gSeek := GWriteFlush(file);
      IF (gSeek >= 0D) THEN
        gSeek := -2D; (* OK so far *)
      END;
    END;
  END;
  IF (gSeek = -2D) THEN
    gSeek := Seek(file, position, mode);
  END;
  RETURN (gSeek);
END GSeek;

PROCEDURE GWriteUndeclare(file: FileHandle): LONGINT;
BEGIN
  RETURN (GWriteDeclare(file, NIL, 0D));
END GWriteUndeclare;

END GIO.
