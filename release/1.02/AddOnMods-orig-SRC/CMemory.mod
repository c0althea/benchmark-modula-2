(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CMemory.MOD                       Version: Amiga.00.00             *
 * Created: 01/17/87   Updated: 02/08/87   Author: Leon Frenkel             *
 * Description: From the "C" language standard library, functions for       *
 *  simplified dynamic memory management. The advantage of these            *
 *  functions is that they do not require the caller to remember the        *
 *  size of the memory block allocated but only its address.                *
 ****************************************************************************)

IMPLEMENTATION MODULE CMemory;

FROM SYSTEM IMPORT ADDRESS, TSIZE;
FROM Memory IMPORT AllocMem, FreeMem, MemReqSet, MemClear;

(*$L+*)

TYPE 
  (* Header attached to each memory block allocated *)
  MemBlockPtr = POINTER TO MemBlock;
  MemBlock = RECORD
               Size: LONGCARD;
             END;


PROCEDURE calloc(numElements, elementSize: LONGCARD): ADDRESS;
VAR
  size : LONGCARD;
  p    : MemBlockPtr;
BEGIN
  size := numElements * elementSize;
  p := AllocMem(size + LONGCARD(TSIZE(MemBlock)), MemReqSet{MemClear});
  IF p # NIL THEN
    p^.Size := size;
    INC(ADDRESS(p), TSIZE(MemBlock));
  END;
  RETURN(p);
END calloc;


PROCEDURE free(memBlock: ADDRESS);
VAR
  p : MemBlockPtr;
BEGIN
  p := memBlock - LONGCARD(TSIZE(MemBlock));
  FreeMem(p, p^.Size + LONGCARD(TSIZE(MemBlock)));
END free;

PROCEDURE malloc(size: LONGCARD): ADDRESS;
VAR
  p : MemBlockPtr;
BEGIN
  p := AllocMem(size + LONGCARD(TSIZE(MemBlock)), MemReqSet{});
  IF p # NIL THEN
    p^.Size := size;
    INC(ADDRESS(p), TSIZE(MemBlock));
  END;
  RETURN(p);
END malloc;

END CMemory.
