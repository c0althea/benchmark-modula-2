(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: TasksUtil.DEF                     Version: Amiga.00.00             *
 * Created: 01/04/87   Updated: 01/16/87   Author: Leon Frenkel             *
 * Description: Exec Support Procedures for Tasks.                          *
 ****************************************************************************)

IMPLEMENTATION MODULE TasksUtil;

FROM SYSTEM IMPORT ADR, ADDRESS, BYTE, TSIZE;
FROM Lists  IMPORT NewList, AddHead;
FROM Memory IMPORT MemEntry,AllocEntry,MemReqSet,MemPublic,MemClear,MemFailed;
FROM Nodes  IMPORT Node, NTTask;
FROM Tasks  IMPORT Task, TaskPtr, AddTask, RemTask;

(*$L+*)

TYPE
  TaskMemListPtr = POINTER TO TaskMemList;
  TaskMemList = RECORD
                  mlNode       : Node;
                  mlNumEntries : CARDINAL;
                  mlME         : ARRAY [0..1] OF MemEntry;
                END;

VAR TaskMemTemplate : TaskMemList;

PROCEDURE CreateTask(name: ADDRESS; pri: INTEGER; initPC: ADDRESS; 
                     stackSize: LONGCARD): TaskPtr;
TYPE LONGSET = SET OF [0..31];
VAR ml  : TaskMemList;
    tp  : TaskPtr;
    mlp : TaskMemListPtr;
BEGIN
  stackSize := LONGCARD(LONGSET(stackSize + 3D) * LONGSET{2..31});
  ml := TaskMemTemplate;  
  ml.mlME[1].meLength := stackSize;
  mlp := AllocEntry(ADR(ml));
  IF MemFailed IN MemReqSet(mlp) THEN RETURN (NIL); END;

  tp := TaskPtr(mlp^.mlME[0].meAddr);
  WITH tp^ DO
    tcSPLower := mlp^.mlME[1].meAddr;
    tcSPUpper := tcSPLower + ADDRESS(stackSize);
    tcSPReg   := tcSPUpper;
    tcNode.lnType := NTTask;
    tcNode.lnPri  := BYTE(pri);
    tcNode.lnName := name;
    NewList(tcMemEntry);
    AddHead(tcMemEntry, mlp^.mlNode);
    AddTask(tp^, initPC, NIL);
    RETURN (tp);
  END;
END CreateTask;


PROCEDURE DeleteTask(task: TaskPtr);
BEGIN
  RemTask(task);
END DeleteTask;

BEGIN
  WITH TaskMemTemplate DO
    (* Assumes all statics initialized to 0 *)
    mlNumEntries     := 2;
    mlME[0].meReqs   := MemReqSet{MemPublic,MemClear}; 
    mlME[0].meLength := TSIZE(Task);
    mlME[1].meReqs   := MemReqSet{MemClear};
  END;
END TasksUtil.
