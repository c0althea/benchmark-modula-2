(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: CopperUtil.MOD                    Version: Amiga.00.00             *
 * Created: 01/21/87   Updated: 05/07/87   Author: Leon Frenkel             *
 * Description: Graphics Copper Utility Functions. Implimintations of "C"   *
 *  macros.                                                                 *
 ****************************************************************************)

IMPLEMENTATION MODULE CopperUtil;

FROM SYSTEM IMPORT ADDRESS, WORD;
FROM Copper IMPORT UCopList, UCopListPtr, CWait, CBump, CMove, UCopperListInit;

(*$L+*)

PROCEDURE CEND(VAR c: UCopList);
BEGIN
  CWAIT(c, 10000, 255);
END CEND;


PROCEDURE CINIT(VAR c: UCopList; n: CARDINAL): UCopListPtr;
BEGIN
  RETURN UCopperListInit(c, n);
END CINIT;


PROCEDURE CMOVE(VAR c: UCopList; a: ADDRESS; v: WORD);
BEGIN
  CMove(c, a, INTEGER(v));
  CBump(c);
END CMOVE;


PROCEDURE CWAIT(VAR c: UCopList; v, h: INTEGER);
BEGIN
  CWait(c, v, h);
  CBump(c);
END CWAIT;


END CopperUtil.
