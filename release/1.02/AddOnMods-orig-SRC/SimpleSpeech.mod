(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                      (c) 1986, 1987 by Leon Frenkel                      *
 ****************************************************************************
 * Name: SimpleSpeech.MOD                  Version: Amiga.00.00             *
 * Created: 04/29/87   Updated: 12/25/87   Author: Leon Frenkel             *
 * Description: Functions for performing synthesized speech.                *
 ****************************************************************************)

IMPLEMENTATION MODULE SimpleSpeech;

FROM SYSTEM IMPORT
	ADDRESS, BYTE,
	TSIZE, ADR;
FROM AudioDevice IMPORT
	Left0, Right0, Right1, Left1,
	AudioChannelsSet;
FROM IODevices IMPORT
	CmdWrite, CmdRead, CmdStop, CmdStart, CmdReset,
	OpenDevice, CloseDevice,
	SendIO, DoIO, WaitIO;
FROM Libraries IMPORT
	OpenLibrary, CloseLibrary;
FROM Memory IMPORT
	MemReqSet,
	AllocMem, FreeMem;
FROM NarratorDevice IMPORT
	NDNoWrite,
	mouthrb, narratorrb;
FROM Nodes IMPORT
	NTMessage;
FROM Ports IMPORT
	MsgPortPtr,
	GetMsg;
FROM PortsUtil IMPORT
	CreatePort, DeletePort;
FROM Translator IMPORT
	TranslatorName, TranslatorBase,
	Translate;


VAR
  DefChannelsMasks : ARRAY [0..3] OF AudioChannelsSet;
  WritePort        : MsgPortPtr;
  WriteMsg         : narratorrb;
  ReadPort         : MsgPortPtr;
  ReadMsg          : mouthrb;
  PhonemeBuf       : ADDRESS;
  NarOpen          : BOOLEAN;
  SpeechInProgress : BOOLEAN;

(* ====== PRIVATE ====== *)

PROCEDURE Cleanup();
BEGIN
  IF (PhonemeBuf # NIL) THEN FreeMem(PhonemeBuf, PhonemeBufferSize); END;
  IF (TranslatorBase # NIL) THEN CloseLibrary(TranslatorBase^); END;
  IF (NarOpen) THEN CloseDevice(ADR(WriteMsg)); END;
  IF (WritePort # NIL) THEN DeletePort(WritePort^); END;
  IF (ReadPort # NIL) THEN DeletePort(ReadPort^); END;
END Cleanup;

PROCEDURE strlen(str: ADDRESS): LONGCARD;
VAR 
  start : ADDRESS;
BEGIN
  start := str;
  WHILE (CHAR(str^) # 0C) DO
    INC(str);
  END;
  RETURN (str - start);
END strlen;

PROCEDURE FinishPrevSpeech;
VAR
  r : LONGINT;
BEGIN
  IF (SpeechInProgress) THEN
    r := WaitIO(ADR(WriteMsg));
    SpeechInProgress := FALSE;
  END;
END FinishPrevSpeech;

PROCEDURE DoNarratorCmd(cmd: CARDINAL);
VAR
  msg : narratorrb;
  r   : LONGINT;
BEGIN
  msg := WriteMsg;
  msg.message.ioCommand := cmd;
  r := DoIO(ADR(msg));
END DoNarratorCmd;

PROCEDURE CopyOptions();
BEGIN
  WITH WriteMsg DO
    mouths   := OptMouths;
    rate     := OptRate;
    pitch    := OptPitch;
    mode     := OptMode;
    sex      := OptSex;
    volume   := OptVolume;
    sampfreq := OptSamplingFreq;
  END;
END CopyOptions;

(* ====== PUBLIC ====== *)

PROCEDURE CloseSpeech();
BEGIN
  FinishPrevSpeech();
  Cleanup();
END CloseSpeech;

PROCEDURE OpenSpeech(): BOOLEAN;
BEGIN
  TranslatorBase := NIL;
  WritePort      := NIL;
  NarOpen        := FALSE;

  LOOP
    PhonemeBuf := AllocMem(PhonemeBufferSize, MemReqSet{});
    IF (PhonemeBuf = NIL) THEN EXIT; (* err *) END;

    TranslatorBase := OpenLibrary(ADR(TranslatorName), 0D);
    IF (TranslatorBase = NIL) THEN EXIT; (* err *) END;

    WritePort := CreatePort(NIL, 0);
    ReadPort  := CreatePort(NIL, 0);
    IF (WritePort = NIL) OR (ReadPort = NIL) THEN EXIT; (* err *) END;

    WITH WriteMsg DO
      chmasks                         := AudioChannelsMasks;
      nmmasks                         := AudioChannelsNumMasks;
      message.ioData                  := PhonemeBuf;
      message.ioCommand               := CmdWrite;
      message.ioMessage.mnNode.lnType := NTMessage;
      message.ioMessage.mnLength      := TSIZE(narratorrb);
      message.ioMessage.mnReplyPort   := WritePort;
    END;

    IF (OpenDevice(ADR("narrator.device"), 0D, ADR(WriteMsg), 0D) # 0D) THEN
      EXIT; (* err *)
    END;
    NarOpen := TRUE;

    WITH ReadMsg DO
      voice.message.ioMessage.mnNode.lnType := NTMessage;
      voice.message.ioMessage.mnLength      := TSIZE(mouthrb);
      voice.message.ioMessage.mnReplyPort   := ReadPort;
      voice.message.ioDevice := WriteMsg.message.ioDevice;
      voice.message.ioUnit   := WriteMsg.message.ioUnit;
      voice.message.ioCommand := CmdRead;
      voice.message.ioError   := BYTE(0);
      width  := BYTE(0);
      height := BYTE(0);
    END;

    WITH WriteMsg DO
      OptMouths       := BOOLEAN(mouths);
      OptRate         := rate;
      OptPitch        := pitch;
      OptMode         := mode;
      OptSex          := sex;
      OptVolume       := volume;
      OptSamplingFreq := sampfreq;
    END;

    SpeechInProgress := FALSE;
    RETURN (TRUE); (* Successfully Opened Speech! *)
  END; (* LOOP *)

  (* Cleanup After Error *)

  RETURN (FALSE); (* Error *)
END OpenSpeech;

PROCEDURE ReadMouth(VAR Width, Height: CARDINAL): BOOLEAN;
VAR
  Ok : BOOLEAN;
BEGIN
  Ok := DoIO(ADR(ReadMsg)) # LONGINT(NDNoWrite);
  WITH ReadMsg DO
    Width  := CARDINAL(width);
    Height := CARDINAL(height); 
  END;
  RETURN (Ok);
END ReadMouth;

PROCEDURE ResetSpeech();
BEGIN
  DoNarratorCmd(CmdReset);
END ResetSpeech;

PROCEDURE SayAndReturn(text: ADDRESS): BOOLEAN;
BEGIN
  FinishPrevSpeech();
  CopyOptions();
  IF (Translate(text, strlen(text), PhonemeBuf, PhonemeBufferSize) = 0D) THEN
    WriteMsg.message.ioLength := strlen(PhonemeBuf);
    SpeechInProgress := TRUE;
    SendIO(ADR(WriteMsg));
    RETURN (TRUE); (* Ok *)
  END;
  RETURN (FALSE); (* Err *)
END SayAndReturn;

PROCEDURE SayAndWait(text: ADDRESS): BOOLEAN;
BEGIN
  FinishPrevSpeech();
  CopyOptions();
  IF (Translate(text, strlen(text), PhonemeBuf, PhonemeBufferSize) = 0D) THEN
    WriteMsg.message.ioLength := strlen(PhonemeBuf);
    IF (DoIO(ADR(WriteMsg)) = 0D) THEN
      RETURN (TRUE); (* Ok *)
    END;
  END;
  RETURN (FALSE); (* Err *)
END SayAndWait;

PROCEDURE StartSpeech();
BEGIN
  DoNarratorCmd(CmdStart);
END StartSpeech;

PROCEDURE StopSpeech();
BEGIN
  DoNarratorCmd(CmdStop);
END StopSpeech;

BEGIN
  (* Init Audio Channel Allocation Map *)
  DefChannelsMasks[0] := AudioChannelsSet{Left0,Right0};
  DefChannelsMasks[1] := AudioChannelsSet{Left0,Right1};
  DefChannelsMasks[2] := AudioChannelsSet{Left1,Right0};
  DefChannelsMasks[3] := AudioChannelsSet{Left1,Right1};

  AudioChannelsMasks    := ADR(DefChannelsMasks);
  AudioChannelsNumMasks := 4;
  PhonemeBufferSize     := 512D;

END SimpleSpeech.
