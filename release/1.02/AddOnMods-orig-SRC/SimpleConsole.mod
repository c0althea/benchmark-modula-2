(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                      (c) 1986, 1987 by Leon Frenkel                      *
 ****************************************************************************
 * Name: SimpleConsole.MOD                 Version: Amiga.00.00             *
 * Created: 04/03/87   Updated: 08/07/87   Author: Leon Frenkel             *
 * Description: Simplified text i/o for Intuition windows.                  *
 ****************************************************************************)

IMPLEMENTATION MODULE SimpleConsole;

FROM SYSTEM        IMPORT ADR, TSIZE, ADDRESS, BYTE;
FROM Intuition     IMPORT Window, WindowPtr;
FROM IODevices     IMPORT IOStdReqPtr, OpenDevice, CloseDevice, DoIO, SendIO,
                          AbortIO, CmdRead, CmdWrite, CmdClear;
FROM IODevicesUtil IMPORT CreateStdIO, DeleteStdIO;
FROM Memory        IMPORT AllocMem, FreeMem, MemReqSet, MemClear;
FROM Ports         IMPORT MsgPortPtr, GetMsg, WaitPort;
FROM PortsUtil     IMPORT CreatePort, DeletePort;

(*$L+*)

CONST
  BS  = 010C;
  LF  = 012C;
  CR  = 015C;
  BEL = 007C;

(* ========= PRIVATE ======== *)
PROCEDURE FreeWindowExt(ext: WindowExtPtr);
BEGIN
  IF ext^.ReadPort  # NIL THEN DeletePort(ext^.ReadPort^);  END;
  IF ext^.WritePort # NIL THEN DeletePort(ext^.WritePort^); END;
  IF ext^.ReadMsg   # NIL THEN DeleteStdIO(ext^.ReadMsg);   END;
  IF ext^.WriteMsg  # NIL THEN DeleteStdIO(ext^.WriteMsg);  END;
  FreeMem(ext, TSIZE(WindowExt));
END FreeWindowExt;

PROCEDURE QueueRead(ext: WindowExtPtr);
BEGIN
  WITH ext^ DO
    WITH ReadMsg^ DO
      ioCommand := CmdRead;
      ioData    := ADR(ChBuffer);
      ioLength  := 1D;
    END;
  END;
  SendIO(ext^.ReadMsg);
END QueueRead;

PROCEDURE DoWrite(ext: WindowExtPtr; buffer: ADDRESS; length: LONGCARD);
VAR
  err : LONGINT;
BEGIN
  WITH ext^.WriteMsg^ DO
    ioCommand := CmdWrite;
    ioData    := buffer;
    ioLength  := length;
  END;
  err := DoIO(ext^.WriteMsg);
END DoWrite;


(* ====== PUBLIC ======== *)

PROCEDURE ClearInput(VAR window: ARRAY OF BYTE (* Window*) );
VAR
  win : WindowPtr;
  ext : WindowExtPtr;
  err : LONGINT;
  msg : ADDRESS;
BEGIN
  win := ADR(window);
  ext := win^.UserData;
  err := AbortIO(ext^.ReadMsg);
  WHILE (GetMsg(ext^.ReadPort^) = NIL) DO
    msg := WaitPort(ext^.ReadPort^);
  END;
  ext^.ReadMsg^.ioCommand := CmdClear;
  err := DoIO(ext^.ReadMsg);
  QueueRead(ext);
  ext^.Again := FALSE; (* Clear Again Flag *)
END ClearInput;

PROCEDURE CreateConsole(VAR window: ARRAY OF BYTE (* Window *) ): BOOLEAN;
VAR
  win  : WindowPtr;
  wExt : WindowExtPtr;
BEGIN
  win := ADR(window);
  wExt := AllocMem(TSIZE(WindowExt), MemReqSet{MemClear});
  IF (wExt # NIL) THEN
    LOOP
      WITH wExt^ DO
        ReadPort  := CreatePort(NIL,0);
        ReadMsg   := CreateStdIO(ReadPort^);
        WritePort := CreatePort(NIL,0);
        WriteMsg  := CreateStdIO(WritePort^);
        IF (ReadPort = NIL)  OR (ReadMsg = NIL) OR
           (WritePort = NIL) OR (WriteMsg = NIL) THEN
          EXIT; (* error *)
        END;
        WriteMsg^.ioData := win;

        IF OpenDevice(ADR("console.device"), 0D, wExt^.WriteMsg, 0D) # 0D THEN
          EXIT; (* error *)
        END;

        ReadMsg^.ioDevice := WriteMsg^.ioDevice;
        ReadMsg^.ioUnit   := WriteMsg^.ioUnit;
      END; (* WITH *)

      QueueRead(wExt);

      win^.UserData := wExt; (* point to WindowExt *)
      RETURN (TRUE); (* console opened on window! *)

    END; (* LOOP *)
    FreeWindowExt(wExt);
  END;
  RETURN (FALSE); (* error *)
END CreateConsole;

PROCEDURE DeleteConsole(VAR window: ARRAY OF BYTE (* Window *) );
VAR
  win : WindowPtr;
  ext : WindowExtPtr;
  err : LONGINT;
BEGIN
  win := ADR(window);
  ext := win^.UserData;

  err := AbortIO(ext^.ReadMsg); (* Abort read request *)

  CloseDevice(ext^.WriteMsg);

  FreeWindowExt(ext);
END DeleteConsole;

PROCEDURE GetAgain(VAR window: ARRAY OF BYTE (* Window *) );
VAR
  win : WindowPtr;
  ext : WindowExtPtr;
BEGIN
  win := ADR(window);
  ext := win^.UserData;
  ext^.Again := TRUE;
END GetAgain;

PROCEDURE GetCh(VAR window: ARRAY OF BYTE (* Window *) ): CHAR;
VAR
  win : WindowPtr;
  ext : WindowExtPtr;
  ch  : CHAR;
  msg : ADDRESS;
BEGIN
  win := ADR(window);
  ext := win^.UserData;
  IF ext^.Again THEN
    ch := ext^.LastChar;
    ext^.Again := FALSE;
  ELSE
    WHILE (GetMsg(ext^.ReadPort^) = NIL) DO
      msg := WaitPort(ext^.ReadPort^);
    END;
    ch := ext^.ChBuffer;
    ext^.LastChar := ch;
    QueueRead(ext);
  END;
  RETURN (ch);
END GetCh;

PROCEDURE GetStr(VAR window: ARRAY OF BYTE; (* Window *) string: ADDRESS;
                 length: LONGCARD);
VAR
  win   : WindowPtr;
  start : ADDRESS;
  used  : LONGCARD;
  ch    : CHAR;
BEGIN
  win := ADR(window);
  start := string;
  used := 1D;
  LOOP
    ch := GetCh(win^); 
    IF (ch = BS) THEN
      IF (used > 1D) THEN
        DEC(string);
        DEC(used);
        PutCh(win^, ch);
        PutCh(win^, " ");
        PutCh(win^, ch);
      END;
    ELSIF (ch = CR) THEN
      EXIT;
    ELSE
      IF (used < length) THEN
        PutCh(win^, ch);
        string^ := ch;
        INC(string);
        INC(used);
      ELSE
        PutCh(win^, BEL); (* flash display *)
      END;
    END;
  END; (* LOOP *)
  string^ := 0C;
  QueueRead(win^.UserData);
END GetStr;

PROCEDURE PutCh(VAR window: ARRAY OF BYTE; (* Window *) ch: CHAR);
VAR
  win : WindowPtr;
BEGIN
  win := ADR(window);
  DoWrite(win^.UserData, ADR(ch), 1D);
END PutCh;

PROCEDURE PutStr(VAR window: ARRAY OF BYTE; (* Window *) string: ADDRESS);
VAR
  win : WindowPtr;
BEGIN
  win := ADR(window);
  DoWrite(win^.UserData, string, -1D);
END PutStr;

PROCEDURE PutBuf(VAR window: ARRAY OF BYTE; (* Window *) 
                 buffer: ADDRESS; length: LONGCARD);
VAR
  win : WindowPtr;
BEGIN
  win := ADR(window);
  DoWrite(win^.UserData, buffer, length);
END PutBuf;

PROCEDURE TestCh(VAR window: ARRAY OF BYTE (* Window *) ): CHAR;
VAR
  win : WindowPtr;
  ext : WindowExtPtr;
  ch  : CHAR;
BEGIN
  win := ADR(window);
  ext := win^.UserData;
  IF ext^.Again THEN
    ch := ext^.LastChar;
    ext^.Again := FALSE;
  ELSE
    IF (GetMsg(ext^.ReadPort^) = NIL) THEN
      RETURN (0C);
    END;
    ch := ext^.ChBuffer;
    ext^.LastChar := ch;
    QueueRead(ext);
  END;
  RETURN (ch);
END TestCh;

END SimpleConsole.
