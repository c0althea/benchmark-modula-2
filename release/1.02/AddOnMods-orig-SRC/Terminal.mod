(****************************************************************************
 *                    Modula-2 Software Construction Set                    *
 *                         (c) 1986 by Leon Frenkel                         *
 ****************************************************************************
 * Name: Terminal.MOD                      Version: Amiga.00.00             *
 * Created: 11/19/86   Updated: 12/25/87   Author: Leon Frenkel             *
 * Description: Standard Terminal I/O as described in Programming in        *
 *  appendix 2 of Programming in Modula-2 by N. Wirth.                      *
 *  The procedures in this module read/write to the I/O files initially     *
 *  assigned to a program. This is usually the window from which the program*
 *  started but may be redirected by the user to any file using the         *
 *  AmigaDOS ">" and "<" after the name of the program.                     *
 ****************************************************************************)

IMPLEMENTATION MODULE Terminal;

FROM SYSTEM IMPORT ADR;
FROM AmigaDOS IMPORT FileHandle;
IMPORT AmigaDOS;
FROM System IMPORT StdInput, StdOutput;

(*$L+*)
(*$D-*)

CONST
  BusyReadDelay = 1D; (* Should really be 0 but AmigaDOS has bug! *)

VAR
  ReadAgainFlag : BOOLEAN;
  LastChar      : CHAR;

PROCEDURE Read(VAR ch: CHAR);
BEGIN
  eof := FALSE;
  IF ReadAgainFlag THEN
    ch := LastChar;
    ReadAgainFlag := FALSE;
  ELSE
    IF AmigaDOS.Read(StdInput, ADR(ch), 1D) = 0D THEN
      eof := TRUE;
    ELSE
      LastChar := ch;
    END;
  END;
END Read;

PROCEDURE BusyRead(VAR ch: CHAR);
VAR result: LONGINT;
BEGIN
  eof := FALSE;
  IF ReadAgainFlag THEN
    ch := LastChar;
    ReadAgainFlag := FALSE;
  ELSE
    IF AmigaDOS.WaitForChar(StdInput, BusyReadDelay) THEN
      result := AmigaDOS.Read(StdInput, ADR(ch), 1);
      LastChar := ch;
    ELSE
      ch := 0C; (* No char read! *)
    END;
  END;
END BusyRead;

PROCEDURE ReadAgain;
BEGIN
  ReadAgainFlag := TRUE;
END ReadAgain;

PROCEDURE Write(ch: CHAR);
VAR result : LONGINT;
BEGIN
  result := AmigaDOS.Write(StdOutput, ADR(ch), 1);
END Write;

PROCEDURE WriteLn;
BEGIN
  Write(12C);
END WriteLn;

PROCEDURE WriteString(s: ARRAY OF CHAR);
VAR result : LONGINT;
    length : CARDINAL;
BEGIN
  length := 0;
  WHILE (length <= HIGH(s)) AND (s[length] # 0C) DO
    INC(length);
  END;
  result := AmigaDOS.Write(StdOutput, ADR(s), VAL(LONGCARD, length));
END WriteString;

BEGIN
(* All Variables Are Assumed To Be 0.
  ReadAgainFlag := FALSE;
  eof           := FALSE;
*)
END Terminal.
