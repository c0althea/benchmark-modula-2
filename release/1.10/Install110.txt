Installation Notes for M2SCS version 1.10
4/3/94

Tom Breeden
Aglet Software
Box 3314
Charlottesville, VA 22903
tmb@virginia.edu

The v1.10 disk contains new executables for the editor (M2Ed), linker (M2Lk),
compiler (M2), debugger (M2Debug), and the compiler configuration
utility (M2Config).

Additionally, replacement versions of a number of existing support modules
are included:

   System.SBM
   System.OBM

   MathLib0.SBM  (see notes below)
   MathLib0.OBM  (see notes below)
   RealConversions.OBM  (see notes below)

   InitMathLib0.OBM

These should replace the corresponding existing files in your M2L:
directory (Don't forget to preserve your originals onto a floppy first!).

A number of new support modules are on the disk. These should be copied
into M2L: as well:

   SystemCPU.SBM
   SystemCPU.OBM
   SystemFFP.SBM
   SystemFFP.OBM
   SystemIEEE.SBM
   SystemIEEE.OBM
   SystemIEEEdp.SBM
   SystemIEEEdp.OBM

   MathFFP.sbm
   MathFFP.obm
   MathIEEESPBas.sbm
   MathIEEESPBas.obm
   MathTrans.sbm
   MathTrans.obm
   MathIEEESPTrans.sbm
   MathIEEESPTrans.obm

   LongMathLib0.SBM
   LongMathLib0.OBM
   LongRealConsts.SBM
   LongRealConsts.OBM
   LongRealConversions.SBM
   LongRealConversions.OBM
   LongRealInOut.SBM
   LongRealInOut.OBM

   RealOps.SBM
   RealOps.OBM

The following files are needed to install or switch your preferred REAL
format between FFP and IEEE REALs:

   MathLib0FFP.SBM
   MathLib0FFP.OBM
   MathLib0IEEE.SBM
   MathLib0IEEE.OBM
   RealConversionsFFP.OBM
   RealConversionsIEEE.OBM

The assembler include files are needed if you are doing any mixed assembler
and M2 programming:

   System_Map.i
   M2Lib.i

###############################################################################
     WARNING: It is very important that you be sure to use the new version
              of System.SBM and System.OBM with the v1.10 compiler/linker.

              Using the old System.OBM with new code could result in a process
              or system crash.
  
              The new System is backwards compatible, however, with old code.
###############################################################################

FFP vs. IEEE REALS

   The MathLib0 on the v1.10 disk supports FFP format REALs.
   The RealConversions on the v1.10 disk supports FFP format REALs.
   The compiler on the v1.10 disk is set to default to FFP REAL code.

   If you want to use or try out the alternative REAL format, MathLib0 needs
   to be "reconfigured" to switch its support from FFP to IEEE format reals.

   To do this, simply:

      copy MathLib0IEEE.SBM to MathLib0.SBM
      copy MathLib0IEEE.OBM to MathLib0.OBM
      copy RealConversionsIEEE.OBM to RealConversions.OBM

   You will also need to be sure that the compiler is set to generate IEEE
   REALs for modules using MathLib0 (or any other real number operations).
   Use the "-e+" CLI switch, or set the M2Ed Compiler Option to IEEE, or
   run M2Config on the compiler.

   Any of your old modules using REAL will have be be recompiled before
   linking with IEEE REAL compiled modules.


Text Files

   User notes and installation notes are contained in the following text
   files on the disk:

      Install110.txt (this file)
      UserNotes110.txt
      KnownBugs110.txt
